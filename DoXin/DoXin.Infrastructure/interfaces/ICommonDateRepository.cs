﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface ICommonDateRepository : IGenericRepository<CommonDate>
    {
        CommonEntity CommonEntity { get; }
    }
}
