﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface ICommonInvoiceRepository : IGenericRepository<CommonInvoice>
    {
        CommonEntity CommonEntity { get; }
    }
}
