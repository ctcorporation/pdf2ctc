﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface ICommonOrderLineRepository : IGenericRepository<CommonOrderLine>
    {
        CommonEntity CommonEntity { get; }
    }
}
