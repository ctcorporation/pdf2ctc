﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface ICompanyRepository : IGenericRepository<Company>
    {
        CommonEntity CommonEntity { get; }
    }
}
