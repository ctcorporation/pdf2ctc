﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface ICompanyTypeRepository : IGenericRepository<CompanyType>
    {
        CommonEntity CommonEntity { get; }
    }
}
