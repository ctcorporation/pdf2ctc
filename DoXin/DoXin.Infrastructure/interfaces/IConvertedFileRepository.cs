﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface IConvertedFileRepository : IGenericRepository<ConvertedFile>
    {
        CommonEntity CommonEntity { get; }
    }
}
