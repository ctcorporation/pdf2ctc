﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface ICustomerReferenceRepository : IGenericRepository<CustomerReference>
    {
        CommonEntity CommonEntity { get; }
    }
}
