﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        CommonEntity CommonEntity { get; }
    }
}
