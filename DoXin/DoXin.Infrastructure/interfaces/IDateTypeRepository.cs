﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface IDateTypeRepository : IGenericRepository<DateType>
    {
        CommonEntity CommonEntity { get; }
    }
}
