﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface IErrorMessageRepository : IGenericRepository<ErrorMessage>
    {
        CommonEntity CommonEntity { get; }
    }
}
