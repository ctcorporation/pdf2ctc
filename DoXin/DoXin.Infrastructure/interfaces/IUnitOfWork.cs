﻿using System;

namespace DoXin.Infrastructure.interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        IUserTypeRepository UserTypes { get; }
        ICommonDateRepository CommonDates { get; }
        ICommonInvoiceRepository CommonInvoices { get; }
        ICommonOrderLineRepository CommonOrderLines { get; }
        ICompanyRepository Companies { get; set; }
        ICompanyTypeRepository CompanyTypes { get; }
        IConvertedFileRepository ConvertedFiles { get; }
        ICustomerReferenceRepository CustomerReferences { get; }
        ICustomerRepository Customers { get; }
        IDateTypeRepository DateTypes { get; }
        IErrorMessageRepository ErrorMessages { get; }
        IWarehouseOrderRepository WarehouseOrders { get; }

        int Complete();
    }
}
