﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        CommonEntity CommonEntity { get; }
    }
}
