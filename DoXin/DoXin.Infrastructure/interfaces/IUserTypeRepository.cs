﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface IUserTypeRepository : IGenericRepository<UserType>
    {
        CommonEntity CommonEntity { get; }
    }
}
