﻿using Doxin.Core.models;

namespace DoXin.Infrastructure.interfaces
{
    public interface IWarehouseOrderRepository : IGenericRepository<WarehouseOrder>
    {
        CommonEntity CommonEntity { get; }
    }
}
