﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class CommonDateRepository : GenericRepository<CommonDate>, ICommonDateRepository
    {
        public CommonDateRepository(CommonEntity context)
            : base(context)
        {

        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
