﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class CommonInvoiceRepository : GenericRepository<CommonInvoice>, ICommonInvoiceRepository
    {
        public CommonInvoiceRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
