﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class CompanyRepository : GenericRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
