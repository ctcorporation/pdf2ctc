﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class ConvertedFileRepository : GenericRepository<ConvertedFile>, IConvertedFileRepository
    {
        public ConvertedFileRepository(CommonEntity context)
            : base(context)
        {

        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }

    }
}
