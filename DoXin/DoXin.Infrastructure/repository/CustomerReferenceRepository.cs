﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class CustomerReferenceRepository : GenericRepository<CustomerReference>, ICustomerReferenceRepository
    {
        public CustomerReferenceRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
