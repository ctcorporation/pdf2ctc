﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
