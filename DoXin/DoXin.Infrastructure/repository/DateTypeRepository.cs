﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class DateTypeRepository : GenericRepository<DateType>, IDateTypeRepository
    {
        public DateTypeRepository(CommonEntity context)
            : base(context)
        {


        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }

        }
    }
}
