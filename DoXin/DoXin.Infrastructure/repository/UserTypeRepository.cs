﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class UserTypeRepository : GenericRepository<UserType>, IUserTypeRepository
    {
        public UserTypeRepository(CommonEntity context)
            : base(context)
        {

        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
