﻿using Doxin.Core.models;
using DoXin.Infrastructure.interfaces;

namespace DoXin.Infrastructure.repository
{
    public class WarehouseOrderRepository : GenericRepository<WarehouseOrder>, IWarehouseOrderRepository
    {

        public WarehouseOrderRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
