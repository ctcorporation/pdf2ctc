﻿using Doxin.Core.models;
using DoxIn.Web.Models;
using DoXin.Infrastructure.dto;
using DoXin.Infrastructure.interfaces;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DoxIn.Web.Controllers
{
    public class AdminController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private CommonEntity _context;

        // GET: Admin
        public AdminController()
        {
            _context = new CommonEntity();
        }

        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;

        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();

            }
            private set { _roleManager = value; }
        }


        public ActionResult Index()
        {
            List<ApplicationUserViewModel> userList = new List<ApplicationUserViewModel>();
            var userlist = (from ul in UserManager.Users.ToList()
                            where ul.IsLoggedIn
                            select ul).ToList();
            foreach (var user in userlist)
            {

                ApplicationUserViewModel userWithRoles = new ApplicationUserViewModel();
                userWithRoles.User = user;

                //  userRole.Roles = UserManager.GetRoles(user.Id).ToList();
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                {
                    userWithRoles.Customer = uow.Customers.Find(x => x.CU_CODE == user.CompanyCode).FirstOrDefault();
                }
                userList.Add(userWithRoles);

            }
            return View(userList);

            
        }
    }
}