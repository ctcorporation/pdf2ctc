﻿using Doxin.Core.models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DoxIn.Web.Controllers
{
    public class CompanyTypesController : Controller
    {
        private CommonEntity db = new CommonEntity();

        // GET: CompanyTypes
        public async Task<ActionResult> Index()
        {
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            ViewBag.Operation = TempData["Operation"];
            ViewBag.Success = TempData["Success"];
            return View(await db.CompanyTypes.ToListAsync());
        }

        // GET: CompanyTypes/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyType companyType = await db.CompanyTypes.FindAsync(id);
            if (companyType == null)
            {
                return HttpNotFound();
            }
            return View(companyType);
        }

        // GET: CompanyTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CompanyTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CT_ID,CT_CompanyType")] CompanyType companyType)
        {
            if (ModelState.IsValid)
            {
                companyType.CT_ID = Guid.NewGuid();
                db.CompanyTypes.Add(companyType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(companyType);
        }

        // GET: CompanyTypes/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyType companyType = await db.CompanyTypes.FindAsync(id);
            if (companyType == null)
            {
                return HttpNotFound();
            }
            return View(companyType);
        }

        // POST: CompanyTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CT_ID,CT_CompanyType")] CompanyType companyType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(companyType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(companyType);
        }

        // GET: CompanyTypes/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var ct = db.CompanyTypes.Find(id);
            var inUse = (from c in db.Companies
                         where (c.CO_CompanyType.CT_CompanyType == ct.CT_CompanyType)
                         select c.CO_ID).ToList();
            if (inUse.Count > 0)
            {
                TempData["ErrorMessage"] = "Unable to Delete when Company Type is in use";
                TempData["Operation"] = "Failed to Delete Company Type";
            }
            else
            {
                db.CompanyTypes.Remove(ct);
                await db.SaveChangesAsync();
                TempData["ErrorMessage"] = "Company Type Deleted successfully";
                TempData["Operation"] = "Delete Company Type";
                TempData["Success"] = "true";
            }

            return RedirectToAction("Index");


        }

        // POST: CompanyTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            CompanyType companyType = await db.CompanyTypes.FindAsync(id);
            db.CompanyTypes.Remove(companyType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
