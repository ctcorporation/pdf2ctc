﻿using Doxin.Core.models;
using DoxIn.Web.Models;
using DoXin.Infrastructure.dto;
using DoXin.Infrastructure.interfaces;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace DoxIn.Web.Controllers
{
    [Authorize]
    public class ConvertedFilesController : Controller
    {
        // GET: ConvertedFiles
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private CommonEntity _context;

        public ConvertedFilesController()
        {
            _context = new CommonEntity();

        }

        public ConvertedFilesController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;

        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [HttpPost]
        public ActionResult ProcessFile(CommonInvoiceMasterDetail cinv)
        {


            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                var file = uow.ConvertedFiles.Get(cinv.File.FC_ID);
                file.FC_FlagForDelete = cinv.File.FC_FlagForDelete;
                file.FC_Released = cinv.File.FC_Released;
                uow.Complete();
                return RedirectToAction("Details", "ConvertedFiles", new { id = file.FC_ID });
            }


        }

        public ActionResult Index()
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Logoff", "Account");
            }
            FilteredConversionViewModel convertedVM = new FilteredConversionViewModel();
            Guid customerId = Guid.Empty;

            if (Session["CompanyId"] == null)
            {
                return RedirectToAction("Logoff", "Account");
            }

            convertedVM.DeletedFilter = "hidedeleted";
            convertedVM.ProcessedFilter = "hideprocessed";
            customerId = Guid.Parse(Session["CompanyId"].ToString());
            var convertedFiles = _context.ConvertedFiles.Where(f => f.FC_CustomerId == customerId).ToList();
            convertedFiles = (from f in convertedFiles
                              where f.FC_Released == false
                              && f.FC_FlagForDelete == false
                              select f).ToList();

            convertedVM.ConvertedFile = convertedFiles;
            return View(convertedVM);
        }

        [HttpGet]

        public ActionResult Index(FilteredConversionViewModel model, string deletedFilter, string processedFilter)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Logoff", "Account");
            }

            Guid customerId = Guid.Empty;

            if (Session["CompanyId"] == null)
            {
                return RedirectToAction("Logoff", "Account");
            }
            FilteredConversionViewModel convertedVM = new FilteredConversionViewModel();

            if (string.IsNullOrEmpty(deletedFilter))
            {

                convertedVM.DeletedFilter = string.IsNullOrEmpty(model.DeletedFilter) ? "hidedeleted" : model.DeletedFilter;
                convertedVM.ProcessedFilter = string.IsNullOrEmpty(model.ProcessedFilter) ? "hideprocessed" : model.ProcessedFilter;

            }
            else
            {
                convertedVM.DeletedFilter = deletedFilter;
                convertedVM.ProcessedFilter = processedFilter;
            }
            customerId = Guid.Parse(Session["CompanyId"].ToString());
            var convertedFiles = _context.ConvertedFiles.Where(f => f.FC_CustomerId == customerId).ToList();
            switch (convertedVM.ProcessedFilter)
            {
                case "hideprocessed":
                    convertedFiles = (from f in convertedFiles
                                      where f.FC_Released == false
                                      select f).ToList();
                    break;
                case "showonlyprocessed":
                    convertedFiles = (from f in convertedFiles
                                      where f.FC_Released == true
                                      select f).ToList();
                    break;

            }
            switch (convertedVM.DeletedFilter)
            {
                case "hidedeleted":
                    convertedFiles = (from f in convertedFiles
                                      where f.FC_FlagForDelete == false
                                      select f).ToList();

                    break;
                case "showonlydeleted":
                    convertedFiles = (from f in convertedFiles
                                      where f.FC_FlagForDelete == true
                                      select f).ToList();

                    break;
            }
            convertedVM.ConvertedFile = convertedFiles;
            ModelState.Clear();
            return View(convertedVM);
        }

        public ActionResult Details(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Guid cfid = Guid.Parse(id);

                var invoice = _context.CommonInvoices.FirstOrDefault(c => c.CI_ConvertedFileId == cfid);
                var dates = (from d in _context.CommonDates
                             where (d.DA_InvoiceId == invoice.CI_ID)
                             join t in _context.DateTypes on d.DA_DateTypeId equals t.DT_Id into dt
                             from dtLookup in dt.DefaultIfEmpty()
                             select new
                             {
                                 d.DA_ActualDate,
                                 value = dtLookup ?? null,
                                 d.DA_DateTypeId,
                                 d.DA_ErrorsFound,
                                 d.DA_EstimateDate,
                                 d.DA_Id,
                                 d.DA_Invoice,
                                 d.DA_InvoiceId,
                                 d.Errors
                             }).ToList()
                             .Select(x => new CommonDate
                             {
                                 DA_ActualDate = x.DA_ActualDate,
                                 DA_DateType = x.value,
                                 DA_DateTypeId = x.DA_DateTypeId,
                                 DA_ErrorsFound = x.DA_ErrorsFound,
                                 DA_EstimateDate = x.DA_EstimateDate,
                                 DA_Id = x.DA_Id,
                                 DA_Invoice = x.DA_Invoice,
                                 DA_InvoiceId = x.DA_InvoiceId,
                                 Errors = x.Errors
                             });
                var dateTypes = _context.DateTypes.OrderBy(t => t.DT_Name).ToList();
                ViewBag.SelectedDateType = new SelectList(dateTypes, "DT_ID", "DT_Name", null);
                var companies = (from c in _context.Companies
                                 where (c.CO_InvoiceId == invoice.CI_ID)
                                 join t in _context.CompanyTypes on c.CO_CompanyTypeId equals t.CT_ID into ct
                                 from ctLookup in ct.DefaultIfEmpty()
                                 select new
                                 {
                                     c.CO_ID,
                                     c.CO_Active,
                                     c.CO_Address1,
                                     c.CO_Address2,
                                     c.CO_Address3,
                                     c.CO_City,
                                     c.CO_Code,
                                     value = ctLookup ?? null,
                                     c.CO_CompanyTypeId,
                                     c.CO_ContactName,
                                     c.CO_Country,
                                     c.CO_EmailAddress,
                                     c.CO_ErrorsFound,
                                     c.CO_Invoice,
                                     c.CO_InvoiceId,
                                     c.CO_Name,
                                     c.CO_PostCode,
                                     c.CO_State
                                 }).ToList().Select(c => new Company
                                 {
                                     CO_ID = c.CO_ID,
                                     CO_Active = c.CO_Active,
                                     CO_Address1 = c.CO_Address1,
                                     CO_Address2 = c.CO_Address2,
                                     CO_Address3 = c.CO_Address3,
                                     CO_City = c.CO_City,
                                     CO_Code = c.CO_Code,
                                     CO_CompanyType = c.value,
                                     CO_CompanyTypeId = c.CO_CompanyTypeId,
                                     CO_ContactName = c.CO_ContactName,
                                     CO_Country = c.CO_Country,
                                     CO_EmailAddress = c.CO_EmailAddress,
                                     CO_ErrorsFound = c.CO_ErrorsFound,
                                     CO_Invoice = c.CO_Invoice,
                                     CO_InvoiceId = c.CO_InvoiceId,
                                     CO_Name = c.CO_Name,
                                     CO_PostCode = c.CO_PostCode,
                                     CO_State = c.CO_State
                                 }
                                 );
                var companyTypes = _context.CompanyTypes.OrderBy(ct => ct.CT_CompanyType).ToList();
                ViewBag.SelectedCompanyType = new SelectList(companyTypes, "CT_ID", "CT_CompanyType", null);
                string sql = @"Select [OL_ID],[OL_ParentID],[OL_ParentKey],[OL_LineNo],[OL_InvoicedQty],[OL_OrderedQty],[OL_UnitPrice],[OL_UnitOfMeasurement]
                              ,[OL_Tariff],[OL_UnitWeight],[OL_WeightUnit],[OL_Volume],[OL_VolumeUnit],[OL_LineTotal],[OL_PackageQty],[OL_PackageUnit]
                              ,[OL_ProductCode],[OL_Barcode],[OL_ProductDescription],[OL_InvoiceID],[OL_Invoice_CI_ID],[OL_ErrorsFound] 
                                from CommonOrderLines order by CAST(OL_LineNo as int)";
                var lines = _context.Database.SqlQuery<CommonOrderLine>(sql).Where(l => l.OL_InvoiceID == invoice.CI_ID).ToList();
                //var lines = (from l in _context.CommonLines.AsEnumerable()
                //             where l.OL_InvoiceID == invoice.CI_ID
                //             select l)
                //             .OrderBy(l => int.Parse(l.OL_LineNo))
                //             .ToList();
                Guid fileID = Guid.Parse(id);
                CommonInvoiceMasterDetail detailInvoice = new CommonInvoiceMasterDetail
                {
                    File = _context.ConvertedFiles.Where(f => f.FC_ID == fileID).FirstOrDefault(),
                    CommonInvoice = invoice,
                    Companies = companies,
                    Dates = dates,
                    Lines = lines
                };
                return View(detailInvoice);

            }
            return RedirectToAction("Index");
        }

        [HttpGet]

        public ActionResult UpdateHeader(Guid? id, Guid? fid)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                var invHeader = uow.CommonInvoices.Get((Guid)id);
                if (invHeader == null)
                {
                    return RedirectToRoute("Error", "404");
                }
                var countryList = from list in DoXinResources.CountryList()
                                  orderby list.Value ascending
                                  select list;
                ViewBag.Countries = new SelectList(countryList, "Key", "Value");
                CommonInvoiceMasterDetail invoice = new CommonInvoiceMasterDetail();
                invoice.CommonInvoice = invHeader;
                invoice.File = uow.ConvertedFiles.Get((Guid)fid);
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
                return View(invoice);
            }
        }

        [HttpPost]

        public ActionResult UpdateHeader(CommonInvoiceMasterDetail invoice)
        {

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(Request.Form["ErrorAck"]))
                {
                    UpdateErrorAck(Request.Form["ErrorAck"]);
                }
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                {
                    var updated = uow.CommonInvoices.Get(invoice.CommonInvoice.CI_ID);
                    if (updated != null)
                    {
                        updated.CI_Currency = string.IsNullOrEmpty(invoice.CommonInvoice.CI_Currency) ? string.Empty : invoice.CommonInvoice.CI_Currency.ToUpper();
                        updated.CI_CountryOfOrigin = string.IsNullOrEmpty(invoice.CommonInvoice.CI_CountryOfOrigin) ? string.Empty : invoice.CommonInvoice.CI_CountryOfOrigin.ToUpper();
                        updated.CI_InvoiceTerms = string.IsNullOrEmpty(invoice.CommonInvoice.CI_InvoiceTerms) ? string.Empty : invoice.CommonInvoice.CI_InvoiceTerms.ToUpper();
                        updated.CI_ShipmentNo = string.IsNullOrEmpty(invoice.CommonInvoice.CI_ShipmentNo) ? string.Empty : invoice.CommonInvoice.CI_ShipmentNo.ToUpper();
                        updated.CI_TotalInvoiceAmount = string.IsNullOrEmpty(invoice.CommonInvoice.CI_TotalInvoiceAmount) ? string.Empty : invoice.CommonInvoice.CI_TotalInvoiceAmount;
                        updated.CI_InvoiceNo = string.IsNullOrEmpty(invoice.CommonInvoice.CI_InvoiceNo) ? string.Empty : invoice.CommonInvoice.CI_InvoiceNo;
                    }

                    uow.Complete();
                }
                return RedirectToAction("Details", new { id = invoice.File.FC_ID.ToString() });
            }
            TempData["ErrorMessage"] = "Please ensure all fields are correctly filled out";

            return View("UpdateHeader", new { id = invoice.CommonInvoice.CI_ID, fid = invoice.File.FC_ID });

        }

        [HttpGet]
        public ActionResult UpdateLine(Guid id, Guid convertId)
        {
            if (id != Guid.Empty)
            {
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                {
                    var invoice = uow.CommonOrderLines.Get(id);
                    if (invoice == null)
                    {
                        return RedirectToRoute("Error", "404");
                    }
                    invoice = ValidateLineTotals(invoice);
                    ViewBag.ErrorList = uow.ErrorMessages.Find(x => x.EM_CommonLineId == invoice.OL_ID).ToList();
                    ViewBag.ConvertFileID = convertId;
                    ViewBag.ConvertedFile = uow.ConvertedFiles.Get((Guid)convertId);
                    return View(invoice);
                }
            }
            else
            {
                return Redirect(Request.UrlReferrer.ToString());
            }

        }

        private CommonOrderLine ValidateLineTotals(CommonOrderLine invoice)
        {
            double qty = 0;
            if (double.TryParse(invoice.OL_InvoicedQty, out qty))
            {
                double linePrice = 0;
                if (double.TryParse(invoice.OL_UnitPrice, out linePrice))
                {
                    var lineTotal = qty * linePrice;
                    invoice.OL_LineTotal = string.IsNullOrEmpty(invoice.OL_LineTotal) ? lineTotal.ToString() : invoice.OL_LineTotal;
                }
            }
            return invoice;
        }

        public ActionResult ProcessFile(Guid id, string procAction)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                var file = uow.ConvertedFiles.Get(id);
                if (file != null)
                {
                    switch (procAction)
                    {
                        case "Released":
                            file.FC_Released = true;
                            file.FC_ReleaseDate = DateTime.Now;
                            break;
                        case "Deleted":
                            file.FC_FlagForDelete = true;
                            file.FC_DeleteDate = DateTime.Now;
                            break;
                    }
                    uow.Complete();
                }
            }

            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult UpdateLine(CommonOrderLine invoice, List<ErrorMessage> errorListing)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                UpdateErrorAck(Request.Form["ErrorAck"]);

                var updatedInvoice = uow.CommonOrderLines.Get(invoice.OL_ID);
                if (updatedInvoice != null)
                {
                    updatedInvoice.OL_Barcode = invoice.OL_Barcode;
                    updatedInvoice.OL_InvoicedQty = invoice.OL_InvoicedQty;
                    updatedInvoice.OL_LineNo = invoice.OL_LineNo;
                    updatedInvoice.OL_LineTotal = invoice.OL_LineTotal;
                    updatedInvoice.OL_OrderedQty = invoice.OL_OrderedQty;
                    updatedInvoice.OL_PackageQty = invoice.OL_PackageQty;
                    updatedInvoice.OL_PackageUnit = invoice.OL_PackageUnit;
                    updatedInvoice.OL_ProductCode = invoice.OL_ProductCode;
                    updatedInvoice.OL_ProductDescription = invoice.OL_ProductDescription;
                    updatedInvoice.OL_Tariff = invoice.OL_Tariff;
                    updatedInvoice.OL_UnitOfMeasurement = invoice.OL_UnitOfMeasurement;
                    updatedInvoice.OL_UnitPrice = invoice.OL_UnitPrice;
                    updatedInvoice.OL_UnitWeight = invoice.OL_UnitWeight;
                    updatedInvoice.OL_Volume = invoice.OL_Volume;
                    updatedInvoice.OL_VolumeUnit = invoice.OL_VolumeUnit;
                    updatedInvoice.OL_WeightUnit = invoice.OL_WeightUnit;
                    updatedInvoice.OL_ErrorsFound = GetLineErrorAcks(invoice.OL_ID);

                    uow.Complete();
                }


            }
            return RedirectToAction("Details", new { id = Request.Form["convertFileId"] });
        }

        public void UpdateErrorAck(string errorAcks)
        {
            if (string.IsNullOrEmpty(errorAcks))
            {
                return;
            }
            var errorList = errorAcks.Split(',').ToList();
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {


                foreach (var error in errorList)
                {
                    var updatedError = uow.ErrorMessages.Get(Guid.Parse(error));
                    if (updatedError != null)
                    {
                        updatedError.EM_Acknowledged = true;

                    }
                }
                uow.Complete();
            }
        }

        private bool GetLineErrorAcks(Guid oL_ID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                var errorlist = uow.ErrorMessages.Find(x => x.EM_CommonLineId == oL_ID && !x.EM_Acknowledged).ToList();
                if (errorlist.Count > 0)
                { return true; }
                return false;

            }
        }

        [HttpGet]
        public ActionResult UpdateCompany(Guid? id, Guid? convertId, Guid? invoiceId)
        {
            try
            {
                if (convertId != null)
                {
                    using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                    {
                        Company company = new Company();
                        if (id != null)
                        {
                            company = uow.Companies.Get((Guid)id);
                            if (company == null)
                            {
                                return RedirectToRoute("Error", "404");
                            }
                            ViewBag.ErrorList = uow.ErrorMessages.Find(x => x.EM_CompanyId == company.CO_ID).ToList();
                        }

                        var companyTypes = _context.CompanyTypes.OrderBy(ct => ct.CT_CompanyType).ToList();
                        ViewBag.CompanyTypes = new SelectList(companyTypes, "CT_ID", "CT_CompanyType", null);
                        var countryList = from list in DoXinResources.CountryList()
                                          orderby list.Value ascending
                                          select list;
                        ViewBag.CountryList = new SelectList(countryList, "Key", "Value");
                        ViewBag.ConvertFileID = convertId;
                        ViewBag.ConvertedFile = uow.ConvertedFiles.Get((Guid)convertId);
                        ViewBag.InvoiceId = invoiceId;
                        return View(company);
                    }

                }
                else
                {

                    return Redirect(Request.UrlReferrer.ToString());
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }


        }

        [HttpPost]
        public ActionResult UpdateCompany(Company company, List<ErrorMessage> errorListing)
        {
            UpdateErrorAck(Request.Form["ErrorAck"]);
            if (company != null)
            {
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                {
                    var updatedCompany = uow.Companies.Get(company.CO_ID);
                    if (updatedCompany == null)
                    {
                        updatedCompany = new Company();
                        Guid t;
                        updatedCompany.CO_InvoiceId = Guid.TryParse(Request.Form["invoiceId"], out t) ? t : Guid.Empty;
                    }
                    else
                    {
                        updatedCompany.CO_ErrorsFound = GetCompanyErrors(company.CO_ID);
                    }
                    updatedCompany.CO_Address1 = company.CO_Address1;
                    updatedCompany.CO_Address2 = company.CO_Address2;
                    updatedCompany.CO_Address3 = company.CO_Address3;
                    updatedCompany.CO_City = company.CO_City;
                    updatedCompany.CO_Code = company.CO_Code;
                    updatedCompany.CO_CompanyTypeId = company.CO_CompanyTypeId;
                    updatedCompany.CO_ContactName = company.CO_ContactName;
                    updatedCompany.CO_Country = company.CO_Country;
                    updatedCompany.CO_EmailAddress = company.CO_EmailAddress;
                    updatedCompany.CO_Name = company.CO_Name;
                    updatedCompany.CO_PostCode = company.CO_PostCode;
                    updatedCompany.CO_State = company.CO_State;
                    if (updatedCompany.CO_ID == Guid.Empty)
                    {
                        uow.Companies.Add(updatedCompany);
                    }
                    uow.Complete();
                }
                return RedirectToAction("Details", new { id = Request.Form["convertFileId"] });
            }
            else
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
        }

        private bool GetCompanyErrors(Guid cO_ID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                var errorlist = uow.ErrorMessages.Find(x => x.EM_CompanyId == cO_ID && !x.EM_Acknowledged).ToList();
                if (errorlist.Count > 0)
                { return true; }
                return false;

            }
        }

        [HttpGet]
        public ActionResult UpdateDates(Guid? id, Guid? fid, Guid? cid)
        {
            if (fid == null)
            {
                return RedirectToAction("Index");
            }
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                CommonDate date = new CommonDate();
                if (id != null)
                {

                    date = uow.CommonDates.Get((Guid)id);
                    if (date == null)
                    {
                        return RedirectToRoute("Error", "404");
                    }
                    date.DA_EstimateDate = ValidateDate(date.DA_EstimateDate);
                    date.DA_ActualDate = ValidateDate(date.DA_ActualDate);
                    ViewBag.ErrorList = uow.ErrorMessages.Find(x => x.EM_DateId == date.DA_Id).ToList();
                }
                var dateTypes = _context.DateTypes.OrderBy(dt => dt.DT_DateType).ToList();
                ViewBag.DateTypes = new SelectList(dateTypes, "DT_Id", "DT_DateType", null);
                ViewBag.ConvertedFile = uow.ConvertedFiles.Get((Guid)fid);
                ViewBag.ConvertFileID = fid;
                ViewBag.InvoiceId = cid;
                return View(date);
            }
        }

        private string ValidateDate(string dateVal)
        {
            DateTime dt = new DateTime();
            if (string.IsNullOrEmpty(dateVal))
            {
                return string.Empty;
            }
            if (DateTime.TryParse(dateVal, out dt))
            {
                return dt.ToString("dd/MM/yyyy");
            }
            return dateVal;
        }

        [HttpPost]
        public ActionResult UpdateDates(CommonDate date, FormCollection form, List<ErrorMessage> errorListing)
        {
            UpdateErrorAck(Request.Form["ErrorAck"]);
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                var updatedDate = uow.CommonDates.Get(date.DA_Id);
                if (updatedDate == null)
                {
                    updatedDate = new CommonDate();
                    updatedDate.DA_InvoiceId = !string.IsNullOrEmpty(form["InvoiceID"]) ? Guid.Parse(form["Invoiceid"]) : Guid.Empty;
                }
                updatedDate.DA_ActualDate = date.DA_ActualDate;
                updatedDate.DA_DateTypeId = date.DA_DateTypeId;
                updatedDate.DA_EstimateDate = date.DA_EstimateDate;
                updatedDate.DA_ErrorsFound = GetDateErrors(date.DA_Id);
                if (updatedDate.DA_Id == Guid.Empty)
                {
                    uow.CommonDates.Add(updatedDate);
                }
                uow.Complete();
                if (errorListing != null)
                {
                    foreach (var error in errorListing)
                    {

                    }
                }

            }
            return RedirectToAction("Details", new { id = Request.Form["convertFileId"] });


        }

        private bool GetDateErrors(Guid dA_Id)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                var errorlist = uow.ErrorMessages.Find(x => x.EM_DateId == dA_Id && !x.EM_Acknowledged).ToList();
                if (errorlist.Count > 0)
                { return true; }
                return false;

            }
        }
    }
}