﻿using Doxin.Core.models;
using DoXin.Infrastructure.dto;
using DoXin.Infrastructure.interfaces;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace DoxIn.Web.Controllers
{
    public class CustomersController : Controller
    {
        private CommonEntity db = new CommonEntity();

        // GET: Customers
        public ActionResult Index()
        {
            return View(db.Customers.ToList());
        }

        // GET: Customers/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "CU_ID,CU_CODE,CU_NAME,CU_ACTIVE,CU_ADDRESS1,CU_ADDRESS2,CU_ADDRESS3,CU_CITY,CU_PHONE,CU_EMAIL,CU_INCOMINGCODE")] Customer customer)
        public ActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                {
                    uow.Customers.Add(customer);
                    uow.Complete();
                }

                //customer.CU_ID = Guid.NewGuid();
                //db.Customers.Add(customer);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customer);
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Customer customer)
        {
            if (ModelState.IsValid)
            {
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                {
                    var cust = uow.Customers.Get(customer.CU_ID);
                    if (cust != null)
                    {
                        cust.CU_ACTIVE = customer.CU_ACTIVE;
                        cust.CU_ADDRESS1 = customer.CU_ADDRESS1;
                        cust.CU_ADDRESS2 = customer.CU_ADDRESS2;
                        cust.CU_ADDRESS3 = customer.CU_ADDRESS3;
                        cust.CU_CITY = customer.CU_CITY;
                        cust.CU_CODE = customer.CU_CODE;
                        cust.CU_EMAIL = customer.CU_EMAIL;
                        cust.CU_INCOMINGCODE = customer.CU_INCOMINGCODE;
                        cust.CU_NAME = customer.CU_NAME;
                        cust.CU_PHONE = customer.CU_PHONE;
                        cust.CU_POSTCODE = customer.CU_POSTCODE;
                        cust.CU_STATE = customer.CU_STATE;
                        uow.Complete();
                    }
                }
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
