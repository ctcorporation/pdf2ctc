﻿using Doxin.Core.models;
using System;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DoxIn.Web.Controllers
{
    public class DateTypesController : Controller
    {
        private CommonEntity db = new CommonEntity();

        // GET: DateTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.DateTypes.ToListAsync());
        }

        // GET: DateTypes/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DateType dateType = await db.DateTypes.FindAsync(id);
            if (dateType == null)
            {
                return HttpNotFound();
            }
            return View(dateType);
        }

        // GET: DateTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DateTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DT_Id,DT_Name,DT_DateType")] DateType dateType)
        {
            if (ModelState.IsValid)
            {
                dateType.DT_Id = Guid.NewGuid();
                db.DateTypes.Add(dateType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(dateType);
        }

        // GET: DateTypes/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DateType dateType = await db.DateTypes.FindAsync(id);
            if (dateType == null)
            {
                return HttpNotFound();
            }
            return View(dateType);
        }

        // POST: DateTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DT_Id,DT_Name,DT_DateType")] DateType dateType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dateType).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(dateType);
        }

        // GET: DateTypes/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DateType dateType = await db.DateTypes.FindAsync(id);
            if (dateType == null)
            {
                return HttpNotFound();
            }
            return View(dateType);
        }

        // POST: DateTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            DateType dateType = await db.DateTypes.FindAsync(id);
            db.DateTypes.Remove(dateType);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
