﻿using Doxin.Core.models;
using DoxIn.Web.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DoxIn.Web.Controllers
{

    public class HomeController : Controller
    {
        #region members
        private CommonEntity _context;

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        #endregion

        public HomeController()
        {
            _context = new CommonEntity();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        #region properties

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().Get<ApplicationUserManager>();

            }
            set
            {
                _userManager = value;
            }
        }
        public ApplicationSignInManager CustomSignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }
        #endregion
        [Authorize]
        public async Task<ActionResult> Index()
        {
            if (User.IsInRole("DoXinAdmin"))
            {
                return RedirectToAction("Index", "Admin");

            }
            var user = await UserManager.FindByNameAsync(User.Identity.Name);
            var currentcompany = _context.Customers.Where(c => c.CU_CODE == user.CompanyCode).FirstOrDefault();
            if (currentcompany == null)
            {
                TempData["ErrorMessage"] = "Unable to find Company Code for your user. Please contact CTC Support";
                return RedirectToAction("AccessDenied", "Account");

            }
            Session["CompanyId"] = currentcompany.CU_ID;


            //UserManager.UpdateAsync(user);

            SummaryViewModel summary = new SummaryViewModel();
            summary.SummaryList = _context.ConvertedFiles
                                .Where(x => !x.FC_Converted
                                && x.FC_CustomerId == currentcompany.CU_ID)
                                .GroupBy(g => new { g.FC_DocumentIdentifier, g.FC_Released })
                                .Select(s => new Summary { DocumentType = s.Key.FC_DocumentIdentifier, Processed = s.Key.FC_Released, Total = s.Count() })
                                .ToList();



            return View(summary);
        }

        public ActionResult AdminIndex()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}