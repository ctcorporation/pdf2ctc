namespace DoxIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserTableAddCompanyCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CompanyCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "CompanyCode");
        }
    }
}
