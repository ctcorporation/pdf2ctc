namespace DoxIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserTableAddCompanyCodeMaxLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "CompanyCode", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "CompanyCode", c => c.String());
        }
    }
}
