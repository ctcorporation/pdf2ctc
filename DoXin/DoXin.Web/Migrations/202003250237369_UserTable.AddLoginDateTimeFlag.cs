namespace DoxIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserTableAddLoginDateTimeFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "LastLoginTime", c => c.DateTime());
            AddColumn("dbo.AspNetUsers", "IsLoggedIn", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsLoggedIn");
            DropColumn("dbo.AspNetUsers", "LastLoginTime");
        }
    }
}
