namespace DoxIn.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ASPUsersAddUserDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsLockedOut", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "FirstName", c => c.String(maxLength: 50));
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "LastName");
            DropColumn("dbo.AspNetUsers", "FirstName");
            DropColumn("dbo.AspNetUsers", "IsLockedOut");
        }
    }
}
