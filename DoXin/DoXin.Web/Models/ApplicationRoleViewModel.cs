﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace DoxIn.Web.Models
{
    public class ApplicationRoleViewModel
    {
        public IdentityRole Role { get; set; }
        public IEnumerable<ApplicationUser> Users { get; set; }

    }
}