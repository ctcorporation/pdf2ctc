﻿using Doxin.Core.models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace DoxIn.Web.Models
{
    public class ApplicationUserViewModel
    {
        public ApplicationUser User { get; set; }

        public IEnumerable<IdentityRole> Roles { get; set; }

        public Customer Customer { get; set; }

        public string Password { get; set; }
    }




}