﻿using Doxin.Core.models;
using System.Collections.Generic;

namespace DoxIn.Web.Models
{
    public class CommonInvoiceMasterDetail
    {
        public ConvertedFile File { get; set; }
        public CommonInvoice CommonInvoice { get; set; }
        public IEnumerable<CommonOrderLine> Lines { get; set; }
        public IEnumerable<CommonDate> Dates { get; set; }
        public IEnumerable<Company> Companies { get; set; }


    }
}