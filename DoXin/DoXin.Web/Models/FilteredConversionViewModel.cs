﻿using Doxin.Core.models;
using System.Collections.Generic;

namespace DoxIn.Web.Models
{
    public class FilteredConversionViewModel
    {
        public string DeletedFilter { get; set; }
        public string ProcessedFilter { get; set; }
        public string CompanyCode { get; set; }
        public IEnumerable<ConvertedFile> ConvertedFile { get; set; }
    }
}