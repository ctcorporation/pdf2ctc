﻿using System.Collections.Generic;

namespace DoxIn.Web.Models
{
    public class SummaryViewModel
    {
        public IEnumerable<Summary> SummaryList { get; set; }
        public int Total { get; set; }

    }

    public class Summary
    {
        public string DocumentType { get; set; }
        public bool Processed { get; set; }
        public int Total { get; set; }
    }
}