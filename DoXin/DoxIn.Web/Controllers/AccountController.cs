﻿using Doxin.Core.models;
using DoxIn.Web.Models;
using DoXin.Infrastructure.dto;
using DoXin.Infrastructure.interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DoxIn.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private CommonEntity _context;


        // private RoleManager<ApplicationRole> RoleManager;

        public AccountController()
        {
            _context = new CommonEntity();
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
        }



        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();

            }
            private set { _roleManager = value; }
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:

                    var userId = SignInManager.AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId();

                    ApplicationUser user = UserManager.FindById(userId);

                    if (user != null)
                    {
                        if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                        {
                            TempData["ErrorMessage"] = "You must have a confirmed email to log on.";
                            return RedirectToAction("AccessDenied");
                        }
                        if (!user.IsActive)
                        {
                            TempData["ErrorMessage"] = "Your account is disabled. Please Contact CTC Support for more information";
                            return RedirectToAction("AccessDenied");
                        }
                        user.IsLoggedIn = true;
                        user.LastLoginTime = DateTime.Now;
                        UserManager.Update(user);
                        FormsAuthentication.SetAuthCookie(user.UserName, false);

                    }
                    if (await UserManager.IsInRoleAsync(user.Id, "DoXinAdmin")) //<= Checking Role and redirecting accordingly.
                        return RedirectToAction("Index", "Admin");
                    else
                        return RedirectToAction("Index", "Home");

                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [Authorize(Roles = "DoXinAdmin")]
        public async Task<ActionResult> ListUsers()
        {


            List<ApplicationUserViewModel> userList = new List<ApplicationUserViewModel>();
            foreach (var user in UserManager.Users.ToList())
            {
                ApplicationUserViewModel userWithRoles = new ApplicationUserViewModel();
                userWithRoles.User = user;
                userWithRoles.Roles = await GetRoleNames(user.Roles);
                //  userRole.Roles = UserManager.GetRoles(user.Id).ToList();
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                {
                    userWithRoles.Customer = uow.Customers.Find(x => x.CU_CODE == user.CompanyCode).FirstOrDefault();
                }
                userList.Add(userWithRoles);

            }
            return View(userList);
        }


        private async Task<List<IdentityRole>> GetRoleNames(ICollection<IdentityUserRole> roles)
        {
            List<IdentityRole> rolesList = new List<IdentityRole>();
            foreach (var role in roles)
            {
                IdentityRole r = await RoleManager.FindByIdAsync(role.RoleId);
                rolesList.Add(r);
            }
            return rolesList;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult AccessDenied()
        {
            ViewBag.ErrorMessage = !string.IsNullOrEmpty(TempData["ErrorMessage"].ToString()) ? TempData["ErrorMessage"].ToString() : "Please Log in again. There was an error locating your user record.";
            return View();
        }

        public ActionResult AddRole()
        {
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            ViewBag.Operation = TempData["Operation"];
            ViewBag.Success = TempData["Success"];
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "DoXinAdmin")]
        public ActionResult AddRole(FormCollection forms)
        {
            if (string.IsNullOrEmpty(forms["Role.Name"]))
            {
                TempData["ErrorMessage"] = "Role Name Cannot be Blank";
                TempData["Operation"] = "Add new Role";
                TempData["Success"] = "false";
                return RedirectToAction("AddRole");
            }

            var role = RoleManager.FindByName(forms["Role.Name"]);
            if (role != null)
            {
                TempData["ErrorMessage"] = "Role already exists";
                TempData["Operation"] = "Add new Role";
                TempData["Success"] = "false";
                return RedirectToAction("AddRole");
            }
            RoleManager.Create(new IdentityRole
            {
                Name = forms["Role.Name"]
            });

            return RedirectToAction("ListRoles");
        }
        [HttpGet]
        public ActionResult ListRoles()
        {
            ViewBag.ErrorMessage = TempData["ErrorMessage"];
            ViewBag.Operation = TempData["Operation"];
            ViewBag.Success = TempData["Success"];
            var roles = RoleManager.Roles.ToList();
            List<RoleViewModel> rm = new List<RoleViewModel>();
            foreach (var role in roles)
            {
                rm.Add(new RoleViewModel
                {
                    Id = role.Id,
                    Name = role.Name
                });
            }
            return View(rm);
        }

        [HttpGet]
        public ActionResult EditRoles(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                TempData["ErrorMessage"] = "Unable to find Roles ID Value";
                TempData["Operation"] = "Failed to Edit Role";
                TempData["Success"] = "false";
                return RedirectToAction("ListRoles");

            }
            ApplicationRoleViewModel rvm = new ApplicationRoleViewModel();
            rvm.Role = RoleManager.FindById(id);
            rvm.Users = GetUsersInRole(id);
            return View(rvm);
        }

        [Authorize(Roles = "DoXinAdmin")]
        public async Task<ActionResult> DeleteRole(string id)
        {
            var role = await RoleManager.FindByIdAsync(id);
            var users = GetUsersInRole(role.Id);
            if (users.Any())
            {
                TempData["ErrorMessage"] = "Cannot delete Role while Users Exist";
                TempData["Operation"] = "Remove Role";
                TempData["Success"] = "false";

            }
            var result = await RoleManager.DeleteAsync(role);
            if (result.Succeeded)
            {
                TempData["ErrorMessage"] = "Role Deleted";
                TempData["Operation"] = "Remove Role";
                TempData["Success"] = "true";
            }
            else
            {
                TempData["ErrorMessage"] = "Error: Removing Role.";
                TempData["Operation"] = "Remove Role";
                TempData["Success"] = "false";
            }
            return RedirectToAction("ListRoles");
        }
        public IQueryable<ApplicationUser> GetUsersInRole(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }
            return (from u in UserManager.Users.ToList()
                    where u.Roles.Any(r => r.RoleId == id)
                    select u).AsQueryable();


        }


        //


        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                TempData["ErrorMessage"] = "User Account not Verified";
                return View("AccessDenied");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    TempData["ErrorMessage"] = "Your Account has been locked out. Please contact CTC Support for more help. ";
                    return RedirectToAction("AccessDenied");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            var customerList = _context.Customers.OrderBy(cu => cu.CU_CODE).ToList();
            ViewBag.CustomerList = new SelectList(customerList, "Key", "Value");
            var allRoles = (new ApplicationDbContext()).Roles.OrderBy(r => r.Name)
                .ToList()
                .Select(rr => new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList();
            ViewBag.Roles = allRoles;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UserDetails(ApplicationUser user, FormCollection formValues)
        {
            if (!formValues["Password"].Any(char.IsDigit))
            {
                ModelState.AddModelError("Password", "Password must be a minimum of 6 Characters and contain at least one Uppercase, one lowercase, one number and one specialcharacter ");
            }
            if (!formValues["Password"].Any(char.IsLower))
            {
                ModelState.AddModelError("Password", "Password must be a minimum of 6 Characters and contain at least one Uppercase, one lowercase, one number and one specialcharacter ");
            }
            if (!formValues["Password"].Any(char.IsUpper))
            {
                ModelState.AddModelError("Password", "Password must be a minimum of 6 Characters and contain at least one Uppercase, one lowercase, one number and one specialcharacter ");
            }
            if (!formValues["Password"].Any(char.IsLetterOrDigit))
            {
                ModelState.AddModelError("Password", "Password must be a minimum of 6 Characters and contain at least one Uppercase, one lowercase, one number and one specialcharacter ");
            }
            if (user.FirstName == user.LastName)
            {
                ModelState.AddModelError("FirstName", "First and Last Names cannot be the same");
            }

            if (!ModelState.IsValid)
            {
                var customerList = _context.Customers.OrderBy(cu => cu.CU_CODE).ToList();
                ViewBag.CustomerList = new SelectList(customerList, "Key", "Value");
                var allRoles = (new ApplicationDbContext()).Roles.OrderBy(r => r.Name)
                    .ToList()
                    .Select(rr => new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList();
                ViewBag.Roles = allRoles;
                ApplicationUser dbUser = await UserManager.FindByIdAsync(user.Id);
                ApplicationUserViewModel avm = new ApplicationUserViewModel
                {

                    User = user,
                    Password = formValues["Password"],
                    Roles = await GetRoleNames(dbUser.Roles)
                };

                return View(avm);
            }
            var updatedUser = UserManager.FindById(user.Id);
            if (!string.IsNullOrEmpty(Request["RoleList"]))
            {
                UserManager.AddToRole(user.Id, Request["RoleList"]);
            }
            updatedUser.FirstName = user.FirstName;
            updatedUser.LastName = user.LastName;
            updatedUser.CompanyCode = user.CompanyCode;
            updatedUser.IsActive = user.IsActive;
            updatedUser.IsLockedOut = user.IsLockedOut;
            updatedUser.Email = user.Email;
            updatedUser.UserName = user.UserName;
            updatedUser.PasswordHash = UserManager.PasswordHasher.HashPassword(formValues["Password"]);
            await UserManager.UpdateAsync(updatedUser);
            return RedirectToAction("ListUsers");
        }
        public async Task<ActionResult> UserDetails(string userid)
        {
            ApplicationUserViewModel userDetail = new ApplicationUserViewModel();
            var user = UserManager.FindById(userid);

            var customerList = _context.Customers.OrderBy(cu => cu.CU_CODE).ToList();
            ViewBag.CustomerList = new SelectList(customerList, "Key", "Value");
            var allRoles = (new ApplicationDbContext()).Roles.OrderBy(r => r.Name)
                .ToList()
                .Select(rr => new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList();
            ViewBag.Roles = allRoles;
            if (user != null)
            {

                userDetail.User = user;
                userDetail.Roles = await GetRoleNames(user.Roles);
                //  userRole.Roles = UserManager.GetRoles(user.Id).ToList();
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
                {
                    userDetail.Customer = uow.Customers.Find(x => x.CU_CODE == user.CompanyCode).FirstOrDefault();
                }
                return View(userDetail);
            }
            else
            {
                return View("Error");
            }
        }

        public ActionResult RemoveUserFromRole(string id, string userid)
        {
            var role = RoleManager.FindById(id);
            UserManager.RemoveFromRole(userid, role.Name);
            return RedirectToAction("EditRoles", new { id = role.Id });
        }

        public ActionResult RemoveRolesFromUser(string name, string userid)
        {
            UserManager.RemoveFromRole(userid, name);
            return RedirectToAction("UserDetails", new { userid = userid });
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [Authorize(Roles = "DoXinAdmin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            var currentUser = User.Identity;
            if (!model.Password.Any(char.IsDigit))
            {
                ModelState.AddModelError("Password", "Password must be a minimum of 6 Characters and contain at least one Uppercase, one lowercase, one number and one specialcharacter ");
            }
            if (!model.Password.Any(char.IsLower))
            {
                ModelState.AddModelError("Password", "Password must be a minimum of 6 Characters and contain at least one Uppercase, one lowercase, one number and one specialcharacter ");
            }
            if (!model.Password.Any(char.IsUpper))
            {
                ModelState.AddModelError("Password", "Password must be a minimum of 6 Characters and contain at least one Uppercase, one lowercase, one number and one specialcharacter ");
            }
            Regex regPass = new Regex("^[a-zA-Z0-9]*$");
            if (regPass.IsMatch(model.Password))
            {
                ModelState.AddModelError("Password", "Password must be a minimum of 6 Characters and contain at least one Uppercase, one lowercase, one number and one specialcharacter ");
            }
            if (model.FirstName == model.LastName)
            {
                ModelState.AddModelError("FirstName", "First and Last Names cannot be the same");
            }
            var u = UserManager.FindByEmailAsync(model.Email);
            if (u.Result != null)
            {
                ModelState.AddModelError("Email", "Email Address already exists.");
            }
            u = UserManager.FindByNameAsync(model.UserName);
            if (u.Result != null)
            {
                ModelState.AddModelError("UserName", "User already exists. ");
            }

            if (!ModelState.IsValid)
            {
                var customerList = _context.Customers.OrderBy(cu => cu.CU_CODE).ToList();
                ViewBag.CustomerList = new SelectList(customerList, "Key", "Value");
                var allRoles = (new ApplicationDbContext()).Roles.OrderBy(r => r.Name)
                    .ToList()
                    .Select(rr => new SelectListItem { Value = rr.Name.ToString(), Text = rr.Name }).ToList();
                ViewBag.Roles = allRoles;
                return View(model);
            }
            else
            {

                var user = new ApplicationUser
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    CompanyCode = model.CompanyCode,
                    IsActive = true,
                    IsLoggedIn = false,
                    IsLockedOut = false,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailConfirmed = true

                };
                var result = await UserManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                {
                    TempData["ErrorMessage"] = "Error Creating User." + result;
                    return RedirectToAction("AccessDenied");
                }
                else
                {
                    var uid = UserManager.FindByName(model.UserName);
                    if (uid != null)
                    {
                        if (!string.IsNullOrEmpty(Request["RoleList"]))
                        {
                            await UserManager.AddToRoleAsync(user.Id, Request["RoleList"]);
                        }
                    }

                    return RedirectToAction("ListUsers", "Account");
                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    //// For more information on how to enable account confirmation and password reset please visit https:////go.microsoft.com/fwlink/?LinkID=320771
                    //// Send an email with this link
                    //string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    //await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                }

            }

        }

        // If we got this far, something failed, redisplay form



        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("AccessDenied") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            try
            {
                ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
                if (user != null)
                {
                    user.IsLoggedIn = false;
                    UserManager.Update(user);
                }
            }
            catch (Exception ex)
            {

            }

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}