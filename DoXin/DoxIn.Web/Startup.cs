﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DoxIn.Web.Startup))]
namespace DoxIn.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
