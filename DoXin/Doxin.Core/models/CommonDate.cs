﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    public class CommonDate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DA_Id { get; set; }
        [MaxLength(20)]
        [Display(Name = "Est. Date")]
        [RegularExpression(@"(((0|1)[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$", ErrorMessage = "Invalid Date Format (dd/MM/yyyy).")]
        public string DA_EstimateDate { get; set; }
        [Display(Name = "Act. Date")]
        [RegularExpression(@"(((0|1)[0-9]|2[0-9]|3[0-1])\/(0[1-9]|1[0-2])\/((19|20)\d\d))$", ErrorMessage = "Invalid Date Format (dd/MM/yyyy).")]
        public string DA_ActualDate { get; set; }
        public bool DA_ErrorsFound { get; set; }
        [Display(Name = "Date Type")]
        public Guid? DA_DateTypeId { get; set; }
        public virtual DateType DA_DateType { get; set; }
        public IList<ErrorMessage> Errors { get; set; }
        public Guid DA_InvoiceId { get; set; }
        public virtual CommonInvoice DA_Invoice { get; set; }




    }
}
