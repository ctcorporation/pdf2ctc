﻿using System.Data.Entity;

namespace Doxin.Core.models
{
    public class CommonEntity : DbContext
    {
        // Your context has been configured to use a 'CommonEntity' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'PDF_2_CTC_Satellite.Models.CommonEntity' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'CommonEntity' 
        // connection string in the application configuration file.
        public CommonEntity()
            : base("name=CommonEntity")
        {
        }
        public CommonEntity(string connString)
            : base(connString)
        {

        }

        public virtual DbSet<ErrorMessage> ErrorMessages { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<ConvertedFile> ConvertedFiles { get; set; }
        public virtual DbSet<CommonInvoice> CommonInvoices { get; set; }
        public virtual DbSet<CommonDate> CommonDates { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CompanyType> CompanyTypes { get; set; }
        public virtual DbSet<WarehouseOrder> WarehouseOrders { get; set; }
        public virtual DbSet<DateType> DateTypes { get; set; }
        public virtual DbSet<CommonOrderLine> CommonLines { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
