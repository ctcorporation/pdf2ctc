﻿namespace Doxin.Core.models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    public class CommonInvoice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CI_ID { get; set; }
        [Display(Name = "Invoice No")]
        public string CI_InvoiceNo { get; set; }
        [Display(Name = "Shipment No")]
        public string CI_ShipmentNo { get; set; }
        [Display(Name = "Invoice Total")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,2})?$", ErrorMessage = "Please enter valid Dollar Value")]

        public string CI_TotalInvoiceAmount { get; set; }
        [Display(Name = "Currency")]
        [StringLength(3)]
        [RegularExpression(@"^[\s\S]{3,}$", ErrorMessage = "Currency value is Characters only (ie AUD)")]
        public string CI_Currency { get; set; }
        [Display(Name = "Country of Origin")]
        public string CI_CountryOfOrigin { get; set; }
        [Display(Name = "Invoice terms")]
        public string CI_InvoiceTerms { get; set; }
        public bool CI_ErrorsFound { get; set; }
        public Guid CI_ConvertedFileId { get; set; }
        public ConvertedFile CI_ConvertedFile { get; set; }
        public ICollection<CommonOrderLine> CI_InvoiceLines { get; set; }
        public ICollection<CommonDate> CI_Dates { get; set; }
        public ICollection<Company> CI_Companies { get; set; }
        public ICollection<ErrorMessage> CI_Errors { get; set; }
        public ICollection<CustomerReference> CI_CustReferences { get; set; }

    }


}
