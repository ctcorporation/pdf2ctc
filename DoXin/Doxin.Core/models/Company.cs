﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    [Table("Company")]
    public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CO_ID { get; set; }
        [Display(Name = "Name")]
        public string CO_Name { get; set; }
        [Display(Name = "Code")]
        public string CO_Code { get; set; }
        [Display(Name = "Address(1)")]
        public string CO_Address1 { get; set; }
        [Display(Name = "Address(2)")]
        public string CO_Address2 { get; set; }
        [Display(Name = "Address(3)")]
        public string CO_Address3 { get; set; }
        [Display(Name = "City")]
        public string CO_City { get; set; }
        [Display(Name = "Post Code")]
        public string CO_PostCode { get; set; }
        [Display(Name = "State")]
        public string CO_State { get; set; }
        [Display(Name = "Country")]
        public string CO_Country { get; set; }
        [Display(Name = "Active")]
        public bool CO_Active { get; set; }
        [Display(Name = "Contact")]
        public string CO_ContactName { get; set; }
        [Display(Name = "Email")]
        public string CO_EmailAddress { get; set; }

        public bool CO_ErrorsFound { get; set; }
        //Invoice Nav Props.
        public Guid CO_InvoiceId { get; set; }
        public CommonInvoice CO_Invoice { get; set; }
        //Company Type Nav Props (One)
        [Display(Name = "Company Type")]
        public Guid CO_CompanyTypeId { get; set; }
        public virtual CompanyType CO_CompanyType { get; set; }

        // Error Messages (Many)
        public IList<ErrorMessage> Errors { get; set; }


    }
}
