﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    public class CompanyType
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CT_ID { get; set; }

        [MaxLength(30)]
        [Display(Name = "Company Type")]
        public string CT_CompanyType { get; set; }

        //Company Nav Props (Many)
        public ICollection<Company> Companies { get; set; }



    }
}
