﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CU_ID { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Customer Code cannot be blank.")]
        [Display(Name = "Customer Code")]
        public string CU_CODE { get; set; }
        [StringLength(80, ErrorMessage = "Customer Code cannot be blank.")]
        [Display(Name = "Customer")]
        public string CU_NAME { get; set; }

        [Display(Name = "Is Active")]
        public bool CU_ACTIVE { get; set; }
        [MaxLength(80)]
        [Display(Name = "Address Line 1")]
        public string CU_ADDRESS1 { get; set; }
        [MaxLength(80)]
        [Display(Name = "Address Line 2")]
        public string CU_ADDRESS2 { get; set; }
        [MaxLength(80)]
        [Display(Name = "Address Line 3")]
        public string CU_ADDRESS3 { get; set; }
        [MaxLength(80)]
        [Display(Name = "City")]
        public string CU_CITY { get; set; }
        [MaxLength(20)]
        [Display(Name = "State")]
        public string CU_STATE { get; set; }
        [MaxLength(10)]
        [Display(Name = "Post Code")]
        public string CU_POSTCODE { get; set; }
        [MaxLength(20)]
        [Display(Name = "Phone")]
        public string CU_PHONE { get; set; }
        [MaxLength(80)]
        [Display(Name = "Main Email")]
        public string CU_EMAIL { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Map Code cannot be blank.")]
        [Display(Name = "Associated Map Code")]
        public string CU_INCOMINGCODE { get; set; }

        public ICollection<User> Users { get; set; }
        public ICollection<ConvertedFile> CU_CONVERTEDFILES { get; set; }


    }
}
