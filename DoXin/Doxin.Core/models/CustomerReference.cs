﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doxin.Core.models
{
    public class CustomerReference
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CR_ID { get; set; }
        public string CR_RefType { get; set; }
        public string CR_RefValue { get; set; }
        public Guid CR_ParentID { get; set; }
        [MaxLength(2)]
        public string CR_ParentKey { get; set; }

        public Guid CR_InvoiceID { get; set; }
        public CommonInvoice CR_Invoice { get; set; }



    }
}
