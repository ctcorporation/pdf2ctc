﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    public class DateType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DT_Id { get; set; }

        [Display(Name = "Date Type")]
        public string DT_Name { get; set; }
        [MaxLength(20)]
        [Display(Name = "Description")]
        public string DT_DateType { get; set; }

        IList<CommonDate> Dates { get; set; }
    }
}