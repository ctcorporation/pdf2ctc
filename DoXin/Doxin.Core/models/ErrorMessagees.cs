﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    public class ErrorMessage
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid EM_ID { get; set; }
        [MaxLength(50)]
        public string EM_Type { get; set; }
        [MaxLength(20)]
        public string EM_Level { get; set; }
        public string EM_Message { get; set; }
        public string EM_ExpectedValue { get; set; }
        public DateTime? EM_DateTime { get; set; }
        public string EM_ScannedValue { get; set; }
        public string EM_SuspectedValue { get; set; }
        [MaxLength(20)]
        public string EM_ConfidenceValue { get; set; }
        public string EM_TagName { get; set; }

        public Guid EM_ParentID { get; set; }
        [MaxLength(2)]
        public string EM_ParentKey { get; set; }
        public Guid EM_DateId { get; set; }
        public CommonDate EM_Date { get; set; }
        public Guid EM_InvoiceId { get; set; }
        public CommonInvoice EM_Invoice { get; set; }
        public Guid EM_CommonLineId { get; set; }
        public CommonOrderLine EM_CommonLine { get; set; }
        public Guid EM_CustomerReferenceId { get; set; }
        public CustomerReference EM_CustomerReference { get; set; }

        public Guid EM_CompanyId { get; set; }
        public Company Company { get; set; }
        public bool EM_Acknowledged { get; set; }






    }
}
