﻿namespace Doxin.Core.models
{
    public class TreeNode
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public bool children { get; set; }
    }
}
