﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    public class UserType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UT_Id { get; set; }
        [MaxLength(20)]
        public string UT_UserType { get; set; }
        [MaxLength(200)]
        public string UT_Description { get; set; }
        public ICollection<User> Users { get; set; }

    }
}
