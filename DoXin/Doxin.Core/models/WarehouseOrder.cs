﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    public class WarehouseOrder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid WO_Id { get; set; }

        public string WO_OrderNo { get; set; }

        public string WO_WarehouseCode { get; set; }

        public string WO_SpecialInstructions { get; set; }

        public IList<CustomerReference> CustomerReferences { get; set; }
        public IList<ErrorMessage> ErrorMessages { get; set; }
    }
}
