﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doxin.Core.models
{
    public class CommonOrderLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid OL_ID { get; set; }
        public Guid OL_ParentID { get; set; }

        // 2 Character Table Qualifier (ie CI or OH) 
        [MaxLength(2)]
        public string OL_ParentKey { get; set; }
        [Display(Name = "Line No.")]
        public string OL_LineNo { get; set; }
        [Display(Name = "Invoice Qty")]
        public string OL_InvoicedQty { get; set; }
        [Display(Name = "Ordered Qty")]
        public string OL_OrderedQty { get; set; }
        [Display(Name = "Unit Price")]
        public string OL_UnitPrice { get; set; }
        [Display(Name = "UOM")]
        public string OL_UnitOfMeasurement { get; set; }
        [Display(Name = "Tariff")]
        public string OL_Tariff { get; set; }
        [Display(Name = "Unit Weight")]
        public string OL_UnitWeight { get; set; }
        [Display(Name = "Wgt Unit")]
        public string OL_WeightUnit { get; set; }
        [Display(Name = "Volume")]
        public string OL_Volume { get; set; }
        [Display(Name = "Vol. Unit")]
        public string OL_VolumeUnit { get; set; }
        [Display(Name = "Line Total")]
        public string OL_LineTotal { get; set; }
        [Display(Name = "Pack Qty")]
        public string OL_PackageQty { get; set; }
        [Display(Name = "Unit")]
        public string OL_PackageUnit { get; set; }
        [Display(Name = "Code")]
        public string OL_ProductCode { get; set; }
        [Display(Name = "Barcode")]
        public string OL_Barcode { get; set; }
        [Display(Name = "Description")]
        public string OL_ProductDescription { get; set; }
        public bool OL_ErrorsFound { get; set; }
        public IList<ErrorMessage> Errors { get; set; }
        public Guid OL_InvoiceID { get; set; }
        public CommonInvoice OL_Invoice { get; set; }




    }
}
