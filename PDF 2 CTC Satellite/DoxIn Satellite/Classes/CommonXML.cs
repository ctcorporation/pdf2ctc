﻿using DoxIn_Satellite.DTO;
using DoxIn_Satellite.Models;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DoxIn_Satellite.Classes
{
    public class CommonXML
    {
        #region Members
        private string _connString;
        #endregion

        #region Properties
        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }
        #endregion

        #region Contructors
        public CommonXML(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region Methods
        public NodeFile GetXMLFile(string path)
        {
            XDocument xmlFile = XDocument.Load(path);
            var node = xmlFile.Element("NodeFile");
            if (node != null)
            {
                XmlSerializer xmlcommon = new XmlSerializer(typeof(NodeFile));
                using (StreamReader sr = new StreamReader(path))
                {
                    var nodeCommon = (NodeFile)xmlcommon.Deserialize(sr);
                    if (nodeCommon != null)
                    {
                        ImportData(nodeCommon);
                    }
                };

            }


            return null;
        }

        private void ImportData(NodeFile node)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connString)))
            {
                var convertedFile = new ConvertedFile();
             //   convertedFile.Customer = GetCustomer(node.IdendtityMatrix.)
            }
        }
        #endregion

        #region Helpers
        #endregion


    }
}
