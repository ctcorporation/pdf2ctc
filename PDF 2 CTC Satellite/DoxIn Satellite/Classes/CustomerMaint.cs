﻿
using DoxIn_Satellite.DTO;
using DoxIn_Satellite.Models;
using System.Collections.Generic;
using System.Linq;

namespace DoxIn_Satellite.Classes
{
    public class CustomerMaint
    {
        #region members

        #endregion

        #region properties
        public string ConnString { get; set; }
        #endregion

        #region constructors
        public CustomerMaint(string connstring)
        {
            ConnString = connstring;
        }
        #endregion

        #region methods
        public List<Customer> GetCustList(string name, string code)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                var custList = uow.Customers.Find(x => x.CU_NAME.Contains(name)
              && x.CU_CODE.Contains(code)).ToList();
                return custList;

            }

            return null;
        }
        #endregion

        #region helpers

        #endregion
    }
}
