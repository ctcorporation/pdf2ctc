﻿using System;

namespace DoxIn_Satellite.Classes
{
    public static class Extensions
    {
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T ToEnum<T>(this string value, T defaultValue)
        {
            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }
            try
            {
                switch (typeof(T).Name)
                {
                    case "DateTime":
                        DateTime dt;
                        if (DateTime.TryParse(value, out dt))
                        {
                            return (T)(object)dt;
                        }

                        break;
                    case "Decimal":
                        decimal d;
                        if (Decimal.TryParse(value, out d))
                        {
                            return (T)(object)d;
                        }

                        break;
                    case "Int32":
                        int i;
                        if (int.TryParse(value, out i))
                        {
                            return (T)(object)i;
                        }
                        break;
                    default:
                        break;
                }
                return (T)(object)Enum.Parse(typeof(T), value, true);
            }
            catch (Exception ex)
            {
                return defaultValue;
            }



        }
    }


}
