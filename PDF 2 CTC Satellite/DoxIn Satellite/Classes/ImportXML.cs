﻿using DoxIn_Satellite.DTO;
using DoxIn_Satellite.Models;
using DoxIn_Satellite.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace DoxIn_Satellite.Classes
{
    public class ImportXML
    {
        #region members
        string _summary = string.Empty;
        string _logType;
        string _connString;
        string _imagePath;
        string _errorMsg;
        List<ErrorElement> _importErrors;
        #endregion

        #region properties
        public string ConnString
        {
            get
            { return _connString; }
            set { _connString = value; }
        }

        public string XmlFile { get; set; }
        public string ImagePath
        {
            get
            { return _imagePath; }
            set
            { _imagePath = value; }
        }

        public string ErrorMessage
        {
            get
            { return _errorMsg; }
            set
            { _errorMsg = value; }
        }

        #endregion

        #region constructors 
        public ImportXML(string connstring, string xmlFile, string imagePath)
        {
            ConnString = connstring;
            XmlFile = xmlFile;
            ImagePath = imagePath;
        }
        #endregion

        #region methods

        public ConvertedFile ImportData(Models.NodeFile ctcFile)
        {
            _logType = "Import";
            if (ctcFile != null)
            {
                var cfId = ProcessIdentityMatrix(ctcFile.IdentityMatrix);
                if (cfId == null)
                {
                    return null;
                }

                if (ctcFile.Operation != null)
                {
                    string optype = string.Empty;
                    if (ctcFile.Operation.CommercialInvoices != null)
                    {
                        optype = "CI";
                        //  ProcessOperation( cfId, optype);
                        bool proceErrorsFound = ProcessCommercialInvoices(cfId, ctcFile.Operation.CommercialInvoices.ToList());
                        using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
                        {
                            var convertedFile = uow.ConvertedFiles.Get(cfId.FC_ID);
                            convertedFile.FC_ErrorsFound = proceErrorsFound;
                            uow.Complete();
                        }
                    }
                    if (cfId != null)
                    {
                        return cfId;
                    }
                }
            }
            return null;
        }

        public void MoveImageFile(ConvertedFile convertedFile, string fromPath)
        {
            if (!string.IsNullOrEmpty(convertedFile.FC_ImageFileLocation))
            {

                //var file = convertedFile.FC_OriginalFileName.Substring(0, convertedFile.FC_OriginalFileName.IndexOf(".rpa"));
                var file = convertedFile.FC_ImageFileLocation.Trim();
                FileInfo imagefile = new FileInfo(Path.Combine(fromPath, file));
                if (imagefile.Exists)
                {
                    DirectoryInfo di = new DirectoryInfo(Path.Combine(ImagePath, convertedFile.FC_CustomerId.ToString()));
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                    try
                    {
                        string imageFileName = imagefile.Name.Substring(0, imagefile.Name.IndexOf(imagefile.Extension));
                        string newFileName = imageFileName + imagefile.Extension;
                        if (File.Exists(Path.Combine(di.FullName, imageFileName + imagefile.Extension)))
                        {

                            int iFileCount = 1;
                            newFileName = imageFileName + "-" + iFileCount + imagefile.Extension;
                            while (File.Exists(Path.Combine(di.FullName, imageFileName + "-" + iFileCount + imagefile.Extension)))
                            {
                                iFileCount++;

                            }
                        }

                        File.Move(imagefile.FullName, Path.Combine(di.FullName, newFileName));

                        using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connString)))
                        {
                            var cf = uow.ConvertedFiles.Get(convertedFile.FC_ID);
                            {
                                if (cf != null)
                                {

                                    cf.FC_ImageFileLocation = "originals/" + cf.FC_CustomerId.ToString() + "/" + newFileName;

                                    cf.FC_FileType = imagefile.Extension.Contains('.') ? imagefile.Extension.Remove(0, 1) : imagefile.Extension;
                                    uow.Complete();
                                }

                            }
                        }
                    }
                    catch (IOException ex)
                    {

                    }
                    catch (Exception ex)
                    {
                        return;
                    }


                }

            }
        }

        private void CreateErrror()
        {

        }

        private bool ProcessCommercialInvoices(ConvertedFile cfID, List<NodeFileOperationCommercialInvoice> civs)
        {
            bool errorsFound = false;
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {

                foreach (var civ in civs)
                {
                    try
                    {
                        _importErrors = new List<ErrorElement>();
                        CommonInvoice ci = new CommonInvoice
                        {
                            CI_InvoiceNo = civ.InvoiceNo,
                            CI_TotalInvoiceAmount = civ.TotalInvoiceAmount,
                            CI_TotalInvoiceCalculated = civ.TotalInvoiceCalculated,
                            CI_Currency = civ.InvoiceCurrency,
                            CI_CountryOfOrigin = civ.CountryOfOrigin,
                            CI_InvoiceTerms = civ.Incoterms,
                            CI_ConvertedFileId = cfID.FC_ID
                        };
                        uow.CommonInvoices.Add(ci);
                        foreach (var error in _importErrors)
                        {
                            AddErrors(error, cfID.FC_ID, "I");
                        }
                        uow.Complete();
                        errorsFound = ProcessInvoiceDates(civ.Dates, ci);
                        ProcessCustomerReferences(civ.CustomerReferences, ci, "CI");
                        errorsFound = ProcessCompanies(civ.Companies, ci, "CI");
                        errorsFound = ProcessCommonOrderLines(civ.CommercialInvoiceLines, ci, "CIV");
                        if (errorsFound)
                        {
                            ci.CI_ErrorsFound = true;
                            uow.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        Log log = new Log();
                        log.LO_Date = DateTime.Now;
                        var m = MethodBase.GetCurrentMethod();
                        log.LO_LogType = _logType;
                        log.LO_Message = ex.Message;
                        log.LO_Operation = m.ReflectedType.Name + @"\" + m.Name;
                        uow.Logs.Add(log);
                        uow.Complete();
                    }


                    _summary += string.Format("{0:g} :", DateTime.Now) + "Commercial Invoice : " + civ.InvoiceNo + " Imported" + Environment.NewLine;
                }



            }
            return errorsFound;
        }

        private void AddErrors(ErrorElement error, Guid fC_ID, string errorParent)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {

                DateTime d;
                var newError = new ErrorMessage
                {
                    EM_Type = error.ErrorType,
                    EM_DateTime = DateTime.TryParse(error.ErrorDate, out d) ? d : DateTime.Now,
                    EM_Message = error.ErrorMessage,
                    EM_Level = error.ErrorLevel,
                    EM_ExpectedValue = error.ErrorLevel,
                    EM_TagName = error.ErrorTagName

                };
                switch (errorParent)
                {
                    case "I":
                        newError.EM_InvoiceId = fC_ID;
                        break;
                    case "D":
                        newError.EM_DateId = fC_ID;
                        break;
                    case "C":
                        newError.EM_CompanyId = fC_ID;
                        break;
                    case "L":
                        newError.EM_CommonLineId = fC_ID;
                        break;
                    case "R":
                        newError.EM_CustomerReferenceId = fC_ID;
                        break;
                }
                uow.ErrorMessages.Add(newError);

                uow.Complete();
            }
        }

        private CompanyType GetCompanyType(Guid typeId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                return uow.CompanyTypes.Get(typeId);
            }

        }

        private bool AllNull(object element)
        {
            var allStringPropertyValues =
        from property in element.GetType().GetProperties()
        where property.PropertyType == typeof(string) && property.CanRead
        select (string)property.GetValue(element);

            var allisnull = allStringPropertyValues.All(value => string.IsNullOrEmpty(value));
            return allisnull;

        }
        private bool IsNullOrEmpty(object value)
        {
            if (Object.ReferenceEquals(value, null))
                return true;
            var type = value.GetType();
            return type.IsValueType && Object.Equals(value, Activator.CreateInstance(type));
        }

        private void ProcessCustomerReferences(Models.CustomerReferenceElement[] customerReferences, CommonInvoice cI_ID, string parentKey)
        {
            if (customerReferences == null)
                return;
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                foreach (var custRef in customerReferences)
                {
                    if (AllNull(custRef))
                    {
                        continue;
                    }
                    if (!string.IsNullOrEmpty(custRef.RefValue))
                    {
                        CustomerReference cr = new CustomerReference
                        {
                            CR_InvoiceID = cI_ID.CI_ID,
                            CR_RefType = custRef.RefType,
                            CR_RefValue = custRef.RefValue

                        };
                        uow.CustomerReferences.Add(cr);
                    }

                }
                uow.Complete();
            }
        }
        private bool ProcessCompanies(Models.CompanyElement[] companies, CommonInvoice cI_IC, string parentKey)
        {
            bool errorsFound = false;
            if (companies == null)
            {
                return false;
            }
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                bool supplierfound = false;
                bool cneefound = false;
                foreach (var company in companies)
                {
                    if (AllNull(company))
                    {
                        continue;
                    }
                    Company co = new Company
                    {
                        //CompType = company.CompanyType,
                        CO_City = company.City,
                        CO_Address1 = company.Address1,
                        CO_Address2 = company.Address2,
                        CO_Address3 = company.Address3,
                        CO_Code = company.CompanyCode,
                        CO_ContactName = company.ContactName,
                        CO_Country = company.CountryCode,
                        CO_EmailAddress = company.EmailAddress,
                        CO_Name = company.CompanyName,
                        CO_PostCode = company.PostCode,
                        CO_State = company.State,
                        CO_InvoiceId = cI_IC.CI_ID,
                    };
                    Guid id;
                    if (Guid.TryParse(company.CompanyType, out id))
                    {
                        var comptype = GetCompanyType(id);
                        switch (comptype.CT_CompanyType)
                        {
                            case "Supplier":
                                supplierfound = true;
                                break;
                            case "Consignee":
                                cneefound = true;
                                break;
                        }
                        co.CO_CompanyTypeId = comptype.CT_ID;
                    }
                    else
                    {
                        co.CO_ErrorsFound = true;
                    }
                    uow.Companies.Add(co);
                    uow.Complete();
                    if (ProcessErrors(company.Errors, co))
                    {
                        co.CO_ErrorsFound = true;
                        uow.Complete();
                        errorsFound = true;
                    }
                }
                if (!supplierfound)
                {
                    _importErrors.Add(new ErrorElement
                    {
                        ErrorDate = DateTime.Now.ToString(),
                        ErrorExpectedValue = "Supplier Address",
                        ErrorLevel = "Information",
                        ErrorMessage = "No Supplier found in Companies",
                        ErrorType = "O11",
                        ErrorTagName = "CompanyType"
                    });
                    errorsFound = true;
                }
                if (!cneefound)
                {
                    _importErrors.Add(new ErrorElement
                    {
                        ErrorDate = DateTime.Now.ToString(),
                        ErrorExpectedValue = "Consignee Address",
                        ErrorLevel = "Information",
                        ErrorMessage = "No Consignee found in Companies",
                        ErrorType = "O11",
                        ErrorTagName = "CompanyType"
                    });
                    errorsFound = true;
                }
            }
            return errorsFound;
        }
        private bool ProcessInvoiceDates(Models.DateElement[] dates, CommonInvoice invoice)
        {
            bool errorsFound = false;
            if (dates == null)
                return false;

            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                foreach (var date in dates)
                {
                    if (AllNull(date))
                    {
                        continue;
                    }
                    DateTime dt;
                    CommonDate cd = new CommonDate
                    {

                        DA_ActualDate = date.ActualDate,
                        DA_EstimateDate = date.EstimateDate,
                        DA_InvoiceId = invoice.CI_ID
                    };
                    Guid id;
                    DateType datetype = new DateType();
                    if (Guid.TryParse(date.DateType, out id))
                    {
                        datetype = GetDateType(id);
                        cd.DA_DateTypeId = datetype.DT_Id;
                    }
                    else
                    {
                        cd.DA_ErrorsFound = true;
                    }


                    uow.CommonDates.Add(cd);
                    if (!string.IsNullOrEmpty(cd.DA_ActualDate))
                    {
                        if (!ValidateData<DateTime>(cd.DA_ActualDate, DateTime.Now))
                        {
                            AddErrors(new ErrorElement
                            {
                                ErrorDate = DateTime.Now.ToString(),
                                ErrorExpectedValue = "Date",
                                ErrorLevel = "Information",
                                ErrorMessage = "Invalid Date",
                                ErrorScannedValue = string.IsNullOrEmpty(cd.DA_ActualDate) ?
                                         !string.IsNullOrEmpty(cd.DA_EstimateDate) ? cd.DA_EstimateDate : string.Empty
                                         : cd.DA_ActualDate,
                                ErrorTagName = datetype.DT_DateType,
                                ErrorType = "D3"
                            }, cd.DA_Id, "D");
                        }
                    }
                    uow.Complete();
                    if (ProcessErrors(date.Errors, cd))
                    {
                        cd.DA_ErrorsFound = true;
                        uow.Complete();
                        errorsFound = true;
                    }
                }
            }
            return errorsFound;
        }



        private bool ProcessCommonOrderLines(Models.OrderLineElement[] lines, CommonInvoice civId, string commonType)
        {
            bool errorsFound = false;
            if (lines == null)
            {
                return false;
            }
            int lineNo = 1;
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                foreach (var line in lines)
                {
                    if (AllNull(line))
                    {
                        continue;
                    }
                    try
                    {
                        CommonOrderLine co = new CommonOrderLine();
                        co.OL_LineNo = line.LineNo;
                        co.OL_InvoicedQty = line.InvoiceQty;
                        co.OL_Tariff = line.Tariff;
                        co.OL_LineTotal = line.LineTotal;
                        co.OL_UnitWeight = line.Weight;
                        co.OL_Volume = line.Volume;
                        co.OL_VolumeUnit = line.VolumeUnit;
                        co.OL_InvoiceID = civId.CI_ID;
                        co.OL_ParentKey = "CI";
                        co.OL_UnitPrice = line.UnitPrice;
                        co.OL_OrderedQty = line.OrderQty;
                        co.OL_PackageQty = line.PackageQty;
                        co.OL_PackageUnit = line.PackageUnit;
                        co.OL_UnitOfMeasurement = line.UnitOfMeasure;
                        co.OL_WeightUnit = line.WeightUnit;
                        if (line.Product != null)
                        {
                            co.OL_ProductCode = line.Product.Code;
                            co.OL_Barcode = line.Product.Barcode;
                            co.OL_ProductDescription = line.Product.Description;
                        }

                        if (line.Errors != null)
                        {
                            //if (ProcessErrors(line.Errors, co))
                            co.OL_ErrorsFound = true;
                                //uow.Complete();
                            errorsFound = true;
                        }

                        uow.CommonOrderLines.Add(co);
                        ValidateLineValues(co);


                        uow.Complete();
                        
                        lineNo++;
                    }
                    catch (Exception ex)
                    {
                        var st = new StackTrace();
                        var sf = st.GetFrame(0);

                        var currentMethodName = sf.GetMethod().Name;
                        Console.WriteLine("Error adding Common Line: Parent=" + civId.ToString() + " Line No=" + lineNo + ": Stack:" + st);
                    }

                }

            }
            return errorsFound;

        }

        private void ValidateLineValues(CommonOrderLine co)
        {
            if (!ValidateData<int>(co.OL_LineNo, 0))
            {
                AddErrors(new ErrorElement
                {
                    ErrorDate = DateTime.Now.ToString(),
                    ErrorExpectedValue = "Number",
                    ErrorLevel = "Critical",
                    ErrorMessage = "Invalid Number",
                    ErrorScannedValue = co.OL_LineNo,
                    ErrorTagName = "Line No",
                    ErrorType = "N3"
                }, co.OL_ID, "L");
            }
            if (!string.IsNullOrEmpty(co.OL_InvoicedQty))
            {
                if (!ValidateData<decimal>(co.OL_InvoicedQty, 0))
                {
                    AddErrors(new ErrorElement
                    {
                        ErrorDate = DateTime.Now.ToString(),
                        ErrorExpectedValue = "Number",
                        ErrorLevel = "Critical",
                        ErrorMessage = "Invalid Number",
                        ErrorScannedValue = co.OL_InvoicedQty,
                        ErrorTagName = "Invoice Qty",
                        ErrorType = "N3"
                    }, co.OL_ID, "L");
                }
            }
            if (!string.IsNullOrEmpty(co.OL_OrderedQty))
            {
                if (!ValidateData<decimal>(co.OL_OrderedQty, 0))
                {
                    AddErrors(new ErrorElement
                    {
                        ErrorDate = DateTime.Now.ToString(),
                        ErrorExpectedValue = "Number",
                        ErrorLevel = "Critical",
                        ErrorMessage = "Invalid Number",
                        ErrorScannedValue = co.OL_OrderedQty,
                        ErrorTagName = "Order Qty",
                        ErrorType = "N3"
                    }, co.OL_ID, "L");
                }
            }
            if (!string.IsNullOrEmpty(co.OL_UnitPrice))
            {
                if (!ValidateData<decimal>(co.OL_UnitPrice, 0))
                {
                    AddErrors(new ErrorElement
                    {
                        ErrorDate = DateTime.Now.ToString(),
                        ErrorExpectedValue = "Number",
                        ErrorLevel = "Critical",
                        ErrorMessage = "Invalid Number",
                        ErrorScannedValue = co.OL_UnitPrice,
                        ErrorTagName = "Unit Price",
                        ErrorType = "N3"
                    }, co.OL_ID, "L");
                }
            }
            if (!string.IsNullOrEmpty(co.OL_LineTotal))
            {
                if (!ValidateData<decimal>(co.OL_LineTotal, 0))
                {
                    AddErrors(new ErrorElement
                    {
                        ErrorDate = DateTime.Now.ToString(),
                        ErrorExpectedValue = "Number",
                        ErrorLevel = "Critical",
                        ErrorMessage = "Invalid Number",
                        ErrorScannedValue = co.OL_LineTotal,
                        ErrorTagName = "Invoice Line Total",
                        ErrorType = "N3"
                    }, co.OL_ID, "L");
                }
            }
            if (!string.IsNullOrEmpty(co.OL_Volume))
            {
                if (!ValidateData<decimal>(co.OL_Volume, 0))
                {
                    AddErrors(new ErrorElement
                    {
                        ErrorDate = DateTime.Now.ToString(),
                        ErrorExpectedValue = "Number",
                        ErrorLevel = "Critical",
                        ErrorMessage = "Invalid Number",
                        ErrorScannedValue = co.OL_Volume,
                        ErrorTagName = "Volume",
                        ErrorType = "N3"
                    }, co.OL_ID, "L");
                }
            }
            if (!string.IsNullOrEmpty(co.OL_PackageQty))
            {
                if (!ValidateData<decimal>(co.OL_PackageQty, 0))
                {
                    AddErrors(new ErrorElement
                    {
                        ErrorDate = DateTime.Now.ToString(),
                        ErrorExpectedValue = "Number",
                        ErrorLevel = "Critical",
                        ErrorMessage = "Invalid Number",
                        ErrorScannedValue = co.OL_PackageQty,
                        ErrorTagName = "Package Qty",
                        ErrorType = "N3"
                    }, co.OL_ID, "L");
                }
            }

        }

        private bool ProcessErrors(ErrorElement[] errors, CommonDate date)
        {
            bool errorsFound = false;
            if (errors == null)
            {
                return false;
            }
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                DateTime dt;
                if (errors == null)
                {
                    return false;
                }
                foreach (var error in errors)
                {
                    if (AllNull(error))
                    {
                        continue;
                    }
                    ErrorMessage er = new ErrorMessage
                    {
                        EM_DateId = date.DA_Id,
                        EM_DateTime = DateTime.TryParse(error.ErrorDate, out dt) ? dt : DateTime.MinValue,
                        EM_ConfidenceValue = error.ErrorConfidenceLevelValue.Truncate(20),
                        EM_ExpectedValue = error.ErrorExpectedValue,
                        EM_Level = error.ErrorLevel.Truncate(20),
                        EM_Message = error.ErrorMessage,
                        EM_ScannedValue = error.ErrorScannedValue,
                        EM_SuspectedValue = error.ErrorSuspectedValue,
                        EM_TagName = error.ErrorTagName,
                        EM_Type = error.ErrorType.Truncate(50)
                    };
                    uow.ErrorMessages.Add(er);
                    errorsFound = true;
                }
                uow.Complete();

            }
            return errorsFound;
        }
        private bool ProcessErrors(ErrorElement[] errors, CommonOrderLine line)
        {
            bool errorsFound = false;
            if (errors == null)
            {
                return false;
            }
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                DateTime dt;
                if (errors == null)
                {
                    return false;
                }
                foreach (var error in errors)
                {
                    if (AllNull(error))
                    {
                        continue;
                    }
                    ErrorMessage er = new ErrorMessage
                    {
                        EM_CommonLineId = line.OL_ID,
                        EM_DateTime = DateTime.TryParse(error.ErrorDate, out dt) ? dt : DateTime.MinValue,
                        EM_ConfidenceValue = error.ErrorConfidenceLevelValue.Truncate(20),
                        EM_ExpectedValue = error.ErrorExpectedValue,
                        EM_Level = error.ErrorLevel.Truncate(20),
                        EM_Message = error.ErrorMessage,
                        EM_ScannedValue = error.ErrorScannedValue,
                        EM_SuspectedValue = error.ErrorSuspectedValue,
                        EM_TagName = error.ErrorTagName,
                        EM_Type = error.ErrorType.Truncate(50)
                    };
                    uow.ErrorMessages.Add(er);
                    errorsFound = true;
                }
                uow.Complete();

            }
            return errorsFound;
        }
        private bool ProcessErrors(ErrorElement[] errors, CommonInvoice invoice)
        {
            bool errorsFound = false;
            if (errors == null)
            {
                return false;
            }
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                DateTime dt;
                if (errors == null)
                {
                    return false;
                }
                foreach (var error in errors)
                {
                    if (AllNull(error))
                    {
                        continue;
                    }
                    ErrorMessage er = new ErrorMessage
                    {
                        EM_InvoiceId = invoice.CI_ID,
                        EM_DateTime = DateTime.TryParse(error.ErrorDate, out dt) ? dt : DateTime.MinValue,
                        EM_ConfidenceValue = error.ErrorConfidenceLevelValue.Truncate(20),
                        EM_ExpectedValue = error.ErrorExpectedValue,
                        EM_Level = error.ErrorLevel.Truncate(20),
                        EM_Message = error.ErrorMessage,
                        EM_ScannedValue = error.ErrorScannedValue,
                        EM_SuspectedValue = error.ErrorSuspectedValue,
                        EM_TagName = error.ErrorTagName,
                        EM_Type = error.ErrorType.Truncate(50)
                    };
                    uow.ErrorMessages.Add(er);
                }
                uow.Complete();
                errorsFound = true;
            }
            return errorsFound;
        }
        private bool ProcessErrors(ErrorElement[] errors, CustomerReference custRef)
        {
            bool errorsFound = false;
            if (errors == null)
            {
                return false;
            }
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                DateTime dt;
                if (errors == null)
                {
                    return false;
                }
                foreach (var error in errors)
                {
                    if (AllNull(error))
                    {
                        continue;
                    }
                    ErrorMessage er = new ErrorMessage
                    {
                        EM_CustomerReferenceId = custRef.CR_ID,
                        EM_DateTime = DateTime.TryParse(error.ErrorDate, out dt) ? dt : DateTime.MinValue,
                        EM_ConfidenceValue = error.ErrorConfidenceLevelValue.Truncate(20),
                        EM_ExpectedValue = error.ErrorExpectedValue,
                        EM_Level = error.ErrorLevel.Truncate(20),
                        EM_Message = error.ErrorMessage,
                        EM_ScannedValue = error.ErrorScannedValue,
                        EM_SuspectedValue = error.ErrorSuspectedValue,
                        EM_TagName = error.ErrorTagName,
                        EM_Type = error.ErrorType.Truncate(50)
                    };
                    uow.ErrorMessages.Add(er);
                    errorsFound = true;
                }
                uow.Complete();

            }
            return errorsFound;
        }
        private bool ProcessErrors(ErrorElement[] errors, Company company)
        {
            bool errorsFound = false;
            if (errors == null)
            {
                return false;
            }
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                DateTime dt;
                if (errors == null)
                {
                    return false;
                }
                foreach (var error in errors)
                {
                    if (AllNull(error))
                    {
                        continue;
                    }
                    ErrorMessage er = new ErrorMessage
                    {
                        EM_CustomerReferenceId = company.CO_ID,
                        EM_DateTime = DateTime.TryParse(error.ErrorDate, out dt) ? dt : DateTime.MinValue,
                        EM_ConfidenceValue = error.ErrorConfidenceLevelValue.Truncate(20),
                        EM_ExpectedValue = error.ErrorExpectedValue,
                        EM_Level = error.ErrorLevel.Truncate(20),
                        EM_Message = error.ErrorMessage,
                        EM_ScannedValue = error.ErrorScannedValue,
                        EM_SuspectedValue = error.ErrorSuspectedValue,
                        EM_TagName = error.ErrorTagName,
                        EM_Type = error.ErrorType.Truncate(50)
                    };
                    uow.ErrorMessages.Add(er);
                    errorsFound = true;
                }
                uow.Complete();

            }
            return errorsFound;
        }

        private ConvertedFile ProcessIdentityMatrix(NodeFileIdentityMatrix id)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
            {
                FileInfo fi = new FileInfo(XmlFile);

                ConvertedFile cf = new ConvertedFile();

                cf.FC_DateReceived = DateTime.Now;
                cf.FC_DocumentIdentifier = id.DocumentIdentifier;
                cf.FC_DocumentType = id.DocumentType;
                cf.FC_EventCode = id.EventCode;
                cf.FC_OriginalFileName = id.OriginalFileName;
                cf.FC_FlagForDelete = false;
                cf.FC_Released = false;
                cf.FC_FlagForDelete = false;
                cf.FC_Converted = false;
                cf.FC_FileName = fi.Name;
                cf.FC_SenderID = id.SenderId;
                cf.FC_ImageFileLocation = id.ImageFile;
                cf.FC_ScanTemplateID = id.TemplateID;
                var cust = GetCustomer(id.CustomerId);
                if (cust == null)
                {
                    _errorMsg += "There is no Customer profile found for (" + id.CustomerId + "). Deleting file";
                    return null;
                }
                cf.FC_CustomerId = cust.CU_ID;
                uow.ConvertedFiles.Add(cf);
                uow.Complete();
                return cf;
            }
        }

        private NodeFileIdentityMatrix ProcessIdentityMatrixXML(XElement ident)
        {
            try
            {
                NodeFileIdentityMatrix id = new NodeFileIdentityMatrix
                {
                    CustomerId = ident.NodeExists("CustomerId"),
                    DocumentIdentifier = ident.NodeExists("DocumentIdentifier"),
                    DocumentType = ident.NodeExists("DocumentType"),
                    EventCode = ident.NodeExists("EventCode"),
                    FileDateTime = ident.NodeExists("FileDateTime"),
                    ImageFile = ident.NodeExists("FileId"),
                    OriginalFileName = ident.NodeExists("OriginalFileName"),
                    PurposeCode = ident.NodeExists("PurposeCode"),
                    SenderId = ident.NodeExists("SenderId"),
                    TemplateID = ident.NodeExists("TemplateID")
                };
                return id;
            }
            catch (Exception ex)
            {

            }
            return null;

        }

        public Models.NodeFile ConvertXml(string xmlFile)
        {
            _logType = "Import";
            try
            {
                Models.NodeFile ctcFile = new Models.NodeFile();
                XDocument xmldoc = XDocument.Load(xmlFile);
                if (xmldoc.Root.Name != "NodeFile")
                {
                    Exception ex = new Exception("Invalid File Found. Missing NodeFile Element");
                    ex.Data.Add("Value", xmldoc.Root.Name);
                    ex.Data.Add("FieldName", string.Empty);
                    _summary += string.Format("{0:g} :", DateTime.Now) + "Invalid File Found. Missing NodeFile Element" + Environment.NewLine;
                    throw ex;

                }
                var ident = XmlHelper.GetXElement(xmldoc, "NodeFile", "IdentityMatrix");
                if (ident != null)
                {
                    var id = ProcessIdentityMatrixXML(ident);
                    if (id == null)
                    {
                        Exception ex = new Exception("Error Processing ID Matrix.");
                        ex.Data.Add("Value", ident);
                        ex.Data.Add("FieldName", string.Empty);
                        _summary += string.Format("{0:g} :", DateTime.Now) + "Error processing ID Matrix" + Environment.NewLine;
                        throw ex;
                    }

                    ctcFile.IdentityMatrix = id;
                    ctcFile.IdentityMatrix.FileId = xmlFile;

                    var civList = XmlHelper.GetXElement(xmldoc, "NodeFile", "Operation");
                    if (civList == null)
                    {
                        Exception ex = new Exception("Error Processing Operation Element.");
                        ex.Data.Add("Value", "Invalid Data");
                        ex.Data.Add("FieldName", string.Empty);
                        _summary += string.Format("{0:g} :", DateTime.Now) + "Error processing Operation Element" + Environment.NewLine;
                        throw ex;
                    }

                    ctcFile.Operation = new NodeFileOperation
                    {
                        CommercialInvoices = ProcessCommercialInvoicesXML(civList)
                    };


                    return ctcFile;
                }

            }
            catch (Exception ex)
            {
                _errorMsg += ex.Message;
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connString)))
                {

                    Log log = new Log();
                    log.LO_Date = DateTime.Now;
                    var m = MethodBase.GetCurrentMethod();
                    log.LO_LogType = _logType;
                    log.LO_Message = ex.Message;

                    log.LO_Operation = m.ReflectedType.Name + @"\" + m.Name;
                    uow.Logs.Add(log);
                    uow.Complete();
                }
            }
            return null;
        }

        private NodeFileOperationCommercialInvoice[] ProcessCommercialInvoicesXML(XElement civList)
        {

            List<string> listNodes = new List<string>() { "CommercialInvoices", "Companies", "Dates" };
            dynamic xmlContent = new ExpandoObject();
            ExpandoObjectHelper.Parse(xmlContent, civList);
            List<NodeFileOperationCommercialInvoice> commercialInvoices = new List<NodeFileOperationCommercialInvoice>();

            foreach (var ci in xmlContent.Operation.CommercialInvoices)
            {
                NodeFileOperationCommercialInvoice newCI = new NodeFileOperationCommercialInvoice();
                foreach (var ciValues in ci.Value)
                {
                    try
                    {

                        switch (ciValues.Key)
                        {
                            case "InvoiceNo":
                                newCI.InvoiceNo = ciValues.Value;
                                break;
                            case "ShipmentNo":
                                newCI.ShipmentNo = ciValues.Value;
                                break;
                            case "TotalInvoiceAmount":
                                newCI.TotalInvoiceAmount = ciValues.Value;
                                break;
                            case "CalcTotalInvoiceAmount":
                                newCI.TotalInvoiceCalculated = ciValues.Value;
                                break;
                            case "InvoiceCurrency":
                                newCI.InvoiceCurrency = ciValues.Value;
                                break;
                            case "CountryOfOrigin":
                                newCI.CountryOfOrigin = ciValues.Value;
                                break;
                            case "Incoterms":
                                newCI.Incoterms = ciValues.Value;
                                break;
                            case "Companies":
                                newCI.Companies = ProcessDynamicCompanies(ciValues.Value);
                                break;
                            case "Dates":
                                newCI.Dates = ProcessDynamicDates(ciValues.Value);
                                break;
                            case "CustomerReferences":
                                newCI.CustomerReferences = ProcessDynamiceCustReferences(ciValues.Value);
                                break;
                            case "CommercialInvoiceLines":
                                newCI.CommercialInvoiceLines = ProcessDynamicLines(ciValues.Value);
                                break;
                            case "Errors":
                                newCI.Errors = ProcessDynamicErrors(ciValues.Value);
                                break;

                        }

                    }
                    catch (Exception ex)
                    {
                        using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connString)))
                        {
                            Log log = new Log();
                            log.LO_Date = DateTime.Now;
                            var m = MethodBase.GetCurrentMethod();
                            log.LO_LogType = _logType;
                            log.LO_Message = ex.Message;
                            log.LO_Operation = m.ReflectedType.Name + @"\" + m.Name;
                            uow.Logs.Add(log);
                            uow.Complete();
                        }
                    }


                }
                commercialInvoices.Add(newCI);
            }
            return commercialInvoices.ToArray();

        }




        #endregion

        #region helpers

        private bool ValidateData<DataType>(string data, dynamic defaultVal)
        {
            var val = Extensions.ToEnum<DataType>(data, defaultVal);
            if (typeof(DataType).Name == "Int32" || typeof(DataType).Name == "Decimal")

            {
                if (val == 0)
                {
                    if (data == "0")
                    {
                        val = -1;
                    }

                }
            }

            return val != defaultVal;
        }
        private Models.Customer GetCustomer(string customerId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(Globals.SattConnManager.ConnString)))
            {
                return uow.Customers.Find(x => x.CU_INCOMINGCODE == customerId).FirstOrDefault();


            }
        }

        private decimal ProcessDynamicDecimal(dynamic dynDecimal)
        {
            throw new NotImplementedException();
        }

        private Models.CompanyElement[] ProcessDynamicCompanies(dynamic dyncomp)
        {

            List<Models.CompanyElement> companies = new List<Models.CompanyElement>();
            bool supplierfound = false;
            bool cneefound = false;
            foreach (var dyn in dyncomp)
            {
                foreach (var dynCompany in dyn.Value)
                {
                    Models.CompanyElement newCompany = new Models.CompanyElement();
                    bool nullComp = true;
                    foreach (var compValues in dynCompany)
                    {

                        switch (compValues.Key)
                        {
                            case "CompanyName":
                                newCompany.CompanyName = compValues.Value;
                                break;
                            case "CompanyCode":
                                newCompany.CompanyCode = compValues.Value;
                                break;
                            case "City":
                                newCompany.City = compValues.Value;
                                break;
                            case "CompanyOrgCode":
                                newCompany.CompanyOrgCode = compValues.Value;
                                break;
                            case "CompanyType":
                                switch (compValues.Value)
                                {
                                    case "Supplier":
                                        supplierfound = true;
                                        break;
                                    case "Consignee":
                                        cneefound = true;
                                        break;
                                }
                                var compType = GetCompType(compValues.Value);

                                newCompany.CompanyType = compType != Guid.Empty ? compType.ToString() : null;
                                break;
                            case "Address1":
                                newCompany.Address1 = compValues.Value;
                                break;
                            case "Address2":
                                newCompany.Address2 = compValues.Value;
                                break;
                            case "Address3":
                                newCompany.Address3 = compValues.Value;
                                break;
                            case "PostCode":
                                newCompany.PostCode = compValues.Value;
                                break;
                            case "State":
                                newCompany.State = compValues.Value;
                                break;
                            case "Country":
                                newCompany.Country = compValues.Value;
                                break;
                            case "CountryCode":
                                newCompany.CountryCode = compValues.Value;
                                break;
                            case "PhoneNo":
                                newCompany.PhoneNo = compValues.Value;
                                break;
                            case "EmailAddress":
                                newCompany.EmailAddress = compValues.Value;
                                break;
                            case "Errors":
                                newCompany.Errors = ProcessDynamicErrors(compValues.Value);
                                break;
                        }

                    }
                    companies.Add(newCompany);
                }
            }
            if (!supplierfound)
            {
                ErrorElement error = new ErrorElement();
                error.ErrorDate = DateTime.Now.ToString();
                error.ErrorMessage = "No Supplier Found.";
                error.ErrorExpectedValue = "Company";
                error.ErrorTagName = "CompanyType";
                error.ErrorType = "O11";
                error.ErrorLevel = "Information";

            }
            return companies.ToArray();
        }
        private Models.OrderLineElement[] ProcessDynamicLines(dynamic dynLines)
        {
            bool isSingle = false;
            List<Models.OrderLineElement> lines = new List<Models.OrderLineElement>();
            foreach (var lineExpand in dynLines)
            {
                Models.OrderLineElement newLine = new Models.OrderLineElement();
                foreach (var lineObj in lineExpand.Value)
                {

                    try
                    {
                        if (lineObj.Key == "LineNo")
                        {
                            isSingle = true;
                        }

                        try
                        {
                            switch (lineObj.Key)
                            {
                                case "LineNo":
                                    newLine.LineNo = lineObj.Value;
                                    break;
                                case "InvoiceQty":
                                    newLine.InvoiceQty = lineObj.Value;
                                    break;
                                case "OrderQty":
                                    newLine.OrderQty = lineObj.Value;
                                    break;
                                case "LineTotal":
                                    newLine.LineTotal = lineObj.Value;
                                    break;
                                case "PackageQty":
                                    newLine.PackageQty = lineObj.Value;
                                    break;
                                case "PackageUnit":
                                    newLine.PackageUnit = lineObj.Value;
                                    break;
                                case "Product":
                                    newLine.Product = ProcessDynamicProduct(lineObj.Value);
                                    break;
                                case "Tariff":
                                    newLine.Tariff = lineObj.Value;
                                    break;
                                case "UnitOfMeasure":
                                    newLine.UnitOfMeasure = lineObj.Value;
                                    break;
                                case "UnitPrice":
                                    newLine.UnitPrice = lineObj.Value;
                                    break;
                                case "Volume":
                                    newLine.Volume = lineObj.Value;
                                    break;
                                case "VolumeUnit":
                                    newLine.VolumeUnit = lineObj.Value;
                                    break;
                                case "Weight":
                                    newLine.Weight = lineObj.Value;
                                    break;
                                case "WeightUnit":
                                    newLine.WeightUnit = lineObj.Value;
                                    break;
                                case "Errors":
                                    newLine.Errors = ProcessDynamicErrors(lineObj.Value);
                                    break;

                            }
                            if (lines.Count == 127)
                            {
                                string here = "";
                            }
                        }
                        catch (Exception ex2)
                        {
                            string strex = ex2.Message;
                        }


                    }
                    catch (Exception ex)
                    {
                        newLine = new Models.OrderLineElement();
                        foreach (var lineValues in lineObj)
                        {
                            try
                            {
                                switch (lineValues.Key)
                                {
                                    case "LineNo":
                                        newLine.LineNo = lineValues.Value;
                                        break;
                                    case "InvoiceQty":
                                        newLine.InvoiceQty = lineValues.Value;
                                        break;
                                    case "OrderQty":
                                        newLine.OrderQty = lineValues.Value;
                                        break;
                                    case "LineTotal":
                                        newLine.LineTotal = lineValues.Value;
                                        break;
                                    case "PackageQty":
                                        newLine.PackageQty = lineValues.Value;
                                        break;
                                    case "PackageUnit":
                                        newLine.PackageUnit = lineValues.Value;
                                        break;
                                    case "Product":
                                        newLine.Product = ProcessDynamicProduct(lineValues.Value);
                                        break;
                                    case "Tariff":
                                        newLine.Tariff = lineValues.Value;
                                        break;
                                    case "UnitOfMeasure":
                                        newLine.UnitOfMeasure = lineValues.Value;
                                        break;
                                    case "UnitPrice":
                                        newLine.UnitPrice = lineValues.Value;
                                        break;
                                    case "Volume":
                                        newLine.Volume = lineValues.Value;
                                        break;
                                    case "VolumeUnit":
                                        newLine.VolumeUnit = lineValues.Value;
                                        break;
                                    case "Weight":
                                        newLine.Weight = lineValues.Value;
                                        break;
                                    case "WeightUnit":
                                        newLine.WeightUnit = lineValues.Value;
                                        break;
                                    case "Errors":
                                        newLine.Errors = ProcessDynamicErrors(lineValues.Value);
                                        break;

                                }
                                if (lines.Count == 127)
                                {
                                    string here = "";
                                }
                            }
                            catch (Exception ex2)
                            {
                                string strex = ex2.Message;
                            }
                        }
                        lines.Add(newLine);
                    }
                }

                if (isSingle)
                {
                    lines.Add(newLine);
                }

            }

            return lines.ToArray();
        }
        private Models.ProductElement ProcessDynamicProduct(dynamic dynProd)
        {
            Models.ProductElement newProduct = new Models.ProductElement();
            foreach (var prod in dynProd)
            {

                switch (prod.Key)
                {
                    case "Code":
                        newProduct.Code = prod.Value;
                        break;
                    case "Barcode":
                        newProduct.Barcode = prod.Value;
                        break;
                    case "Description":
                        newProduct.Description = prod.Value;
                        break;

                }


            }
            return newProduct;
        }
        private Models.CustomerReferenceElement[] ProcessDynamiceCustReferences(dynamic dynCustRef)
        {
            List<Models.CustomerReferenceElement> custRefs = new List<Models.CustomerReferenceElement>();
            foreach (var custRefExpand in dynCustRef)
            {
                Models.CustomerReferenceElement newCustRef = new Models.CustomerReferenceElement();
                foreach (var custRefValues in custRefExpand.Value)
                {
                    switch (custRefValues.Key)
                    {
                        case "RefType":
                            newCustRef.RefType = custRefValues.Value;
                            break;
                        case "RefValue":
                            newCustRef.RefValue = custRefValues.Value;
                            break;
                        case "Errors":
                            newCustRef.Errors = ProcessDynamicErrors(custRefValues.Value);
                            break;
                    }

                }
                custRefs.Add(newCustRef);

            }
            return custRefs.ToArray();

        }

        private Models.DateElement[] ProcessDynamicDates(dynamic dynDates)
        {
            List<Models.DateElement> dates = new List<Models.DateElement>();
            foreach (var dateExpand in dynDates)
            {
                Models.DateElement newDate = new Models.DateElement();
                bool nullDates = true;
                foreach (var dateValues in dateExpand.Value)
                {



                    switch (dateValues.Key)
                    {
                        case "DateType":
                            var dateType = GetDateType(dateValues.Value);
                            newDate.DateType = dateType != Guid.Empty ? dateType.ToString() : null;
                            break;
                        case "EstimateDate":
                            newDate.EstimateDate = dateValues.Value;

                            break;
                        case "ActualDate":
                            newDate.ActualDate = dateValues.Value;
                            break;
                        case "Errors":
                            newDate.Errors = ProcessDynamicErrors(dateValues.Value);
                            break;
                    }
                    if (!string.IsNullOrEmpty(dateValues.Value.ToString()))
                    {
                        nullDates = false;
                    }

                }
                if (string.IsNullOrEmpty(newDate.EstimateDate) && string.IsNullOrEmpty(newDate.ActualDate))
                {
                    nullDates = true;
                }
                if (!nullDates)
                {
                    dates.Add(newDate);
                }

            }

            return dates.ToArray();
        }

        private Guid GetDateType(string dynEnum)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
                {
                    var ct = uow.DateTypes.Find(t => t.DT_DateType == dynEnum).FirstOrDefault();
                    if (ct != null)
                    {
                        return ct.DT_Id;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType().Name + " " + ex.InnerException.Message);
            }
            return Guid.Empty;
        }
        private DateType GetDateType(Guid dtId)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
                {

                    return uow.DateTypes.Get(dtId);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType().Name + " " + ex.InnerException.Message);
            }
            return null;
        }

        private ErrorElement[] ProcessDynamicErrors(dynamic dynErrors)
        {
            List<ErrorElement> errors = new List<ErrorElement>();

            foreach (var errValues in dynErrors)
            {
                bool errorBlank = true;
                ErrorElement newError = new ErrorElement();
                //foreach (var errValues in errExpanded.Value)
                //{

                    try
                    {
                        switch (errValues.Key)
                        {
                            case "ErrorTagName":
                                if (!string.IsNullOrEmpty(errValues.Value))
                                {
                                    newError.ErrorTagName = errValues.Value;
                                    errorBlank = false;
                                }

                                break;
                            case "ErrorType":
                                if (!string.IsNullOrEmpty(errValues.Value))
                                {
                                    newError.ErrorType = errValues.Value;
                                    errorBlank = false;
                                }
                                break;
                            case "ErrorLevel":
                                if (!string.IsNullOrEmpty(errValues.Value))
                                {
                                    newError.ErrorLevel = errValues.Value;
                                    errorBlank = false;
                                }
                                break;
                            case "ErrorMessage":
                                if (!string.IsNullOrEmpty(errValues.Value))
                                {
                                    newError.ErrorMessage = errValues.Value;
                                    errorBlank = false;
                                }
                                break;
                            case "ErrorExpectedValue":
                                if (!string.IsNullOrEmpty(errValues.Value))
                                {
                                    newError.ErrorExpectedValue = errValues.Value;
                                    errorBlank = false;
                                }
                                break;
                            case "ErrorDate":
                                if (!string.IsNullOrEmpty(errValues.Value))
                                {
                                    newError.ErrorDate = errValues.Value;
                                    errorBlank = false;
                                }
                                break;
                            case "ErrorScannedValue":
                                if (!string.IsNullOrEmpty(errValues.Value))
                                {
                                    newError.ErrorScannedValue = errValues.Value;
                                    errorBlank = false;
                                }
                                break;
                            case "ErrorConfidenceLevelValue":
                                if (!string.IsNullOrEmpty(errValues.Value))
                                {
                                    newError.ErrorConfidenceLevelValue = errValues.Value;
                                    errorBlank = false;
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        var st = new StackTrace();
                        var sf = st.GetFrame(0);

                        var currentMethodName = sf.GetMethod().Name;
                        Console.WriteLine("Error adding Error Line: Parent=" + errValues.ToString() + " : Stack:" + st);
                    }


                //}
                if (!errorBlank)
                {
                    errors.Add(newError);
                }


            }
            if (errors.Count() > 0)
            {
                return errors.ToArray();
            }
            return null;

        }

        private Guid GetCompType(string dynEnum)
        {
            //  var enumval = Enum.GetName(typeof(T), dynEnum);
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(ConnString)))
                {
                    var ct = uow.CompanyTypes.Find(t => t.CT_CompanyType == dynEnum).FirstOrDefault();
                    if (ct != null)
                    {
                        return ct.CT_ID;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Guid.Empty;

        }

        private string ProcessDynamicString(dynamic dynString, int length)
        {
            if (dynString == null)
            {
                return string.Empty;
            }
            return dynString.Truncate(length);
        }


        #endregion
    }
}
