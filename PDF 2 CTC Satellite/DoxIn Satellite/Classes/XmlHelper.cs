﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace DoxIn_Satellite.Classes
{
    public static class XmlHelper
    {
        public static string NodeExists(this XElement parent, string elementName)
        {
            var foundEl = parent.Element(elementName);
            if (foundEl != null)
            {
                return foundEl.Value;
            }
            return string.Empty;
        }

        public static XmlElement AsXmlElement<T>(this T o, XmlSerializerNamespaces ns = null, XmlSerializer serializer = null)
        {
            return o.AsXmlDocument(ns, serializer).DocumentElement;
        }

        public static XmlDocument AsXmlDocument<T>(this T o, XmlSerializerNamespaces ns = null, XmlSerializer serializer = null)
        {
            XmlDocument doc = new XmlDocument();
            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
            {
                new XmlSerializer(o.GetType()).Serialize(writer, o, ns ?? NoStandardXmlNamespaces());
            }

            //    doc = RemoveNameSpace(doc,ns);

            return doc;
        }

        public static XmlSerializerNamespaces NoStandardXmlNamespaces()
        {
            var ns = new XmlSerializerNamespaces();
            ns.Add("", ""); // Disable the xmlns:xsi and xmlns:xsd lines.
            return ns;
        }

        public static string AsString(this XmlDocument xmlDoc)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter tx = new XmlTextWriter(sw))
                {
                    xmlDoc.WriteTo(tx);
                    string strXmlText = sw.ToString();
                    return strXmlText;
                }
            }
        }

        public static IEnumerable<XElement> GetXElements(XDocument xDoc, string parent, string nodesToFind)
        {
            try
            {
                var nodes = xDoc.Descendants(parent).Elements(nodesToFind);
                if (nodes != null)
                {
                    if (nodes.Count() > 0)
                    {
                        return nodes;
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                Console.WriteLine("Error returning value: Parent='" + parent + "' NodeToFind='" + nodesToFind + "'");
                return null;
            }
        }
        public static XElement GetXElement(XDocument xDoc, string parent, string nodeToFind)
        {
            try
            {
                var node = xDoc.Descendants(parent).Elements(nodeToFind).Single();
                if (node.Name == nodeToFind)
                {
                    return node;
                }
                return null;
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                Console.WriteLine("Error returning value: Parent='" + parent + "' NodeToFind='" + nodeToFind + "'");
                return null;
            }
        }

        public static string GetNodeValue(XDocument file, string parent, string nodeToFind)
        {
            try
            {
                var node = file.Descendants(parent).Elements(nodeToFind).Single();
                if (node.Name == nodeToFind)
                {
                    return node.Value;
                }
                return string.Empty;
            }
            catch (Exception)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                Console.WriteLine("Error returning value: Parent='" + parent + "' NodeToFind='" + nodeToFind + "'");
                return string.Empty;
            }


        }

    }
}
