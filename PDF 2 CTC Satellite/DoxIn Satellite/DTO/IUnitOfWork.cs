﻿using DoxIn_Satellite.Repositories;
using System;

namespace DoxIn_Satellite.DTO
{
    public interface IUnitOfWork : IDisposable
    {
        ICommonDateRepository CommonDates { get; }
        ICommonInvoiceRepository CommonInvoices { get; }
        ICommonOrderLineRepository CommonOrderLines { get; }
        ICompanyRepository Companies { get; set; }
        ICompanyTypeRepository CompanyTypes { get; }
        IConvertedFileRepository ConvertedFiles { get; }
        ICustomerReferenceRepository CustomerReferences { get; }
        ICustomerRepository Customers { get; }
        IDateTypeRepository DateTypes { get; }
        IErrorMessageRepository ErrorMessages { get; }
        IWarehouseOrderRepository WarehouseOrders { get; }
        ILogRepository Logs { get; }



        int Complete();
    }
}