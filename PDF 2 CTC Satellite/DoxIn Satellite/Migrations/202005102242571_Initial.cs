﻿namespace DoxIn_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ErrorMessages", "WarehouseOrder_WO_Id", "dbo.WarehouseOrders");
            DropForeignKey("dbo.CustomerReferences", "WarehouseOrder_WO_Id", "dbo.WarehouseOrders");
            DropForeignKey("dbo.Users", "UserType_UT_Id", "dbo.UserTypes");
            DropForeignKey("dbo.Users", "Customer_CU_ID", "dbo.Customers");
            DropForeignKey("dbo.ConvertedFiles", "Customer_CU_ID", "dbo.Customers");
            DropForeignKey("dbo.ErrorMessages", "EM_CustomerReference_CR_ID", "dbo.CustomerReferences");
            DropForeignKey("dbo.ErrorMessages", "EM_CommonLine_OL_ID", "dbo.CommonOrderLines");
            DropForeignKey("dbo.ErrorMessages", "Company_CO_ID", "dbo.Company");
            DropForeignKey("dbo.CommonOrderLines", "OL_Invoice_CI_ID", "dbo.CommonInvoices");
            DropForeignKey("dbo.ErrorMessages", "EM_Invoice_CI_ID", "dbo.CommonInvoices");
            DropForeignKey("dbo.ErrorMessages", "EM_Date_DA_Id", "dbo.CommonDates");
            DropForeignKey("dbo.CommonDates", "DA_Invoice_CI_ID", "dbo.CommonInvoices");
            DropForeignKey("dbo.CommonDates", "DA_DateType_DT_Id", "dbo.DateTypes");
            DropForeignKey("dbo.CustomerReferences", "CR_Invoice_CI_ID", "dbo.CommonInvoices");
            DropForeignKey("dbo.CommonInvoices", "CI_ConvertedFile_FC_ID", "dbo.ConvertedFiles");
            DropForeignKey("dbo.Company", "CO_Invoice_CI_ID", "dbo.CommonInvoices");
            DropForeignKey("dbo.Company", "CO_CompanyType_CT_ID", "dbo.CompanyTypes");
            DropIndex("dbo.Users", new[] { "UserType_UT_Id" });
            DropIndex("dbo.Users", new[] { "Customer_CU_ID" });
            DropIndex("dbo.CommonDates", new[] { "DA_Invoice_CI_ID" });
            DropIndex("dbo.CommonDates", new[] { "DA_DateType_DT_Id" });
            DropIndex("dbo.CustomerReferences", new[] { "WarehouseOrder_WO_Id" });
            DropIndex("dbo.CustomerReferences", new[] { "CR_Invoice_CI_ID" });
            DropIndex("dbo.ConvertedFiles", new[] { "Customer_CU_ID" });
            DropIndex("dbo.CommonInvoices", new[] { "CI_ConvertedFile_FC_ID" });
            DropIndex("dbo.Company", new[] { "CO_Invoice_CI_ID" });
            DropIndex("dbo.Company", new[] { "CO_CompanyType_CT_ID" });
            DropIndex("dbo.ErrorMessages", new[] { "WarehouseOrder_WO_Id" });
            DropIndex("dbo.ErrorMessages", new[] { "EM_CustomerReference_CR_ID" });
            DropIndex("dbo.ErrorMessages", new[] { "EM_CommonLine_OL_ID" });
            DropIndex("dbo.ErrorMessages", new[] { "Company_CO_ID" });
            DropIndex("dbo.ErrorMessages", new[] { "EM_Invoice_CI_ID" });
            DropIndex("dbo.ErrorMessages", new[] { "EM_Date_DA_Id" });
            DropIndex("dbo.CommonOrderLines", new[] { "OL_Invoice_CI_ID" });
            DropTable("dbo.WarehouseOrders");
            DropTable("dbo.UserTypes");
            DropTable("dbo.Users");
            DropTable("dbo.Customers");
            DropTable("dbo.DateTypes");
            DropTable("dbo.CommonDates");
            DropTable("dbo.CustomerReferences");
            DropTable("dbo.ConvertedFiles");
            DropTable("dbo.CommonInvoices");
            DropTable("dbo.CompanyTypes");
            DropTable("dbo.Company");
            DropTable("dbo.ErrorMessages");
            DropTable("dbo.CommonOrderLines");
        }
    }
}
