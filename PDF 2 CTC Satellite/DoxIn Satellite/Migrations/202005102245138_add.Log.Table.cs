﻿namespace DoxIn_Satellite.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class addLogTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logs",
                c => new
                {
                    LO_Id = c.Guid(nullable: false, defaultValueSql: "newid()", identity: true),
                    LO_LogType = c.String(maxLength: 20),
                    LO_Date = c.DateTime(nullable: false),
                    LO_Operation = c.String(maxLength: 50),
                    LO_FieldName = c.String(maxLength: 50),
                    LO_FC = c.Guid(nullable: false),
                    LO_Message = c.String(),
                })
                .PrimaryKey(t => t.LO_Id);

        }

        public override void Down()
        {
            DropTable("dbo.Logs");
        }
    }
}
