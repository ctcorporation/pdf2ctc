﻿namespace DoxIn_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class alterLogTableParentId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Logs", "LO_ParentId", c => c.Guid(nullable: false));
            DropColumn("dbo.Logs", "LO_FC");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Logs", "LO_FC", c => c.Guid(nullable: false));
            DropColumn("dbo.Logs", "LO_ParentId");
        }
    }
}
