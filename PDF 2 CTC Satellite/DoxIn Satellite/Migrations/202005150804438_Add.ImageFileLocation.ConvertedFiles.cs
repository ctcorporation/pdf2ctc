﻿namespace DoxIn_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImageFileLocationConvertedFiles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConvertedFiles", "FC_ImageFileLocation", c => c.String());
            AddColumn("dbo.ConvertedFiles", "FC_FileType", c => c.String(maxLength: 3));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConvertedFiles", "FC_FileType");
            DropColumn("dbo.ConvertedFiles", "FC_ImageFileLocation");
        }
    }
}
