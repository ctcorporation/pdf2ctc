﻿namespace DoxIn_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConvertedFileDocumentIdentifierLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ConvertedFiles", "FC_DocumentIdentifier", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ConvertedFiles", "FC_DocumentIdentifier", c => c.String(maxLength: 10));
        }
    }
}
