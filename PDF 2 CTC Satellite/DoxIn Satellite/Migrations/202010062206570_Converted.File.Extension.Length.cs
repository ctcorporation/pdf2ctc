﻿namespace DoxIn_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConvertedFileExtensionLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ConvertedFiles", "FC_FileType", c => c.String(maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ConvertedFiles", "FC_FileType", c => c.String(maxLength: 3));
        }
    }
}
