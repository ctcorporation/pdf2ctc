﻿namespace DoxIn_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddScanTemplateID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConvertedFiles", "FC_ScanTemplateID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConvertedFiles", "FC_ScanTemplateID");
        }
    }
}
