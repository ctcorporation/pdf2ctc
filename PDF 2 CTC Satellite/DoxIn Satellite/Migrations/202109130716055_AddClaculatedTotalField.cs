﻿namespace DoxIn_Satellite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClaculatedTotalField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CommonInvoices", "CI_TotalInvoiceCalculated", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CommonInvoices", "CI_TotalInvoiceCalculated");
        }
    }
}
