﻿namespace DoxIn_Satellite.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DoxIn_Satellite.Models.CommonEntity>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DoxIn_Satellite.Models.CommonEntity context)
        {
            //           context.CompanyTypes.AddOrUpdate(x => x.CT_ID,
            //               new CompanyType { CT_CompanyType = "BillToAddress" },
            //               new CompanyType { CT_CompanyType = "Buyer" },
            //new CompanyType { CT_CompanyType = "Customer" },
            //new CompanyType { CT_CompanyType = "Consignee" },
            //new CompanyType { CT_CompanyType = "Consignor" },
            //new CompanyType { CT_CompanyType = "DeliveryAddress" },
            //new CompanyType { CT_CompanyType = "ForwardingAgent" },
            //new CompanyType { CT_CompanyType = "ImportBroker" },
            //new CompanyType { CT_CompanyType = "PickupAddress" },
            //new CompanyType { CT_CompanyType = "ShippingLine" },
            //new CompanyType { CT_CompanyType = "Supplier" },
            //new CompanyType { CT_CompanyType = "Warehouse" },
            //new CompanyType { CT_CompanyType = "TransportCompany" },
            //new CompanyType { CT_CompanyType = "ReceivingAgent" },
            //new CompanyType { CT_CompanyType = "OrderDeliveryAddress" },
            //new CompanyType { CT_CompanyType = "OrderDeliveryAddress" }
            //               );
            //           context.DateTypes.AddOrUpdate(x => x.DT_Id,
            //                       new DateType { DT_DateType = "Arrival" },
            //                       new DateType { DT_DateType = "Available" },
            //                       new DateType { DT_DateType = "Booked" },
            //                       new DateType { DT_DateType = "Closing" },
            //                       new DateType { DT_DateType = "Complete" },
            //                       new DateType { DT_DateType = "CustomsCleared" },
            //                       new DateType { DT_DateType = "DeliverBy" },
            //                       new DateType { DT_DateType = "Delivered" },
            //                       new DateType { DT_DateType = "Departure" },
            //                       new DateType { DT_DateType = "ExFactory" },
            //                       new DateType { DT_DateType = "GateIn" },
            //                       new DateType { DT_DateType = "GateOut" },
            //                       new DateType { DT_DateType = "Ordered" },
            //                       new DateType { DT_DateType = "Loading" },
            //                       new DateType { DT_DateType = "Packing" },
            //                       new DateType { DT_DateType = "Storage" },
            //                       new DateType { DT_DateType = "Unloading" },
            //                       new DateType { DT_DateType = "Raised" },
            //                       new DateType { DT_DateType = "InStoreDateReq" },
            //                       new DateType { DT_DateType = "CutOff" }
            //               );

        }
    }
}
