﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn_Satellite.Models
{
    public class CommonDate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DA_Id { get; set; }
        [MaxLength(20)]
        public string DA_EstimateDate { get; set; }
        public string DA_ActualDate { get; set; }
        public string DA_ParentKey { get; set; }
        public bool DA_ErrorsFound { get; set; }
        public Guid? DA_DateTypeId { get; set; }
        public virtual DateType DA_DateType { get; set; }
        public IList<ErrorMessage> Errors { get; set; }
        public Guid DA_InvoiceId { get; set; }
        public virtual CommonInvoice DA_Invoice { get; set; }


    }
}
