﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn_Satellite.Models
{
    public class CommonOrderLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid OL_ID { get; set; }
        public Guid OL_ParentID { get; set; }

        // 2 Character Table Qualifier (ie CI or OH) 
        [MaxLength(2)]
        public string OL_ParentKey { get; set; }
        public string OL_LineNo { get; set; }
        public string OL_InvoicedQty { get; set; }
        public string OL_OrderedQty { get; set; }
        public string OL_UnitPrice { get; set; }
        public string OL_UnitOfMeasurement { get; set; }
        public string OL_Tariff { get; set; }
        public string OL_UnitWeight { get; set; }
        public string OL_WeightUnit { get; set; }
        public string OL_Volume { get; set; }
        public string OL_VolumeUnit { get; set; }
        public string OL_LineTotal { get; set; }
        public string OL_PackageQty { get; set; }
        public string OL_PackageUnit { get; set; }
        public string OL_ProductCode { get; set; }
        public string OL_Barcode { get; set; }
        public bool OL_ErrorsFound { get; set; }
        public string OL_ProductDescription { get; set; }
        public IList<ErrorMessage> Errors { get; set; }
        public Guid OL_InvoiceID { get; set; }
        public CommonInvoice OL_Invoice { get; set; }




    }
}
