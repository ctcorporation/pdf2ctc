﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn_Satellite.Models
{
    [Table("Company")]
    public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CO_ID { get; set; }
        public string CO_Name { get; set; }
        public string CO_Code { get; set; }
        public string CO_Address1 { get; set; }
        public string CO_Address2 { get; set; }
        public string CO_Address3 { get; set; }
        public string CO_City { get; set; }
        public string CO_PostCode { get; set; }
        public string CO_State { get; set; }
        public string CO_Country { get; set; }
        public bool CO_Active { get; set; }
        public string CO_ContactName { get; set; }
        public string CO_EmailAddress { get; set; }
        public bool CO_ErrorsFound { get; set; }
        //Invoice Nav Props.
        public Guid CO_InvoiceId { get; set; }
        public CommonInvoice CO_Invoice { get; set; }
        //Company Type Nav Props (One)       
        public Guid? CO_CompanyTypeId { get; set; }
        public virtual CompanyType CO_CompanyType { get; set; }

        // Error Messages (Many)
        public IList<ErrorMessage> Errors { get; set; }








    }
}
