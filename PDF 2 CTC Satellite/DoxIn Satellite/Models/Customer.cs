﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn_Satellite.Models
{
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CU_ID { get; set; }
        [MaxLength(20)]
        public string CU_CODE { get; set; }
        [MaxLength(80)]
        public string CU_NAME { get; set; }
        public bool CU_ACTIVE { get; set; }
        [MaxLength(80)]
        public string CU_ADDRESS1 { get; set; }
        [MaxLength(80)]
        public string CU_ADDRESS2 { get; set; }
        [MaxLength(80)]
        public string CU_ADDRESS3 { get; set; }
        [MaxLength(80)]
        public string CU_CITY { get; set; }
        [MaxLength(20)]
        public string CU_STATE { get; set; }
        [MaxLength(10)]
        public string CU_POSTCODE { get; set; }
        [MaxLength(50)]
        public string CU_PHONE { get; set; }
        [MaxLength(80)]
        public string CU_EMAIL { get; set; }
        [MaxLength(20)]
        public string CU_INCOMINGCODE { get; set; }

        public ICollection<User> Users { get; set; }
        public ICollection<ConvertedFile> CU_CONVERTEDFILES { get; set; }


    }
}
