﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn_Satellite.Models
{
    [Table("Feedback")]
    public class Feedback
    {


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid FB_ID { get; set; }

        public Guid FB_ParentId { get; set; }
        [Display(Name = "Date Created")]
        public DateTime FB_DateTimeReported { get; set; }
        public Guid FB_US { get; set; }
        [MaxLength(10)]
        [Display(Name = "Type of Message")]
        public string FB_FeedbackType { get; set; }
        [Display(Name = "Message details")]
        public string FB_Details { get; set; }
        [Display(Name = "Message has been Acknowledged")]
        private bool FB_Acknowledged { get; set; }
        [Display(Name = "Date Acknowledged")]
        public DateTime FB_AckDate { get; set; }
        public Guid FB_AckUS { get; set; }
        [Display(Name = "Response Category")]
        public string FB_Category { get; set; }
        [Display(Name = "Date of Response")]
        public DateTime FB_ResponeDate { get; set; }
        [Display(Name = "Response")]
        public string FB_Response { get; set; }
        [Display(Name = "Incident Open")]
        public bool FB_Active { get; set; }
        [Display(Name = "Close Date")]
        public DateTime FB_ClsoeDate { get; set; }







    }
}
