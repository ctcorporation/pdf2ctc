﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn_Satellite.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid US_Id { get; set; }
        [MaxLength(20)]
        public string US_Username { get; set; }
        [MaxLength(50)]
        public string US_FirstName { get; set; }
        [MaxLength(50)]
        public string US_LastName { get; set; }
        public string US_Office { get; set; }
        public bool US_IsActive { get; set; }
        [MaxLength(50)]
        public string US_Password { get; set; }
        public Guid UserTypeId { get; set; }
        public UserType UserType { get; set; }
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }





    }
}
