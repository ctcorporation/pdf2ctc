﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public class CommonInvoiceRepository : GenericRepository<CommonInvoice>, ICommonInvoiceRepository
    {
        public CommonInvoiceRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }

}
