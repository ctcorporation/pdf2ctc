﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public class CommonOrderLineRepository : GenericRepository<CommonOrderLine>, ICommonOrderLineRepository
    {
        public CommonOrderLineRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
