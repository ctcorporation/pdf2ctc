﻿using DoxIn_Satellite.Models;


namespace DoxIn_Satellite.Repositories
{
    public class ConvertedFileRepository : GenericRepository<ConvertedFile>, IConvertedFileRepository
    {
        public ConvertedFileRepository(CommonEntity context)
            : base(context)
        {

        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }

    }
}
