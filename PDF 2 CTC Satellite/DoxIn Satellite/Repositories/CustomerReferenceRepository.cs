﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public class CustomerReferenceRepository : GenericRepository<CustomerReference>, ICustomerReferenceRepository
    {
        public CustomerReferenceRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
