﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public class DateTypeRepository : GenericRepository<DateType>, IDateTypeRepository
    {
        public DateTypeRepository(CommonEntity context)
            : base(context)
        {


        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }

        }
    }
}
