﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public class ErrorMessageRepository : GenericRepository<ErrorMessage>, IErrorMessageRepository
    {
        public ErrorMessageRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
