﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface ICommonDateRepository : IGenericRepository<CommonDate>
    {
        CommonEntity CommonEntity { get; }
    }
}