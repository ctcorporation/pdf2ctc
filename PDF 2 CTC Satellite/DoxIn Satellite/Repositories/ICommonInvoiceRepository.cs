﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface ICommonInvoiceRepository : IGenericRepository<CommonInvoice>
    {
        CommonEntity CommonEntity { get; }
    }
}