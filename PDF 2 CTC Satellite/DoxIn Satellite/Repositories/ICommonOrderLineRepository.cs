﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface ICommonOrderLineRepository : IGenericRepository<CommonOrderLine>
    {
        CommonEntity CommonEntity { get; }
    }
}