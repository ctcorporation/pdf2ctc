﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface ICompanyRepository : IGenericRepository<Company>
    {
        CommonEntity CommonEntity { get; }
    }
}