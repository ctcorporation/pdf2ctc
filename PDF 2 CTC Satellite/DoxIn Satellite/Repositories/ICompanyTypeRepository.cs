﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface ICompanyTypeRepository : IGenericRepository<CompanyType>
    {
        CommonEntity CommonEntity { get; }
    }
}