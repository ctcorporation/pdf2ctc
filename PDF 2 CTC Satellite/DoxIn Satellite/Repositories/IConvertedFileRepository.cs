﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface IConvertedFileRepository : IGenericRepository<ConvertedFile>
    {
        CommonEntity CommonEntity { get; }
    }
}