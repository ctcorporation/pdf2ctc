﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface ICustomerReferenceRepository : IGenericRepository<CustomerReference>
    {
        CommonEntity CommonEntity { get; }
    }
}