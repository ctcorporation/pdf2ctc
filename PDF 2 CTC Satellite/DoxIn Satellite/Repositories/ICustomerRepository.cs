﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        CommonEntity CommonEntity { get; }
    }
}