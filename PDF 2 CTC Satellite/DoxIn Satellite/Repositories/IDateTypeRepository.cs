﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface IDateTypeRepository : IGenericRepository<DateType>
    {
        CommonEntity CommonEntity { get; }
    }
}