﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface IErrorMessageRepository : IGenericRepository<ErrorMessage>
    {
        CommonEntity CommonEntity { get; }
    }
}