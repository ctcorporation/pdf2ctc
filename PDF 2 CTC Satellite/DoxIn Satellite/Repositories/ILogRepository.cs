﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface ILogRepository : IGenericRepository<Log>
    {
        CommonEntity CommonEntity { get; }
    }
}