﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public interface IWarehouseOrderRepository : IGenericRepository<WarehouseOrder>
    {
        CommonEntity CommonEntity { get; }
    }
}