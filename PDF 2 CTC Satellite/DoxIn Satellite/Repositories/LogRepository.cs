﻿using DoxIn_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoxIn_Satellite.Repositories
{
    public class LogRepository : GenericRepository<Log>, ILogRepository
    {
        public LogRepository(CommonEntity context)
            : base(context)
        {

        }

        public CommonEntity CommonEntity
        {
            get
            {
                return Context as CommonEntity;
            }
        }
    }
}
