﻿using DoxIn_Satellite.Models;

namespace DoxIn_Satellite.Repositories
{
    public class WarehouseOrderRepository : GenericRepository<WarehouseOrder>, IWarehouseOrderRepository
    {
        public WarehouseOrderRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
