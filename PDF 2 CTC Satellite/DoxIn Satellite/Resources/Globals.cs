﻿using NodeResources;

namespace DoxIn_Satellite.Resources
{
    public static class Globals
    {

        #region members
        private static string _appConfig;
        private static string _appLogPath;
        private static int _mainTimerInt;
        private static IMailServerSettings _mailServerSettings;
        private static NodeData.IConnectionManager _satConnManager;
        private static NodeData.IConnectionManager _nodeConnManager;
        private static string _pickupPath;
        private static string _outputPath;
        private static string _testPath;
        private static string _failPath;
        private static string _archivePath;
        private static string _winSCPPath;
        private static string _alertsTo;
        private static string _imagePath;
        #endregion

        #region Properties

        public const string AppName = "Doxin";
        public static string ImagePath
        {
            get
            { return _imagePath; }
            set
            { _imagePath = value; }
        }

        public static string PickupPath
        {
            get { return _pickupPath; }
            set { _pickupPath = value; }
        }
        public static string OutPutPath
        {
            get { return _outputPath; }
            set { _outputPath = value; }
        }
        public static string TestPath
        {
            get { return _testPath; }
            set { _testPath = value; }
        }
        public static string FailPath
        {
            get { return _failPath; }
            set { _failPath = value; }
        }

        public static string ArchivePath
        {
            get { return _archivePath; }
            set { _archivePath = value; }
        }

        public static string WinSCPPath
        {
            get { return _winSCPPath; }
            set { _winSCPPath = value; }
        }

        public static string AlertsTo
        {
            get { return _alertsTo; }
            set { _alertsTo = value; }
        }
        public static NodeData.IConnectionManager SattConnManager
        {
            get { return _satConnManager; }
            set { _satConnManager = value; }
        }

        public static NodeData.IConnectionManager NodeConnManager
        {
            get { return _nodeConnManager; }
            set { _nodeConnManager = value; }

        }
        public static int MainTimerInt
        {
            get { return _mainTimerInt; }
            set { _mainTimerInt = value; }
        }

        public static IMailServerSettings MailServerSettings
        {
            get { return _mailServerSettings; }
            set { _mailServerSettings = value; }
        }
        public static string AppConfig
        {
            get { return _appConfig; }
            set
            {
                _appConfig = value;

            }
        }
        public static string AppLogPath
        {
            get { return _appLogPath; }
            set { _appLogPath = value; }
        }
        #endregion

        #region Constructors

        #endregion

        #region Methods

        #endregion

        #region helpers

        #endregion
    }


}
