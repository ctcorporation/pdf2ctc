﻿namespace DoxIn_Satellite.Views
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.edName = new System.Windows.Forms.TextBox();
            this.edCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rbAll = new System.Windows.Forms.RadioButton();
            this.rbActive = new System.Windows.Forms.RadioButton();
            this.rbInActive = new System.Windows.Forms.RadioButton();
            //this.bbFind = new Syncfusion.WinForms.Controls.SfButton();
            //this.bbClose = new Syncfusion.WinForms.Controls.SfButton();
            this.grdCustomerList = new System.Windows.Forms.DataGridView();
            this.custName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custActive = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.custPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custIncomingCode = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.custCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdCustomerList)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer Name";
            // 
            // edName
            // 
            this.edName.Location = new System.Drawing.Point(122, 18);
            this.edName.Name = "edName";
            this.edName.Size = new System.Drawing.Size(214, 20);
            this.edName.TabIndex = 1;
            // 
            // edCode
            // 
            this.edCode.Location = new System.Drawing.Point(122, 44);
            this.edCode.Name = "edCode";
            this.edCode.Size = new System.Drawing.Size(214, 20);
            this.edCode.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer Code";
            // 
            // rbAll
            // 
            this.rbAll.AutoSize = true;
            this.rbAll.Checked = true;
            this.rbAll.Location = new System.Drawing.Point(37, 81);
            this.rbAll.Name = "rbAll";
            this.rbAll.Size = new System.Drawing.Size(66, 17);
            this.rbAll.TabIndex = 4;
            this.rbAll.TabStop = true;
            this.rbAll.Text = "Show All";
            this.rbAll.UseVisualStyleBackColor = true;
            // 
            // rbActive
            // 
            this.rbActive.AutoSize = true;
            this.rbActive.Location = new System.Drawing.Point(109, 81);
            this.rbActive.Name = "rbActive";
            this.rbActive.Size = new System.Drawing.Size(109, 17);
            this.rbActive.TabIndex = 5;
            this.rbActive.Text = "Show Active Only";
            this.rbActive.UseVisualStyleBackColor = true;
            // 
            // rbInActive
            // 
            this.rbInActive.AutoSize = true;
            this.rbInActive.Location = new System.Drawing.Point(227, 81);
            this.rbInActive.Name = "rbInActive";
            this.rbInActive.Size = new System.Drawing.Size(117, 17);
            this.rbInActive.TabIndex = 6;
            this.rbInActive.Text = "Show Inactive Only";
            this.rbInActive.UseVisualStyleBackColor = true;
            // 
            // bbFind
            // 
            //this.bbFind.AccessibleName = "Button";
            //this.bbFind.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            //this.bbFind.Font = new System.Drawing.Font("Segoe UI Semibold", 9F);
            //this.bbFind.Location = new System.Drawing.Point(366, 21);
            //this.bbFind.Name = "bbFind";
            //this.bbFind.Size = new System.Drawing.Size(71, 28);
            //this.bbFind.Style.Image = global::DoxIn_Satellite.Properties.Resources.find;
            //this.bbFind.TabIndex = 8;
            //this.bbFind.Text = "&Find";
            //this.bbFind.Click += new System.EventHandler(this.bbFind_Click);
            // 
            // bbClose
            // 
            //this.bbClose.AccessibleName = "Button";
            //this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            //this.bbClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            //this.bbClose.Font = new System.Drawing.Font("Segoe UI Semibold", 9F);
            //this.bbClose.Location = new System.Drawing.Point(707, 410);
            //this.bbClose.Name = "bbClose";
            //this.bbClose.Size = new System.Drawing.Size(81, 28);
            //this.bbClose.Style.Image = global::DoxIn_Satellite.Properties.Resources.Close;
            //this.bbClose.TabIndex = 9;
            //this.bbClose.Text = "&Close";
            //this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // grdCustomerList
            // 
            this.grdCustomerList.AllowUserToAddRows = false;
            this.grdCustomerList.AllowUserToDeleteRows = false;
            this.grdCustomerList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdCustomerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCustomerList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.custName,
            this.custCode,
            this.custActive,
            this.custPhone,
            this.custIncomingCode,
            this.custCity});
            this.grdCustomerList.Location = new System.Drawing.Point(12, 114);
            this.grdCustomerList.Name = "grdCustomerList";
            this.grdCustomerList.ReadOnly = true;
            this.grdCustomerList.Size = new System.Drawing.Size(776, 281);
            this.grdCustomerList.TabIndex = 10;
            // 
            // custName
            // 
            this.custName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.custName.HeaderText = "Customer Name";
            this.custName.Name = "custName";
            this.custName.ReadOnly = true;
            // 
            // custCode
            // 
            this.custCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.custCode.HeaderText = "Customer Code";
            this.custCode.Name = "custCode";
            this.custCode.ReadOnly = true;
            this.custCode.Width = 96;
            // 
            // custActive
            // 
            this.custActive.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.custActive.HeaderText = "Active";
            this.custActive.Name = "custActive";
            this.custActive.ReadOnly = true;
            this.custActive.Width = 43;
            // 
            // custPhone
            // 
            this.custPhone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.custPhone.HeaderText = "Phone";
            this.custPhone.Name = "custPhone";
            this.custPhone.ReadOnly = true;
            // 
            // custIncomingCode
            // 
            this.custIncomingCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.custIncomingCode.HeaderText = "Incoming Code";
            this.custIncomingCode.Name = "custIncomingCode";
            this.custIncomingCode.ReadOnly = true;
            this.custIncomingCode.Width = 76;
            // 
            // custCity
            // 
            this.custCity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.custCity.HeaderText = "City";
            this.custCity.Name = "custCity";
            this.custCity.ReadOnly = true;
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.grdCustomerList);
            //this.Controls.Add(this.bbClose);
            //this.Controls.Add(this.bbFind);
            this.Controls.Add(this.rbInActive);
            this.Controls.Add(this.rbActive);
            this.Controls.Add(this.rbAll);
            this.Controls.Add(this.edCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edName);
            this.Controls.Add(this.label1);
            this.Name = "frmCustomer";
            this.Text = "Customer Maintenance";
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdCustomerList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edName;
        private System.Windows.Forms.TextBox edCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbAll;
        private System.Windows.Forms.RadioButton rbActive;
        private System.Windows.Forms.RadioButton rbInActive;
        //private Syncfusion.WinForms.Controls.SfButton bbFind;
        //private Syncfusion.WinForms.Controls.SfButton bbClose;
        private System.Windows.Forms.DataGridView grdCustomerList;
        private System.Windows.Forms.DataGridViewTextBoxColumn custName;
        private System.Windows.Forms.DataGridViewTextBoxColumn custCode;
        private System.Windows.Forms.DataGridViewCheckBoxColumn custActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn custPhone;
        private System.Windows.Forms.DataGridViewCheckBoxColumn custIncomingCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn custCity;
    }
}