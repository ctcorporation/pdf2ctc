﻿using DoxIn_Satellite.Classes;
using DoxIn_Satellite.Models;
using DoxIn_Satellite.Resources;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DoxIn_Satellite.Views
{
    public partial class frmCustomer : Form
    {
        public frmCustomer()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bbFind_Click(object sender, EventArgs e)
        {
            FindCustomers();
        }

        private void FindCustomers()
        {
            CustomerMaint cm = new CustomerMaint(Globals.SattConnManager.ConnString);
            var custList = cm.GetCustList(edName.Text, edCode.Text);
            grdCustomerList.AutoGenerateColumns = false;
            BindingSource bsCustomers = new BindingSource();
            bsCustomers.DataSource = new SortableBindingList<Customer>(custList.ToList());
            grdCustomerList.DataSource = bsCustomers.DataSource;


        }

        private void frmCustomer_Load(object sender, EventArgs e)
        {
            rbAll.Checked = true;
            FindCustomers();
        }
    }
}
