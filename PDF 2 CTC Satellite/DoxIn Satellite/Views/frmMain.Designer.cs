﻿namespace DoxIn_Satellite.Views
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userAccessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.processFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runReleaseFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runDeletedFlagsCleanupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statMain = new System.Windows.Forms.StatusStrip();
            this.tslMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslSpacer = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslCmbMode = new System.Windows.Forms.ToolStripDropDownButton();
            this.testingToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.rtbActivity = new System.Windows.Forms.RichTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnClearImport = new System.Windows.Forms.Button();
            this.grdImportLog = new System.Windows.Forms.DataGridView();
            this.ImportDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImportOperation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImportMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImportFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnClearExportLog = new System.Windows.Forms.Button();
            this.grdExportLog = new System.Windows.Forms.DataGridView();
            this.ExportDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExportOperation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExportFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExportMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.statMain.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdImportLog)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdExportLog)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.userAccessToolStripMenuItem,
            this.systemToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // userAccessToolStripMenuItem
            // 
            this.userAccessToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customersToolStripMenuItem1,
            this.usersToolStripMenuItem});
            this.userAccessToolStripMenuItem.Name = "userAccessToolStripMenuItem";
            this.userAccessToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.userAccessToolStripMenuItem.Text = "User Access";
            // 
            // customersToolStripMenuItem1
            // 
            this.customersToolStripMenuItem1.Name = "customersToolStripMenuItem1";
            this.customersToolStripMenuItem1.Size = new System.Drawing.Size(131, 22);
            this.customersToolStripMenuItem1.Text = "Customers";
            this.customersToolStripMenuItem1.Click += new System.EventHandler(this.customersToolStripMenuItem1_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.usersToolStripMenuItem.Text = "Users";
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.customersToolStripMenuItem,
            this.processFilesToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.systemToolStripMenuItem.Text = "&System";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F2)));
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.settingsToolStripMenuItem.Text = "&Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // customersToolStripMenuItem
            // 
            this.customersToolStripMenuItem.Name = "customersToolStripMenuItem";
            this.customersToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F3)));
            this.customersToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.customersToolStripMenuItem.Text = "&Customers";
            this.customersToolStripMenuItem.Click += new System.EventHandler(this.customersToolStripMenuItem_Click);
            // 
            // processFilesToolStripMenuItem
            // 
            this.processFilesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runReleaseFilesToolStripMenuItem,
            this.runDeletedFlagsCleanupToolStripMenuItem});
            this.processFilesToolStripMenuItem.Name = "processFilesToolStripMenuItem";
            this.processFilesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.processFilesToolStripMenuItem.Text = "Process Files";
            // 
            // runReleaseFilesToolStripMenuItem
            // 
            this.runReleaseFilesToolStripMenuItem.Name = "runReleaseFilesToolStripMenuItem";
            this.runReleaseFilesToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.runReleaseFilesToolStripMenuItem.Text = "Run Release Files";
            this.runReleaseFilesToolStripMenuItem.Click += new System.EventHandler(this.runReleaseFilesToolStripMenuItem_Click);
            // 
            // runDeletedFlagsCleanupToolStripMenuItem
            // 
            this.runDeletedFlagsCleanupToolStripMenuItem.Name = "runDeletedFlagsCleanupToolStripMenuItem";
            this.runDeletedFlagsCleanupToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.runDeletedFlagsCleanupToolStripMenuItem.Text = "Run Deleted Flags Cleanup";
            this.runDeletedFlagsCleanupToolStripMenuItem.Click += new System.EventHandler(this.runDeletedFlagsCleanupToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // statMain
            // 
            this.statMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslMain,
            this.tslSpacer,
            this.tslMode,
            this.tslCmbMode});
            this.statMain.Location = new System.Drawing.Point(0, 428);
            this.statMain.Name = "statMain";
            this.statMain.Size = new System.Drawing.Size(800, 22);
            this.statMain.TabIndex = 2;
            this.statMain.Text = "statusStrip1";
            // 
            // tslMain
            // 
            this.tslMain.AutoSize = false;
            this.tslMain.Name = "tslMain";
            this.tslMain.Size = new System.Drawing.Size(39, 17);
            this.tslMain.Text = "Status";
            // 
            // tslSpacer
            // 
            this.tslSpacer.AutoSize = false;
            this.tslSpacer.Name = "tslSpacer";
            this.tslSpacer.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.tslSpacer.Size = new System.Drawing.Size(60, 17);
            this.tslSpacer.Text = " ";
            // 
            // tslMode
            // 
            this.tslMode.Name = "tslMode";
            this.tslMode.Size = new System.Drawing.Size(50, 17);
            this.tslMode.Text = "tslMode";
            this.tslMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tslCmbMode
            // 
            this.tslCmbMode.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslCmbMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslCmbMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testingToolStrip,
            this.productionToolStrip});
            this.tslCmbMode.Image = ((System.Drawing.Image)(resources.GetObject("tslCmbMode.Image")));
            this.tslCmbMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tslCmbMode.Margin = new System.Windows.Forms.Padding(0, 2, 10, 0);
            this.tslCmbMode.Name = "tslCmbMode";
            this.tslCmbMode.Size = new System.Drawing.Size(29, 20);
            this.tslCmbMode.Text = "toolStripDropDownButton1";
            // 
            // testingToolStrip
            // 
            this.testingToolStrip.CheckOnClick = true;
            this.testingToolStrip.Name = "testingToolStrip";
            this.testingToolStrip.Size = new System.Drawing.Size(133, 22);
            this.testingToolStrip.Text = "Testing";
            this.testingToolStrip.Click += new System.EventHandler(this.testingToolStrip_Click);
            // 
            // productionToolStrip
            // 
            this.productionToolStrip.CheckOnClick = true;
            this.productionToolStrip.Name = "productionToolStrip";
            this.productionToolStrip.Size = new System.Drawing.Size(133, 22);
            this.productionToolStrip.Text = "Production";
            this.productionToolStrip.Click += new System.EventHandler(this.productionToolStrip_Click);
            // 
            // tcMain
            // 
            this.tcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcMain.Controls.Add(this.tabPage1);
            this.tcMain.Controls.Add(this.tabPage2);
            this.tcMain.Controls.Add(this.tabPage3);
            this.tcMain.Location = new System.Drawing.Point(12, 27);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(776, 360);
            this.tcMain.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.rtbActivity);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(768, 334);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Activity";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // rtbActivity
            // 
            this.rtbActivity.Location = new System.Drawing.Point(15, 23);
            this.rtbActivity.Name = "rtbActivity";
            this.rtbActivity.Size = new System.Drawing.Size(734, 292);
            this.rtbActivity.TabIndex = 0;
            this.rtbActivity.Text = "";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnClearImport);
            this.tabPage2.Controls.Add(this.grdImportLog);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(768, 334);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Import Log";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnClearImport
            // 
            this.btnClearImport.Image = ((System.Drawing.Image)(resources.GetObject("btnClearImport.Image")));
            this.btnClearImport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClearImport.Location = new System.Drawing.Point(686, 305);
            this.btnClearImport.Name = "btnClearImport";
            this.btnClearImport.Size = new System.Drawing.Size(76, 23);
            this.btnClearImport.TabIndex = 1;
            this.btnClearImport.Text = "Clear Log";
            this.btnClearImport.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClearImport.UseVisualStyleBackColor = true;
            this.btnClearImport.Click += new System.EventHandler(this.btnClearImport_Click);
            // 
            // grdImportLog
            // 
            this.grdImportLog.AllowUserToAddRows = false;
            this.grdImportLog.AllowUserToDeleteRows = false;
            this.grdImportLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdImportLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ImportDate,
            this.ImportOperation,
            this.ImportMessage,
            this.ImportFieldName});
            this.grdImportLog.Location = new System.Drawing.Point(6, 6);
            this.grdImportLog.Name = "grdImportLog";
            this.grdImportLog.ReadOnly = true;
            this.grdImportLog.Size = new System.Drawing.Size(756, 270);
            this.grdImportLog.TabIndex = 0;
            // 
            // ImportDate
            // 
            this.ImportDate.DataPropertyName = "LO_Date";
            this.ImportDate.HeaderText = "Date";
            this.ImportDate.Name = "ImportDate";
            this.ImportDate.ReadOnly = true;
            // 
            // ImportOperation
            // 
            this.ImportOperation.DataPropertyName = "LO_Operation";
            this.ImportOperation.HeaderText = "Operation";
            this.ImportOperation.Name = "ImportOperation";
            this.ImportOperation.ReadOnly = true;
            // 
            // ImportMessage
            // 
            this.ImportMessage.DataPropertyName = "LO_Message";
            this.ImportMessage.HeaderText = "Message";
            this.ImportMessage.Name = "ImportMessage";
            this.ImportMessage.ReadOnly = true;
            // 
            // ImportFieldName
            // 
            this.ImportFieldName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ImportFieldName.DataPropertyName = "LO_FieldName";
            this.ImportFieldName.HeaderText = "FieldName";
            this.ImportFieldName.Name = "ImportFieldName";
            this.ImportFieldName.ReadOnly = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnClearExportLog);
            this.tabPage3.Controls.Add(this.grdExportLog);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(768, 334);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Export Log";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnClearExportLog
            // 
            this.btnClearExportLog.Image = ((System.Drawing.Image)(resources.GetObject("btnClearExportLog.Image")));
            this.btnClearExportLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClearExportLog.Location = new System.Drawing.Point(686, 305);
            this.btnClearExportLog.Name = "btnClearExportLog";
            this.btnClearExportLog.Size = new System.Drawing.Size(76, 23);
            this.btnClearExportLog.TabIndex = 2;
            this.btnClearExportLog.Text = "Clear Log";
            this.btnClearExportLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClearExportLog.UseVisualStyleBackColor = true;
            this.btnClearExportLog.Click += new System.EventHandler(this.btnClearExportLog_Click);
            // 
            // grdExportLog
            // 
            this.grdExportLog.AllowUserToAddRows = false;
            this.grdExportLog.AllowUserToDeleteRows = false;
            this.grdExportLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdExportLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ExportDate,
            this.ExportOperation,
            this.ExportFieldName,
            this.ExportMessage});
            this.grdExportLog.Location = new System.Drawing.Point(6, 6);
            this.grdExportLog.Name = "grdExportLog";
            this.grdExportLog.ReadOnly = true;
            this.grdExportLog.Size = new System.Drawing.Size(756, 270);
            this.grdExportLog.TabIndex = 1;
            // 
            // ExportDate
            // 
            this.ExportDate.DataPropertyName = "LO_Date";
            this.ExportDate.HeaderText = "Date";
            this.ExportDate.Name = "ExportDate";
            this.ExportDate.ReadOnly = true;
            // 
            // ExportOperation
            // 
            this.ExportOperation.DataPropertyName = "LO_Operation";
            this.ExportOperation.HeaderText = "Operation";
            this.ExportOperation.Name = "ExportOperation";
            this.ExportOperation.ReadOnly = true;
            // 
            // ExportFieldName
            // 
            this.ExportFieldName.DataPropertyName = "Lo_FieldName";
            this.ExportFieldName.HeaderText = "Field Name";
            this.ExportFieldName.Name = "ExportFieldName";
            this.ExportFieldName.ReadOnly = true;
            // 
            // ExportMessage
            // 
            this.ExportMessage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExportMessage.DataPropertyName = "LO_Message";
            this.ExportMessage.HeaderText = "Message";
            this.ExportMessage.Name = "ExportMessage";
            this.ExportMessage.ReadOnly = true;
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStart.Location = new System.Drawing.Point(662, 393);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(60, 23);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "&Start";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = global::DoxIn_Satellite.Properties.Resources.Close;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(728, 393);
            this.btnClose.Name = "btnClose";
            this.btnClose.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnClose.Size = new System.Drawing.Size(60, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.statMain);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "DoxIn Satellite";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statMain.ResumeLayout(false);
            this.statMain.PerformLayout();
            this.tcMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdImportLog)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdExportLog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statMain;
        private System.Windows.Forms.ToolStripStatusLabel tslMain;
        private System.Windows.Forms.ToolStripStatusLabel tslSpacer;
        private System.Windows.Forms.ToolStripStatusLabel tslMode;
        private System.Windows.Forms.ToolStripDropDownButton tslCmbMode;
        private System.Windows.Forms.ToolStripMenuItem testingToolStrip;
        private System.Windows.Forms.ToolStripMenuItem productionToolStrip;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ToolStripMenuItem userAccessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem processFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runDeletedFlagsCleanupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runReleaseFilesToolStripMenuItem;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.RichTextBox rtbActivity;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView grdImportLog;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView grdExportLog;
        private System.Windows.Forms.Button btnClearImport;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportOperation;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportFieldName;
        private System.Windows.Forms.Button btnClearExportLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExportDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExportOperation;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExportFieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExportMessage;
    }
}