﻿using CTCLogging;
using DoxIn_Satellite.Classes;
using DoxIn_Satellite.Models;
using DoxIn_Satellite.Resources;
using NodeData;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows.Forms;

namespace DoxIn_Satellite.Views
{
    public partial class frmMain : Form
    {
        #region members
        private string appDataPath;
        private string _path;
        System.Timers.Timer tmrMain;
        const string images = "xmlpdf";
        const string origPath = @"\\ctc-webserver08\files\PDF2Go\In";

        #endregion

        #region properties

        #endregion

        #region Constructors
        public frmMain()
        {
            InitializeComponent();
            tmrMain = new System.Timers.Timer
            {
                Interval = 300000

            };
            var assembly = typeof(Program).Assembly;
            var attribute = (GuidAttribute)assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0];
            var id = attribute.Value;

        }
        #endregion

        #region Methods

        private void runDeletedFlagsCleanupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcDeleted();
        }

        private void ProcDeleted()
        {
            AddRTBText(rtbActivity, "Beginning Deleted Files processing", Color.Black);
            ProcessFiles pf = new ProcessFiles(Globals.SattConnManager.ConnString, Globals.AppLogPath);
            AddRTBText(rtbActivity, pf.DeleteFiles(), Color.Black);
        }

        private void runReleaseFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddRTBText(rtbActivity, "Beginning File Release processing", Color.Black);
            ProcReleased();

        }

        private void ProcReleased()
        {
            AddRTBText(rtbActivity, "Begin Released Files Processing", Color.Black);
            ProcessFiles pf = new ProcessFiles(Globals.SattConnManager.ConnString, Globals.AppLogPath);
            pf.DoConvert();
            ShowExportLog();
            AddRTBText(rtbActivity, pf.DoConvert(), Color.Black);

        }

        private void ShowExportLog()
        {
            if (grdExportLog.InvokeRequired)
            {
                grdExportLog.BeginInvoke(new Action(delegate { ShowExportLog(); }));
                return;
            }
            BindingSource bsExportLog = new BindingSource();
            ActivityLog al = new ActivityLog(Globals.SattConnManager.ConnString);
            var exportLog = al.GetLog("Export");
            if (exportLog.Count > 0)
            {
                bsExportLog.DataSource = new SortableBindingList<Log>(exportLog);


            }
            grdExportLog.DataSource = bsExportLog.DataSource;
        }

        private void ShowImportLog()
        {
            if (grdImportLog.InvokeRequired)
            {
                grdImportLog.BeginInvoke(new Action(delegate { ShowImportLog(); }));
                return;
            }

            BindingSource bsExportLog = new BindingSource();
            ActivityLog al = new ActivityLog(Globals.SattConnManager.ConnString);
            var importLog = al.GetLog("Import");

            if (importLog.Count > 0)
            {
                bsExportLog.DataSource = new SortableBindingList<Log>(importLog);


            }
            grdExportLog.DataSource = bsExportLog.DataSource;

        }

        private void btnClearImport_Click(object sender, EventArgs e)
        {
            ActivityLog al = new ActivityLog(Globals.SattConnManager.ConnString);
            al.ClearLog("Import");
            ShowImportLog();
        }

        private void btnClearExportLog_Click(object sender, EventArgs e)
        {
            ActivityLog al = new ActivityLog(Globals.SattConnManager.ConnString);
            al.ClearLog("Export");
            ShowExportLog();
        }

        private void runFileMoveToolStripMenuItem_Click(object sender, EventArgs e)
        {



        }
        private void ProcessingQueue()
        {
            tslMain.Text = "Processing...";
            MethodBase m = MethodBase.GetCurrentMethod();
            AddRTBText(rtbActivity, "Processing Started", Color.Black);
            NodeData.HeartBeatManager heartBeat = new HeartBeatManager(Globals.NodeConnManager.ConnString);
            heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            tmrMain.Stop();
            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                if (string.IsNullOrEmpty(Globals.PickupPath))
                {
                    MessageBox.Show("Settings for Production Pickup path are missing. Please enter in Setup", "Setup Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Setup(m.Name);
                    return;
                }
                if (!Directory.Exists(Globals.PickupPath))
                {
                    MessageBox.Show("Folder does not exist for Production Pickup path. Please enter in Setup", "Setup Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Setup(m.Name);
                    return;
                }

                filesPath = Globals.PickupPath;
            }
            else
            {
                if (string.IsNullOrEmpty(Globals.TestPath))
                {
                    MessageBox.Show("Settings for Test Pickup path are missing. Please enter in Setup", "Setup Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Setup(m.Name);
                    return;
                }
                if (!Directory.Exists(Globals.TestPath))
                {
                    MessageBox.Show("Folder does not exist for Test Pickup path. Please enter in Setup", "Setup Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Setup(m.Name);
                    return;
                }
                filesPath = Globals.TestPath;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            int iCount = 0;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            AddRTBText(rtbActivity, "Import Cycle started.", Color.Black);
            var list = diTodo.GetFiles();
            if (list.Length == 0)
            {
                AddRTBText(rtbActivity, "No Files to Import.", Color.Black);

            }
            int filesImported = 0;
            foreach (var file in list)
            {
                tslMain.Text = "Importing... ";
                filesImported++;

                ImportXML importXML = new ImportXML(Globals.SattConnManager.ConnString, file.FullName, Globals.ImagePath);
                AddRTBText(rtbActivity, "Import : " + file.Name, Color.Black);
                var nodefile = importXML.ConvertXml(file.FullName);
                var convertedFile = importXML.ImportData(nodefile);
                if (convertedFile != null)
                {
                    importXML.MoveImageFile(convertedFile, Path.Combine(origPath, images));

                }
                else
                {
                    AddRTBText(rtbActivity, file.Name + "Import failed: " + importXML.ErrorMessage, Color.Red);
                }
                file.Delete();
            }
            if (filesImported > 0)
            {
                AddRTBText(rtbActivity, "Import process complete." + filesImported + " files imported", Color.Green);
            }
            tslMain.Text = "Imported Files " + filesImported.ToString();
            ProcDeleted();
            ProcReleased();
            tmrMain.Start();
        }
        private void LoadSettings()
        {
            CommonApplicationData appData = new CommonApplicationData("CTC", "Doxin", true);
            string appDataPath = appData.ToString();
            string s = Path.Combine(appDataPath, Globals.AppName + ".xml");
            frmSettings settings = new frmSettings();

            Globals.AppConfig = s;
            SystemSettings ini = new SystemSettings(Globals.AppConfig);
            if (ini.LoadSettings())
            {
                MessageBox.Show("Application Settings missing. Now running setup", "Application Setup needed.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                settings.ShowDialog();
                LoadSettings();
            }
            AddRTBText(rtbActivity, "Settings Loaded", Color.Green);


        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }




        #endregion

        #region Helpers
        private void AddRTBText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new Action(delegate { AddRTBText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(string.Format("{0:g} :", DateTime.Now) + value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();
        }

        #endregion

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setup(sender);
        }

        private void Setup(object sender)
        {
            frmSettings settings = new frmSettings();
            settings.Show();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            LoadSettings();
            tmrMain.Interval = (int)TimeSpan.FromMinutes(Globals.MainTimerInt).TotalMilliseconds;
            tslMode.Text = "Production";
            productionToolStrip.Checked = true;
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.NodeConnManager.ConnString))
            {

                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", "Starting", null);
            }
            tmrMain.Interval = (int)TimeSpan.FromMinutes(Globals.MainTimerInt).TotalMilliseconds;
            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            tslMain.Width = this.Width / 2;
            tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStrip.Width);
            string appLog = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "-Log.XML";
            Globals.AppLogPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
               System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, appLog);
            using (AppLogger log = new AppLogger(Globals.AppLogPath, string.Empty, "Starting Application", "Starting", DateTime.Now, string.Empty, string.Empty))
            {
                //log.AddLog();
            }
            this.tslSpacer.Padding = new Padding(this.Size.Width - (195), 0, 0, 0);
            AddRTBText(rtbActivity, "Starting Timed Process", Color.Black);
            btnStart.Text = "&Start";
            btnStart.BackColor = Color.LightGreen;
            //btnStart_Click(btnStart, null);
        }

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {

            ProcessingQueue();
            tslMain.Text = "Next Process at " + DateTime.Now.AddMinutes(5);
            System.Windows.Forms.Application.DoEvents();
            if (btnStart.Text == "&Stop")
            {
                tmrMain.Start();

            }
        }



        private void frmMain_Resize(object sender, EventArgs e)
        {
            tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStrip.Width);
        }

        private void testingToolStrip_Click(object sender, EventArgs e)
        {
            if (testingToolStrip.Checked)
            {
                productionToolStrip.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStrip.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void productionToolStrip_Click(object sender, EventArgs e)
        {
            if (productionToolStrip.Checked)
            {
                testingToolStrip.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStrip.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                tslMain.Text = "Starting...";
                ProcessingQueue();
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = Color.LightCoral;
                tmrMain.Start();
                tslMain.Text = "Timed process started";
            }
            else
            {
                tslMain.Text = "Stopping...";
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = Color.LightGreen;

                tmrMain.Stop();
                tslMain.Text = "Timed process stopped";
            }
        }

        private void customersToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmCustomer custMaint = new frmCustomer();
            custMaint.Show();

        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }


}
