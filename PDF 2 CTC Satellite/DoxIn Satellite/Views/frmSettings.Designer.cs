﻿namespace DoxIn_Satellite.Views
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tcSettings = new System.Windows.Forms.TabControl();
            this.tabSystem = new System.Windows.Forms.TabPage();
            this.edAlertsTo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.edMainTimer = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tabDatabase = new System.Windows.Forms.TabPage();
            this.rtbDBCheck = new System.Windows.Forms.RichTextBox();
            this.btnCheckDB = new System.Windows.Forms.Button();
            this.edPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edUserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edInstance = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabCTCNode = new System.Windows.Forms.TabPage();
            this.rtbCTCTest = new System.Windows.Forms.RichTextBox();
            this.btnCTCTest = new System.Windows.Forms.Button();
            this.edCTCPassword = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.edCTCUsername = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.edCTCDatabase = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.edCTCServer = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tabComms = new System.Windows.Forms.TabPage();
            this.gbMailSecurity = new System.Windows.Forms.GroupBox();
            this.rbTLS = new System.Windows.Forms.RadioButton();
            this.rbSSL = new System.Windows.Forms.RadioButton();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.edEmailAddress = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.edMailPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edMailUsername = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edMailPort = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.edMailServer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabFilelocations = new System.Windows.Forms.TabPage();
            this.bbTestPath = new System.Windows.Forms.Button();
            this.edTestLocation = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.bbCustomPath = new System.Windows.Forms.Button();
            this.edCustomPath = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.bbFailLoc = new System.Windows.Forms.Button();
            this.edFailPath = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.bbOutLoc = new System.Windows.Forms.Button();
            this.edOutputLocation = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bbPickLoc = new System.Windows.Forms.Button();
            this.edPickupLocation = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnImageLocation = new System.Windows.Forms.Button();
            this.txtImageLocation = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tcSettings.SuspendLayout();
            this.tabSystem.SuspendLayout();
            this.tabDatabase.SuspendLayout();
            this.tabCTCNode.SuspendLayout();
            this.tabComms.SuspendLayout();
            this.gbMailSecurity.SuspendLayout();
            this.tabFilelocations.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(438, 285);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(519, 285);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tcSettings
            // 
            this.tcSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcSettings.Controls.Add(this.tabSystem);
            this.tcSettings.Controls.Add(this.tabDatabase);
            this.tcSettings.Controls.Add(this.tabCTCNode);
            this.tcSettings.Controls.Add(this.tabComms);
            this.tcSettings.Controls.Add(this.tabFilelocations);
            this.tcSettings.Location = new System.Drawing.Point(12, 12);
            this.tcSettings.Name = "tcSettings";
            this.tcSettings.SelectedIndex = 0;
            this.tcSettings.Size = new System.Drawing.Size(582, 267);
            this.tcSettings.TabIndex = 3;
            // 
            // tabSystem
            // 
            this.tabSystem.Controls.Add(this.edAlertsTo);
            this.tabSystem.Controls.Add(this.label18);
            this.tabSystem.Controls.Add(this.edMainTimer);
            this.tabSystem.Controls.Add(this.label16);
            this.tabSystem.Location = new System.Drawing.Point(4, 22);
            this.tabSystem.Name = "tabSystem";
            this.tabSystem.Padding = new System.Windows.Forms.Padding(3);
            this.tabSystem.Size = new System.Drawing.Size(574, 241);
            this.tabSystem.TabIndex = 3;
            this.tabSystem.Text = "System Settings";
            this.tabSystem.UseVisualStyleBackColor = true;
            // 
            // edAlertsTo
            // 
            this.edAlertsTo.Location = new System.Drawing.Point(115, 35);
            this.edAlertsTo.Name = "edAlertsTo";
            this.edAlertsTo.Size = new System.Drawing.Size(296, 20);
            this.edAlertsTo.TabIndex = 9;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 38);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "System Alerts to";
            // 
            // edMainTimer
            // 
            this.edMainTimer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edMainTimer.Location = new System.Drawing.Point(115, 9);
            this.edMainTimer.Name = "edMainTimer";
            this.edMainTimer.Size = new System.Drawing.Size(43, 20);
            this.edMainTimer.TabIndex = 5;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Main Timer Interval";
            // 
            // tabDatabase
            // 
            this.tabDatabase.Controls.Add(this.rtbDBCheck);
            this.tabDatabase.Controls.Add(this.btnCheckDB);
            this.tabDatabase.Controls.Add(this.edPassword);
            this.tabDatabase.Controls.Add(this.label4);
            this.tabDatabase.Controls.Add(this.edUserName);
            this.tabDatabase.Controls.Add(this.label3);
            this.tabDatabase.Controls.Add(this.edInstance);
            this.tabDatabase.Controls.Add(this.label2);
            this.tabDatabase.Controls.Add(this.edServer);
            this.tabDatabase.Controls.Add(this.label1);
            this.tabDatabase.Location = new System.Drawing.Point(4, 22);
            this.tabDatabase.Name = "tabDatabase";
            this.tabDatabase.Padding = new System.Windows.Forms.Padding(3);
            this.tabDatabase.Size = new System.Drawing.Size(574, 241);
            this.tabDatabase.TabIndex = 0;
            this.tabDatabase.Text = "Database Settings (Satellite)";
            this.tabDatabase.UseVisualStyleBackColor = true;
            // 
            // rtbDBCheck
            // 
            this.rtbDBCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbDBCheck.Location = new System.Drawing.Point(16, 175);
            this.rtbDBCheck.Name = "rtbDBCheck";
            this.rtbDBCheck.Size = new System.Drawing.Size(455, 66);
            this.rtbDBCheck.TabIndex = 13;
            this.rtbDBCheck.Text = "";
            // 
            // btnCheckDB
            // 
            this.btnCheckDB.Location = new System.Drawing.Point(16, 135);
            this.btnCheckDB.Name = "btnCheckDB";
            this.btnCheckDB.Size = new System.Drawing.Size(105, 23);
            this.btnCheckDB.TabIndex = 12;
            this.btnCheckDB.Text = "Test Connection";
            this.btnCheckDB.UseVisualStyleBackColor = true;
            this.btnCheckDB.Click += new System.EventHandler(this.btnCheckDB_Click);
            // 
            // edPassword
            // 
            this.edPassword.Location = new System.Drawing.Point(133, 89);
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(266, 20);
            this.edPassword.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Password";
            // 
            // edUserName
            // 
            this.edUserName.Location = new System.Drawing.Point(133, 63);
            this.edUserName.Name = "edUserName";
            this.edUserName.Size = new System.Drawing.Size(266, 20);
            this.edUserName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "User Name";
            // 
            // edInstance
            // 
            this.edInstance.Location = new System.Drawing.Point(133, 37);
            this.edInstance.Name = "edInstance";
            this.edInstance.Size = new System.Drawing.Size(266, 20);
            this.edInstance.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Database";
            // 
            // edServer
            // 
            this.edServer.Location = new System.Drawing.Point(133, 11);
            this.edServer.Name = "edServer";
            this.edServer.Size = new System.Drawing.Size(266, 20);
            this.edServer.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Database Server";
            // 
            // tabCTCNode
            // 
            this.tabCTCNode.Controls.Add(this.rtbCTCTest);
            this.tabCTCNode.Controls.Add(this.btnCTCTest);
            this.tabCTCNode.Controls.Add(this.edCTCPassword);
            this.tabCTCNode.Controls.Add(this.label20);
            this.tabCTCNode.Controls.Add(this.edCTCUsername);
            this.tabCTCNode.Controls.Add(this.label21);
            this.tabCTCNode.Controls.Add(this.edCTCDatabase);
            this.tabCTCNode.Controls.Add(this.label22);
            this.tabCTCNode.Controls.Add(this.edCTCServer);
            this.tabCTCNode.Controls.Add(this.label23);
            this.tabCTCNode.Location = new System.Drawing.Point(4, 22);
            this.tabCTCNode.Name = "tabCTCNode";
            this.tabCTCNode.Padding = new System.Windows.Forms.Padding(3);
            this.tabCTCNode.Size = new System.Drawing.Size(574, 241);
            this.tabCTCNode.TabIndex = 4;
            this.tabCTCNode.Text = "Database Settings (CTC Node)";
            this.tabCTCNode.UseVisualStyleBackColor = true;
            // 
            // rtbCTCTest
            // 
            this.rtbCTCTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbCTCTest.Location = new System.Drawing.Point(16, 163);
            this.rtbCTCTest.Name = "rtbCTCTest";
            this.rtbCTCTest.Size = new System.Drawing.Size(455, 66);
            this.rtbCTCTest.TabIndex = 23;
            this.rtbCTCTest.Text = "";
            // 
            // btnCTCTest
            // 
            this.btnCTCTest.Location = new System.Drawing.Point(16, 123);
            this.btnCTCTest.Name = "btnCTCTest";
            this.btnCTCTest.Size = new System.Drawing.Size(105, 23);
            this.btnCTCTest.TabIndex = 22;
            this.btnCTCTest.Text = "Test Connection";
            this.btnCTCTest.UseVisualStyleBackColor = true;
            this.btnCTCTest.Click += new System.EventHandler(this.btnCTCTest_Click);
            // 
            // edCTCPassword
            // 
            this.edCTCPassword.Location = new System.Drawing.Point(133, 89);
            this.edCTCPassword.Name = "edCTCPassword";
            this.edCTCPassword.Size = new System.Drawing.Size(266, 20);
            this.edCTCPassword.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(21, 92);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Password";
            // 
            // edCTCUsername
            // 
            this.edCTCUsername.Location = new System.Drawing.Point(133, 63);
            this.edCTCUsername.Name = "edCTCUsername";
            this.edCTCUsername.Size = new System.Drawing.Size(266, 20);
            this.edCTCUsername.TabIndex = 19;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(21, 66);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 18;
            this.label21.Text = "User Name";
            // 
            // edCTCDatabase
            // 
            this.edCTCDatabase.Location = new System.Drawing.Point(133, 37);
            this.edCTCDatabase.Name = "edCTCDatabase";
            this.edCTCDatabase.Size = new System.Drawing.Size(266, 20);
            this.edCTCDatabase.TabIndex = 17;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(21, 40);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 16;
            this.label22.Text = "Database";
            // 
            // edCTCServer
            // 
            this.edCTCServer.Location = new System.Drawing.Point(133, 11);
            this.edCTCServer.Name = "edCTCServer";
            this.edCTCServer.Size = new System.Drawing.Size(266, 20);
            this.edCTCServer.TabIndex = 15;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(21, 14);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(106, 13);
            this.label23.TabIndex = 14;
            this.label23.Text = "CTC Node Database";
            // 
            // tabComms
            // 
            this.tabComms.Controls.Add(this.gbMailSecurity);
            this.tabComms.Controls.Add(this.edEmailAddress);
            this.tabComms.Controls.Add(this.label9);
            this.tabComms.Controls.Add(this.edMailPassword);
            this.tabComms.Controls.Add(this.label8);
            this.tabComms.Controls.Add(this.edMailUsername);
            this.tabComms.Controls.Add(this.label7);
            this.tabComms.Controls.Add(this.edMailPort);
            this.tabComms.Controls.Add(this.label6);
            this.tabComms.Controls.Add(this.edMailServer);
            this.tabComms.Controls.Add(this.label5);
            this.tabComms.Location = new System.Drawing.Point(4, 22);
            this.tabComms.Name = "tabComms";
            this.tabComms.Padding = new System.Windows.Forms.Padding(3);
            this.tabComms.Size = new System.Drawing.Size(574, 241);
            this.tabComms.TabIndex = 1;
            this.tabComms.Text = "Communications ";
            this.tabComms.UseVisualStyleBackColor = true;
            // 
            // gbMailSecurity
            // 
            this.gbMailSecurity.Controls.Add(this.rbTLS);
            this.gbMailSecurity.Controls.Add(this.rbSSL);
            this.gbMailSecurity.Controls.Add(this.rbNone);
            this.gbMailSecurity.Location = new System.Drawing.Point(399, 49);
            this.gbMailSecurity.Name = "gbMailSecurity";
            this.gbMailSecurity.Size = new System.Drawing.Size(110, 94);
            this.gbMailSecurity.TabIndex = 14;
            this.gbMailSecurity.TabStop = false;
            this.gbMailSecurity.Text = "Mail Security";
            // 
            // rbTLS
            // 
            this.rbTLS.AutoSize = true;
            this.rbTLS.Location = new System.Drawing.Point(13, 68);
            this.rbTLS.Name = "rbTLS";
            this.rbTLS.Size = new System.Drawing.Size(45, 17);
            this.rbTLS.TabIndex = 2;
            this.rbTLS.TabStop = true;
            this.rbTLS.Text = "TLS";
            this.rbTLS.UseVisualStyleBackColor = true;
            // 
            // rbSSL
            // 
            this.rbSSL.AutoSize = true;
            this.rbSSL.Location = new System.Drawing.Point(13, 45);
            this.rbSSL.Name = "rbSSL";
            this.rbSSL.Size = new System.Drawing.Size(45, 17);
            this.rbSSL.TabIndex = 1;
            this.rbSSL.TabStop = true;
            this.rbSSL.Text = "SSL";
            this.rbSSL.UseVisualStyleBackColor = true;
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Location = new System.Drawing.Point(13, 22);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 0;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            // 
            // edEmailAddress
            // 
            this.edEmailAddress.Location = new System.Drawing.Point(109, 122);
            this.edEmailAddress.Name = "edEmailAddress";
            this.edEmailAddress.Size = new System.Drawing.Size(266, 20);
            this.edEmailAddress.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Email Address";
            // 
            // edMailPassword
            // 
            this.edMailPassword.Location = new System.Drawing.Point(109, 89);
            this.edMailPassword.Name = "edMailPassword";
            this.edMailPassword.Size = new System.Drawing.Size(266, 20);
            this.edMailPassword.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Password";
            // 
            // edMailUsername
            // 
            this.edMailUsername.Location = new System.Drawing.Point(109, 56);
            this.edMailUsername.Name = "edMailUsername";
            this.edMailUsername.Size = new System.Drawing.Size(266, 20);
            this.edMailUsername.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Username";
            // 
            // edMailPort
            // 
            this.edMailPort.Location = new System.Drawing.Point(431, 23);
            this.edMailPort.Name = "edMailPort";
            this.edMailPort.Size = new System.Drawing.Size(38, 20);
            this.edMailPort.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(396, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Port:";
            // 
            // edMailServer
            // 
            this.edMailServer.Location = new System.Drawing.Point(109, 23);
            this.edMailServer.Name = "edMailServer";
            this.edMailServer.Size = new System.Drawing.Size(266, 20);
            this.edMailServer.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mail Server:";
            // 
            // tabFilelocations
            // 
            this.tabFilelocations.Controls.Add(this.btnImageLocation);
            this.tabFilelocations.Controls.Add(this.txtImageLocation);
            this.tabFilelocations.Controls.Add(this.label10);
            this.tabFilelocations.Controls.Add(this.bbTestPath);
            this.tabFilelocations.Controls.Add(this.edTestLocation);
            this.tabFilelocations.Controls.Add(this.label25);
            this.tabFilelocations.Controls.Add(this.bbCustomPath);
            this.tabFilelocations.Controls.Add(this.edCustomPath);
            this.tabFilelocations.Controls.Add(this.label24);
            this.tabFilelocations.Controls.Add(this.bbFailLoc);
            this.tabFilelocations.Controls.Add(this.edFailPath);
            this.tabFilelocations.Controls.Add(this.label19);
            this.tabFilelocations.Controls.Add(this.bbOutLoc);
            this.tabFilelocations.Controls.Add(this.edOutputLocation);
            this.tabFilelocations.Controls.Add(this.label14);
            this.tabFilelocations.Controls.Add(this.bbPickLoc);
            this.tabFilelocations.Controls.Add(this.edPickupLocation);
            this.tabFilelocations.Controls.Add(this.label13);
            this.tabFilelocations.Location = new System.Drawing.Point(4, 22);
            this.tabFilelocations.Name = "tabFilelocations";
            this.tabFilelocations.Padding = new System.Windows.Forms.Padding(3);
            this.tabFilelocations.Size = new System.Drawing.Size(574, 241);
            this.tabFilelocations.TabIndex = 2;
            this.tabFilelocations.Text = "File Locations";
            this.tabFilelocations.UseVisualStyleBackColor = true;
            // 
            // bbTestPath
            // 
            this.bbTestPath.Location = new System.Drawing.Point(402, 64);
            this.bbTestPath.Name = "bbTestPath";
            this.bbTestPath.Size = new System.Drawing.Size(25, 23);
            this.bbTestPath.TabIndex = 27;
            this.bbTestPath.Text = "...";
            this.bbTestPath.UseVisualStyleBackColor = true;
            this.bbTestPath.Click += new System.EventHandler(this.bbTestPath_Click);
            // 
            // edTestLocation
            // 
            this.edTestLocation.Location = new System.Drawing.Point(137, 65);
            this.edTestLocation.Name = "edTestLocation";
            this.edTestLocation.Size = new System.Drawing.Size(266, 20);
            this.edTestLocation.TabIndex = 26;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(16, 68);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(72, 13);
            this.label25.TabIndex = 28;
            this.label25.Text = "Test Location";
            // 
            // bbCustomPath
            // 
            this.bbCustomPath.Location = new System.Drawing.Point(402, 37);
            this.bbCustomPath.Name = "bbCustomPath";
            this.bbCustomPath.Size = new System.Drawing.Size(25, 23);
            this.bbCustomPath.TabIndex = 21;
            this.bbCustomPath.Text = "...";
            this.bbCustomPath.UseVisualStyleBackColor = true;
            this.bbCustomPath.Click += new System.EventHandler(this.bbCustomPath_Click);
            // 
            // edCustomPath
            // 
            this.edCustomPath.Location = new System.Drawing.Point(137, 39);
            this.edCustomPath.Name = "edCustomPath";
            this.edCustomPath.Size = new System.Drawing.Size(266, 20);
            this.edCustomPath.TabIndex = 20;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(16, 42);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(66, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "Custom Files";
            // 
            // bbFailLoc
            // 
            this.bbFailLoc.Location = new System.Drawing.Point(402, 11);
            this.bbFailLoc.Name = "bbFailLoc";
            this.bbFailLoc.Size = new System.Drawing.Size(25, 23);
            this.bbFailLoc.TabIndex = 7;
            this.bbFailLoc.Text = "...";
            this.bbFailLoc.UseVisualStyleBackColor = true;
            this.bbFailLoc.Click += new System.EventHandler(this.bbFailLoc_Click);
            // 
            // edFailPath
            // 
            this.edFailPath.Location = new System.Drawing.Point(137, 13);
            this.edFailPath.Name = "edFailPath";
            this.edFailPath.Size = new System.Drawing.Size(266, 20);
            this.edFailPath.TabIndex = 6;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "Failure Path";
            // 
            // bbOutLoc
            // 
            this.bbOutLoc.Location = new System.Drawing.Point(402, 117);
            this.bbOutLoc.Name = "bbOutLoc";
            this.bbOutLoc.Size = new System.Drawing.Size(25, 23);
            this.bbOutLoc.TabIndex = 11;
            this.bbOutLoc.Text = "...";
            this.bbOutLoc.UseVisualStyleBackColor = true;
            this.bbOutLoc.Click += new System.EventHandler(this.bbOutLoc_Click);
            // 
            // edOutputLocation
            // 
            this.edOutputLocation.Location = new System.Drawing.Point(137, 119);
            this.edOutputLocation.Name = "edOutputLocation";
            this.edOutputLocation.Size = new System.Drawing.Size(266, 20);
            this.edOutputLocation.TabIndex = 10;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "OutPut Location";
            // 
            // bbPickLoc
            // 
            this.bbPickLoc.Location = new System.Drawing.Point(402, 90);
            this.bbPickLoc.Name = "bbPickLoc";
            this.bbPickLoc.Size = new System.Drawing.Size(25, 23);
            this.bbPickLoc.TabIndex = 9;
            this.bbPickLoc.Text = "...";
            this.bbPickLoc.UseVisualStyleBackColor = true;
            this.bbPickLoc.Click += new System.EventHandler(this.bbPickLoc_Click);
            // 
            // edPickupLocation
            // 
            this.edPickupLocation.Location = new System.Drawing.Point(137, 92);
            this.edPickupLocation.Name = "edPickupLocation";
            this.edPickupLocation.Size = new System.Drawing.Size(266, 20);
            this.edPickupLocation.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Pickup Location";
            // 
            // btnImageLocation
            // 
            this.btnImageLocation.Location = new System.Drawing.Point(402, 143);
            this.btnImageLocation.Name = "btnImageLocation";
            this.btnImageLocation.Size = new System.Drawing.Size(25, 23);
            this.btnImageLocation.TabIndex = 30;
            this.btnImageLocation.Text = "...";
            this.btnImageLocation.UseVisualStyleBackColor = true;
            this.btnImageLocation.Click += new System.EventHandler(this.btnImageLocation_Click);
            // 
            // txtImageLocation
            // 
            this.txtImageLocation.Location = new System.Drawing.Point(137, 145);
            this.txtImageLocation.Name = "txtImageLocation";
            this.txtImageLocation.Size = new System.Drawing.Size(266, 20);
            this.txtImageLocation.TabIndex = 29;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 148);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Location of Image Files";
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 320);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tcSettings);
            this.Name = "frmSettings";
            this.Text = "System Settings";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.tcSettings.ResumeLayout(false);
            this.tabSystem.ResumeLayout(false);
            this.tabSystem.PerformLayout();
            this.tabDatabase.ResumeLayout(false);
            this.tabDatabase.PerformLayout();
            this.tabCTCNode.ResumeLayout(false);
            this.tabCTCNode.PerformLayout();
            this.tabComms.ResumeLayout(false);
            this.tabComms.PerformLayout();
            this.gbMailSecurity.ResumeLayout(false);
            this.gbMailSecurity.PerformLayout();
            this.tabFilelocations.ResumeLayout(false);
            this.tabFilelocations.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tcSettings;
        private System.Windows.Forms.TabPage tabSystem;
        private System.Windows.Forms.TextBox edAlertsTo;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox edMainTimer;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabDatabase;
        private System.Windows.Forms.RichTextBox rtbDBCheck;
        private System.Windows.Forms.Button btnCheckDB;
        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edInstance;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabCTCNode;
        private System.Windows.Forms.RichTextBox rtbCTCTest;
        private System.Windows.Forms.Button btnCTCTest;
        private System.Windows.Forms.TextBox edCTCPassword;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox edCTCUsername;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox edCTCDatabase;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox edCTCServer;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage tabComms;
        private System.Windows.Forms.TextBox edEmailAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox edMailPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edMailUsername;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edMailPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edMailServer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabFilelocations;
        private System.Windows.Forms.Button bbTestPath;
        private System.Windows.Forms.TextBox edTestLocation;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button bbCustomPath;
        private System.Windows.Forms.TextBox edCustomPath;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button bbFailLoc;
        private System.Windows.Forms.TextBox edFailPath;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button bbOutLoc;
        private System.Windows.Forms.TextBox edOutputLocation;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button bbPickLoc;
        private System.Windows.Forms.TextBox edPickupLocation;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox gbMailSecurity;
        private System.Windows.Forms.RadioButton rbSSL;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.RadioButton rbTLS;
        private System.Windows.Forms.Button btnImageLocation;
        private System.Windows.Forms.TextBox txtImageLocation;
        private System.Windows.Forms.Label label10;
    }
}