﻿using DoxIn_Satellite.Resources;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DoxIn_Satellite.Views
{
    public partial class frmSettings : Form
    {
        #region members

        #endregion

        #region properties

        #endregion

        #region Constructors
        public frmSettings()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods


        protected void SaveSettings()
        {
            SystemSettings settings = new SystemSettings(Globals.AppConfig);
            settings.SetElement("SatelliteDatabase", "ServerName", edServer.Text);
            settings.SetElement("SatelliteDatabase", "Database", edInstance.Text);
            settings.SetElement("SatelliteDatabase", "UserName", edUserName.Text);
            settings.SetElement("SatelliteDatabase", "Password", edPassword.Text);
            settings.SetElement("NodeDatabase", "ServerName", edCTCServer.Text);
            settings.SetElement("NodeDatabase", "Database", edCTCDatabase.Text);
            settings.SetElement("NodeDatabase", "UserName", edCTCUsername.Text);
            settings.SetElement("NodeDatabase", "Password", edCTCPassword.Text);
            settings.SetElement("Config", "PickupPath", edPickupLocation.Text);
            settings.SetElement("Config", "FailPath", edFailPath.Text);
            settings.SetElement("Config", "OutputPath", edOutputLocation.Text);
            settings.SetElement("Config", "TestPath", edTestLocation.Text);
            settings.SetElement("Config", "AlertsTo", edAlertsTo.Text);
            settings.SetElement("Config", "Timer", edMainTimer.Text);
            settings.SetElement("Config", "ImagePath", txtImageLocation.Text);
            settings.SetElement("Communication", "MailServer", edMailServer.Text);
            settings.SetElement("Communication", "Port", edMailPort.Text);
            settings.SetElement("Communication", "UserName", edMailUsername.Text);
            settings.SetElement("Communication", "Password", edMailPassword.Text);
            settings.SetElement("Communication", "EmailFrom", edEmailAddress.Text);

            //var radioSecurity = gbMailSecurity.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            //settings.SetElement("Communication", "SSL", radioSecurity.Text);


            settings.UpdateSettings();
            MessageBox.Show("Settings Saved");
            this.Close();
        }

        private void LoadSettings()
        {
            edAlertsTo.Text = Globals.AlertsTo;
            // edArchiveLocation.Text = Globals.ArchivePath;
            edCTCDatabase.Text = Globals.NodeConnManager != null ? Globals.NodeConnManager.DatabaseName : string.Empty;
            edCTCServer.Text = Globals.NodeConnManager != null ? Globals.NodeConnManager.ServerName : string.Empty;
            edCTCUsername.Text = Globals.NodeConnManager != null ? Globals.NodeConnManager.User : string.Empty;
            edCTCPassword.Text = Globals.NodeConnManager != null ? Globals.NodeConnManager.Password : string.Empty;
            edServer.Text = Globals.SattConnManager != null ? Globals.SattConnManager.ServerName : string.Empty;
            edInstance.Text = Globals.SattConnManager != null ? Globals.SattConnManager.DatabaseName : string.Empty;
            edUserName.Text = Globals.SattConnManager != null ? Globals.SattConnManager.User : string.Empty;
            edPassword.Text = Globals.SattConnManager != null ? Globals.SattConnManager.Password : string.Empty;
            edPickupLocation.Text = Globals.PickupPath;
            edFailPath.Text = Globals.FailPath;
            edOutputLocation.Text = Globals.OutPutPath;
            edTestLocation.Text = Globals.TestPath;
            txtImageLocation.Text = Globals.ImagePath;
            edMailServer.Text = Globals.MailServerSettings.Server;
            edMailUsername.Text = Globals.MailServerSettings.UserName;
            edMailPassword.Text = Globals.MailServerSettings.Password;
            edEmailAddress.Text = Globals.MailServerSettings.Email;
            edMailPort.Text = Globals.MailServerSettings.Port.ToString();
            //if (Globals.MailServerSettings.IsSecure)
            //{
            //    switch (Globals.MailServerSettings.)
            //    {
            //        case "None":
            //            rbNone.Checked = true;
            //            break;
            //        case "SSL":
            //            rbSSL.Checked = true;
            //            break;
            //        case "TLS":
            //            rbTLS.Checked = true;
            //            break;
            //    }
            //}

            // edWinSCPPath.Text = Globals.WinSCPPath;
            edMainTimer.Text = Globals.MainTimerInt.ToString();
        }



        private void frmSettings_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void CheckDb(string server, string db, string user, string password)
        {
            rtbDBCheck.Clear();
            rtbDBCheck.Text = "Data Source=" + server + ";Initial Catalog=" + db + ";User ID = " + user + ";Password=" + password + ";" + Environment.NewLine;
            rtbDBCheck.Text += "Testing Connection" + Environment.NewLine;
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = "Data Source=" + server + ";Initial Catalog=" + db + ";User ID = " + user + ";Password=" + password + ";";
            try
            {
                sqlconn.Open();
                rtbDBCheck.Text += " Connected Successfully." + Environment.NewLine;
            }
            catch (SqlException ex)
            {
                rtbDBCheck.Text += "Error found :" + ex.Message + Environment.NewLine;
            }
        }






        #endregion

        #region Button Actions
        private void btnCheckDB_Click(object sender, EventArgs e)
        {
            CheckDb(edServer.Text, edInstance.Text, edUserName.Text, edPassword.Text);
        }

        private void btnCTCTest_Click(object sender, EventArgs e)
        {
            CheckDb(edCTCServer.Text, edCTCDatabase.Text, edCTCUsername.Text, edCTCPassword.Text);

        }
        private void bbOutLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edOutputLocation.Text;
            fd.ShowDialog();
            edOutputLocation.Text = fd.SelectedPath;
        }

        private void bbPickLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edPickupLocation.Text;
            fd.ShowDialog();
            edPickupLocation.Text = fd.SelectedPath;
        }

        private void bbTestPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edTestLocation.Text;
            fd.ShowDialog();
            edTestLocation.Text = fd.SelectedPath;
        }

        private void bbCustomPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edCustomPath.Text;
            fd.ShowDialog();
            edCustomPath.Text = fd.SelectedPath;
        }

        private void bbFailLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edFailPath.Text;
            fd.ShowDialog();
            edFailPath.Text = fd.SelectedPath;
        }
        private void btnImageLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtImageLocation.Text;
            fd.ShowDialog();
            txtImageLocation.Text = fd.SelectedPath;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }
        #endregion

        #region Helpers

        #endregion


    }
}
