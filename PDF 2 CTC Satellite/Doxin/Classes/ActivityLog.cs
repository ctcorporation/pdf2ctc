﻿using DoxIn.DTO;
using DoxIn.Models;
using System.Collections.Generic;
using System.Linq;

namespace DoxIn.Classes
{
    public class ActivityLog
    {

        #region members
        string _connstring;
        #endregion

        #region propoerties
        public string ConnString
        {
            get
            {
                return _connstring;
            }
            set
            {
                _connstring = value;
            }
        }
        #endregion

        #region constructor
        public ActivityLog(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region methods
        public List<Log> GetLog(string logType)
        {
            using (IUnitOfWork uow = new DTO.UnitOfWork(new CommonEntity(_connstring)))
            {
                return uow.Logs.Find(x => x.LO_LogType == logType, o => o.OrderByDescending(l => l.LO_Date)).ToList();
            }

        }

        public void ClearLog(string logType)
        {
            using (IUnitOfWork uow = new DTO.UnitOfWork(new CommonEntity(_connstring)))
            {
                var logList = uow.Logs.Find(x => x.LO_LogType == logType).ToList();
                uow.Logs.RemoveRange(logList);
                uow.Complete();
            }

        }
        #endregion
        #region helpers
        #endregion

    }
}
