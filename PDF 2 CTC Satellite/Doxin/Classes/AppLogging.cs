﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DoxIn.Classes
{
    public class AppLogging : IDisposable
    {
        #region members
        string _connString;
        private bool disposedValue;
        string _appName;
        string _serverName;
        #endregion


        #region properties
        public string ConnString
        {
            get
            {
                return _connString;
            }
            set
            {
                _connString = value;
            }
        }





        public string AppName
        {
            get
            { return _appName; }
            set
            {
                _appName = value;
            }
        }

        public string ServerName
        {
            get
            { return _serverName; }
            set
            { _serverName = value; }
        }

        public AppLog AppLog { get; set; }
        #endregion

        #region constructors
        public AppLogging(string connString, string serverName, string appName)
        {
            ConnString = connString;
            AppName = appName;
            ServerName = serverName;
        }
        public AppLogging(string connString)
        {
            ConnString = connString;
            _serverName = Environment.MachineName;

        }

        #endregion

        #region methods

        internal bool PurgeLog(bool all, DateTime dateBefore, bool info, bool warn, bool err)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                List<AppLog> purgeLogs = new List<AppLog>();
                if (all)
                {
                    purgeLogs = uow.AppLogs.Find(x => x.GL_AppName == _appName).ToList();
                }
                else
                {
                    var logs = uow.AppLogs.Find(x => x.GL_AppName == _appName && x.GL_Date <= dateBefore).ToList();

                    if (info)
                    {
                        purgeLogs.AddRange(from l in logs
                                           where l.GL_MessageType == "Information"
                                           select l);
                    }
                    if (warn)
                    {
                        purgeLogs.AddRange(from l in logs
                                           where l.GL_MessageType == "Warning"
                                           select l);
                    }
                    if (err)
                    {
                        purgeLogs.AddRange(from l in logs
                                           where l.GL_MessageType == "Error"
                                           select l);
                    }
                }
                var m = MethodBase.GetCurrentMethod();

                uow.AppLogs.RemoveRange(purgeLogs);
                uow.AppLogs.Add(new AppLog
                {
                    GL_AppName = _appName,
                    GL_Date = DateTime.Now,
                    GL_MessageType = "Information",
                    GL_MessageLevel = 1,
                    GL_ProcName = m.Name,
                    GL_MessageDetail = "Logs Purged (" + purgeLogs.Count + " records purged)",
                    GL_ServerName = _serverName
                });
                uow.Complete();
            }

            return true;
        }
        public List<AppLog> GetLogs()
        {
            if (string.IsNullOrEmpty(_appName))
            {
                return null;
            }
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                return (uow.AppLogs.Find(x => x.GL_AppName.Trim() == _appName).OrderByDescending(x => x.GL_Date).ToList());
            }

        }
        public bool AddLog(AppLog log)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                uow.AppLogs.Add(log);
                uow.Complete();
                return true;
            }

            return false;
        }

        public bool ClearLogs()
        {
            return false;
        }
        #endregion

        #region helpers



        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~AppLogging()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }


        #endregion

    }
}
