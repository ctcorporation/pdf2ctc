﻿using DoxIn.DTO;
using DoxIn.Models;
using DoxIn.Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using XML_Locker;
using XMLLocker.CTC;

namespace DoxIn.Classes
{
    public class ProcessFiles
    {
        #region members
        string _connstring;
        string _logPath;
        string _logType;
        string _logField;
        string _outputPath;
        string _summary = string.Empty;
        const string dateFormat = "dd/MM/yyyyTHH:mm:ss";
        #endregion

        #region properties
        public string ConnString
        {
            get
            {
                return _connstring;
            }
            set
            {
                _connstring = value;
            }
        }
        public string OutputPath
        {
            get
            { return _outputPath; }
            set
            {
                _outputPath = value;
            }

        }


        public string LogPath
        {
            get
            {
                return _logPath;
            }
            set
            {
                _logPath = value;
            }
        }
        #endregion

        #region Constructors
        public ProcessFiles(string connstring, string appLogPath)
        {
            LogPath = appLogPath;

            ConnString = connstring;




        }

        public string DoConvert()
        {
            List<ConvertedFile> releaseList = new List<ConvertedFile>();

            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                releaseList = uow.ConvertedFiles.Find(x => x.FC_Released && !x.FC_Converted).ToList();

                bool released = false;
                foreach (var file in releaseList)
                {
                    _summary += string.Format("{0:g} :", DateTime.Now) + "Converting " + file.FC_OriginalFileName + Environment.NewLine;
                    switch (file.FC_DocumentIdentifier)
                    {
                        case "CINV":
                            released = ConvertCINV(file);
                            break;
                        case "WORD":
                            released = ConvertWORD(file);
                            break;
                        case "HBL":
                            released = ConvertHBL(file);
                            break;

                    }
                    if (released)
                    {
                        _summary += string.Format("{0:g} :", DateTime.Now) + "Converted " + file.FC_OriginalFileName + Environment.NewLine;
                        var convertedFile = uow.ConvertedFiles.Get(file.FC_ID);
                        // convertedFile.FC_ReleaseDate = DateTime.Now;
                        convertedFile.FC_ConvertDate = DateTime.Now;
                        convertedFile.FC_Converted = true;
                        uow.Complete();
                    }
                }
            }
            return _summary;
        }

        private bool ConvertHBL(ConvertedFile file)
        {
            return false;

        }

        private bool ConvertWORD(ConvertedFile file)
        {
            return false;
        }

        private bool ConvertCINV(ConvertedFile file)
        {
            _logType = "Export";

            NodeFile common = new NodeFile();
            common.IdendtityMatrix = CreateIdentityMatrix(file);
            var ci = GetCommercialInvoice(file.FC_ID);
            if (ci == null)
            {
                return false;
            }

            common.CommercialInvoice = ci;
            CommonToCw commonToCw = new CommonToCw(LogPath);
            var cwFile = commonToCw.CreateCommercialInvoice(common, Globals.OutPutPath);
            //  ToCargowise commonFunc = new ToCargowise(LogPath);

            if (cwFile == null)
            {
                return false;
            }
            using (FileBuilder fileBuilder = new FileBuilder(Globals.OutPutPath))
            {
                String cwXML = fileBuilder.BuildXmlFile<XUS.UniversalInterchange>(
                    new System.Xml.Serialization.XmlSerializerNamespaces(),
                    "http://www.cargowise.com/Schemas/Universal/2011/11",
                    cwFile,
                    common.IdendtityMatrix.CustomerId,
                    common.CommercialInvoice.InvoiceNo, ".XML");
            }

            return true;


        }

        private NodeFileCommercialInvoice GetCommercialInvoice(Guid fC_ID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                var commonInvoice = (uow.CommonInvoices.Find(x => x.CI_ConvertedFileId == fC_ID)).FirstOrDefault();
                NodeFileCommercialInvoice invoice = new NodeFileCommercialInvoice();
                invoice.CountryOfOrigin = commonInvoice.CI_CountryOfOrigin;
                invoice.InvoiceCurrency = commonInvoice.CI_Currency;
                invoice.InvoiceNo = commonInvoice.CI_InvoiceNo;

                invoice.IncoTerm = commonInvoice.CI_InvoiceTerms;
                invoice.ShipmentNo = commonInvoice.CI_ShipmentNo;
                decimal totalAmount = 0;
                if (decimal.TryParse(commonInvoice.CI_TotalInvoiceAmount, out totalAmount))
                {
                    invoice.TotalInvoiceAmount = totalAmount;
                    invoice.TotalInvoiceAmountSpecified = true;
                }



                invoice.CustomerReferences = GetCustomerReferences(commonInvoice.CI_ID).ToArray();
                invoice.CommercialInvoiceLines = GetCommercialInvoiceLines(commonInvoice.CI_ID).ToArray();
                invoice.Companies = GetCompanies(commonInvoice.CI_ID).ToArray();
                if (invoice.Companies.Length == 0)
                {
                    _summary += string.Format("{0:g} :", DateTime.Now) + "Failed to Commercial Invoice (" + commonInvoice.CI_InvoiceNo + "). No Companies found" + Environment.NewLine;
                    return null;
                }
                invoice.Dates = GetDates(commonInvoice.CI_ID).ToArray();
                return invoice;
            }

        }

        private List<OrderLineElement> GetCommercialInvoiceLines(Guid cI_ID)
        {
            List<OrderLineElement> lines = new List<OrderLineElement>();
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                var invoiceLines = uow.CommonOrderLines.Find(x => x.OL_InvoiceID == cI_ID, y => y.OrderBy(ln => ln.OL_LineNo)).ToList();
                if (invoiceLines.Count > 0)
                {
                    foreach (var inv in invoiceLines)
                    {
                        OrderLineElement newLine = new OrderLineElement();
                        int lineno;
                        newLine.LineNo = int.TryParse(inv.OL_LineNo, out lineno) ? lineno : 0;
                        decimal d;
                        newLine.InvoiceQty = decimal.TryParse(inv.OL_InvoicedQty, out d) ? d : 0;
                        newLine.InvoiceQtySpecified = d > 0 ? true : false;
                        newLine.UnitPrice = decimal.TryParse(inv.OL_UnitPrice, out d) ? d : 0;
                        newLine.UnitPriceSpecified = d > 0 ? true : false;
                        newLine.LineTotal = decimal.TryParse(inv.OL_LineTotal, out d) ? d : 0;
                        if (newLine.UnitPrice > 0 && newLine.InvoiceQty > 0)
                        {
                            if (newLine.LineTotal == 0)
                            {
                                newLine.LineTotal = newLine.InvoiceQty * newLine.UnitPrice;
                            }
                        }
                        newLine.LineTotalSpecified = newLine.LineTotal > 0 ? true : false;
                        newLine.PackageQty = decimal.TryParse(inv.OL_PackageQty, out d) ? d : 0;
                        newLine.PackageQtySpecified = d > 0 ? true : false;
                        newLine.OrderQty = decimal.TryParse(inv.OL_OrderedQty, out d) ? d : 0;
                        newLine.OrderQtySpecified = d > 0 ? true : false;
                        newLine.Product = new ProductElement
                        {
                            Description = inv.OL_ProductDescription,
                            Code = inv.OL_ProductCode,
                            Barcode = inv.OL_Barcode,
                            StockKeepingUnit = inv.OL_PackageUnit
                        };
                        newLine.PackageUnit = inv.OL_PackageUnit;
                        newLine.Tariff = inv.OL_Tariff;
                        newLine.Volume = decimal.TryParse(inv.OL_Volume, out d) ? d : 0;
                        newLine.VolumeSpecified = d > 0 ? true : false;
                        newLine.Weight = decimal.TryParse(inv.OL_UnitWeight, out d) ? d : 0;
                        newLine.WeightSpecified = d > 0 ? true : false;
                        newLine.WeightSpecified = d > 0 ? true : false;
                        newLine.UnitOfMeasure = inv.OL_UnitOfMeasurement;
                        OrderLineElementVolumeUnit v = new OrderLineElementVolumeUnit();
                        newLine.VolumeUnit = Enum.TryParse<OrderLineElementVolumeUnit>(inv.OL_VolumeUnit, true, out v) ? v : OrderLineElementVolumeUnit.M3;
                        newLine.VolumeUnitSpecified = !true;
                        OrderLineElementWeightUnit w = new OrderLineElementWeightUnit();
                        newLine.WeightUnit = Enum.TryParse<OrderLineElementWeightUnit>(inv.OL_WeightUnit, true, out w) ? w : OrderLineElementWeightUnit.KG;
                        newLine.WeightUnitSpecified = true;
                        lines.Add(newLine);
                    }
                }
                return lines;

            }
        }

        private List<DateElement> GetDates(Guid cI_ID)
        {
            List<DateElement> dates = new List<DateElement>();
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                var dateList = (from d in uow.CommonDates.GetAll()
                                where d.DA_InvoiceId == cI_ID
                                join dt in uow.DateTypes.GetAll() on d.DA_DateTypeId equals dt.DT_Id into dtl
                                from dateType in dtl.DefaultIfEmpty()
                                select new
                                {
                                    d.DA_ActualDate,
                                    d.DA_EstimateDate,
                                    value = dateType ?? null
                                }).ToList()
                               .Select(x => new CommonDate
                               {
                                   DA_ActualDate = x.DA_ActualDate,
                                   DA_EstimateDate = x.DA_EstimateDate,
                                   DA_DateType = x.value
                               });

                if (dateList.Count() > 0)
                {
                    foreach (var date in dateList)
                    {
                        DateElement newDate = new DateElement();
                        try
                        {
                            DateTime d;
                            if (!string.IsNullOrEmpty(date.DA_ActualDate))
                            {
                                if (DateTime.TryParse(date.DA_ActualDate, out d))
                                {
                                    newDate.ActualDate = d.ToString(dateFormat);
                                }
                                else
                                {
                                    Exception ex = new Exception("Invalid Date format");
                                    ex.Data.Add("Value", date.DA_ActualDate);
                                    ex.Data.Add("FieldName", "DA_ActualDate");

                                    throw ex;
                                }
                            }
                            if (!string.IsNullOrEmpty(date.DA_EstimateDate))
                            {
                                if (DateTime.TryParse(date.DA_EstimateDate, out d))
                                {
                                    newDate.EstimateDate = d.ToString(dateFormat);
                                }
                                else
                                {
                                    Exception ex = new Exception("Invalid Date format");
                                    ex.Data.Add("Value", date.DA_EstimateDate);
                                    ex.Data.Add("FieldName", "DA_EstimateDate");

                                    throw ex;
                                }
                            }
                            newDate.DateType = Extensions.ToEnum<DateElementDateType>(date.DA_DateType.DT_DateType, DateElementDateType.None);
                            dates.Add(newDate);
                        }
                        catch (Exception ex)
                        {

                            Log log = new Log();
                            log.LO_Date = DateTime.Now;
                            log.LO_ParentId = cI_ID;
                            log.LO_LogType = _logType;
                            MethodBase m = MethodBase.GetCurrentMethod();
                            log.LO_Operation = m.ReflectedType.Name + @"\" + m.Name;
                            log.LO_FieldName = ex.Data["FieldName"].ToString();
                            log.LO_Message = ex.Message;
                            uow.Logs.Add(log);
                            date.DA_ErrorsFound = true;
                            ErrorMessage error = new ErrorMessage
                            {
                                EM_DateId = date.DA_Id,
                                EM_Message = ex.Message,
                                EM_DateTime = DateTime.Now,
                                EM_ScannedValue = ex.Data["Value"].ToString(),
                                EM_TagName = "GetDate",
                                EM_Type = "Release"
                            };

                        }
                    }
                }

                uow.Complete();



            }
            return dates;
        }



        private string GetDateType(Guid? dA_DateTypeId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity()))
            {
                if (dA_DateTypeId == null)
                {
                    return string.Empty;
                }
                return uow.DateTypes.Get((Guid)dA_DateTypeId).DT_DateType;
            }
        }

        private List<CompanyElement> GetCompanies(Guid cI_ID)
        {
            List<CompanyElement> companies = new List<CompanyElement>();
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                var cList = (from comps in uow.Companies.GetAll()
                             where comps.CO_InvoiceId == cI_ID
                             join ct in uow.CompanyTypes.GetAll() on comps.CO_CompanyTypeId equals ct.CT_ID into ctl
                             from ctLookup in ctl.DefaultIfEmpty()
                             select new
                             {
                                 comps.CO_Name,
                                 comps.CO_Code,
                                 comps.CO_Address1,
                                 comps.CO_Address2,
                                 comps.CO_Address3,
                                 comps.CO_City,
                                 comps.CO_State,
                                 comps.CO_PostCode,
                                 value = ctLookup ?? null,
                                 comps.CO_ContactName,
                                 comps.CO_Country,
                                 comps.CO_EmailAddress
                             }).ToList()
                            .Select(x => new Models.Company
                            {
                                CO_ContactName = x.CO_ContactName,
                                CO_Address1 = x.CO_Address2,
                                CO_Address2 = x.CO_Address2,
                                CO_Address3 = x.CO_Address3,
                                CO_City = x.CO_City,
                                CO_Name = x.CO_Name,
                                CO_Code = x.CO_Code,
                                CO_State = x.CO_State,
                                CO_PostCode = x.CO_PostCode,
                                CO_EmailAddress = x.CO_EmailAddress,
                                CO_Country = x.CO_Country,
                                CO_CompanyType = x.value
                            });
                if (cList.Count() == 0)
                {
                    Exception ex = new Exception("No Companies are listed. Must have at least Supplier and Buyer");
                    ex.Data.Add("FieldName", string.Empty);
                    throw ex;
                }
                foreach (var company in cList)
                {
                    try
                    {
                        CompanyElement co = new CompanyElement();
                        co.CompanyName = company.CO_Name;
                        co.CompanyCode = company.CO_Code;
                        co.Address1 = company.CO_Address1;
                        co.Address2 = company.CO_Address2;
                        co.City = company.CO_City;
                        co.ContactName = company.CO_ContactName;
                        co.Country = company.CO_Country;
                        co.PostCode = company.CO_PostCode;
                        co.State = company.CO_State;
                        co.CompanyType = Extensions.ToEnum<CompanyElementCompanyType>(company.CO_CompanyType.CT_CompanyType, CompanyElementCompanyType.None);
                        companies.Add(co);
                    }
                    catch (Exception ex)
                    {
                        Log log = new Log();
                        log.LO_Date = DateTime.Now;
                        log.LO_ParentId = company != null ? company.CO_ID : cI_ID;
                        log.LO_LogType = _logType;
                        MethodBase m = MethodBase.GetCurrentMethod();
                        log.LO_Operation = m.ReflectedType.Name + @"\" + m.Name;
                        log.LO_FieldName = ex.Data["FieldName"] != null ? ex.Data["FieldName"].ToString() : string.Empty;
                        log.LO_Message = ex.Message;
                        uow.Logs.Add(log);
                        if (company != null)
                        {
                            company.CO_ErrorsFound = true;
                            uow.ErrorMessages.Add(new ErrorMessage
                            {
                                EM_CompanyId = company.CO_ID,
                                EM_Message = ex.Message,
                                EM_DateTime = DateTime.Now,
                                EM_ScannedValue = company.CO_Name,
                                EM_TagName = "GetCompanies",
                                EM_Type = "Release"
                            });
                        }
                        else
                        {
                            uow.ErrorMessages.Add(new ErrorMessage
                            {
                                EM_InvoiceId = cI_ID,
                                EM_Message = ex.Message,
                                EM_DateTime = DateTime.Now,
                                EM_ScannedValue = ex.Data["Value"].ToString(),
                                EM_TagName = "GetCompanies",
                                EM_Type = "Release"
                            });
                        }
                        uow.Complete();
                    }
                }
            }
            return companies;
        }

        private List<CustomerReferenceElement> GetCustomerReferences(Guid cI_ID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                List<CustomerReferenceElement> custRefs = new List<CustomerReferenceElement>();
                var refs = uow.CustomerReferences.Find(x => x.CR_InvoiceID == cI_ID).ToList();

                if (refs.Count > 0)
                {
                    foreach (var custRef in refs)
                    {
                        CustomerReferenceElement newCustRef = new CustomerReferenceElement
                        {
                            RefType = custRef.CR_RefType,
                            RefValue = custRef.CR_RefValue
                        };
                        custRefs.Add(newCustRef);
                    }
                }

                return custRefs;
            }

        }

        private NodeFileIdendtityMatrix CreateIdentityMatrix(ConvertedFile file)
        {
            NodeFileIdendtityMatrix idMatrix = new NodeFileIdendtityMatrix
            {
                SenderId = "CTCDOXIN",
                CustomerId = GetCustomerID(file.FC_CustomerId),
                DocumentIdentifier = file.FC_DocumentIdentifier,
                DocumentType = string.Empty,
                EventCode = file.FC_EventCode,
                FileDateTime = DateTime.Now.ToString("dd/MM/yyyyTHH:mm:ss"),
                FileId = file.FC_FileName,
                OriginalFileName = file.FC_OriginalFileName
            };
            return idMatrix;

        }

        private string GetCustomerID(Guid fC_CustomerId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                return uow.Customers.Get(fC_CustomerId).CU_CODE;
            }
        }

        #endregion

        #region Methods
        public string DeleteFiles()
        {

            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                var deleteDate = DateTime.Now.Date;

                var deleteList = uow.ConvertedFiles.Find(x => x.FC_FlagForDelete && DbFunctions.TruncateTime(DbFunctions.AddDays(x.FC_DeleteDate, 7)) == deleteDate);

                var commoninvoices = (from deleted in deleteList
                                      join ci in uow.CommonInvoices.GetAll() on deleted.FC_ID equals ci.CI_ConvertedFileId
                                      select ci).ToList();
                var companiesToDelete = (from ci in commoninvoices
                                         join cc in uow.Companies.GetAll() on ci.CI_ID equals cc.CO_InvoiceId
                                         select cc).ToList();
                var datesToDelete = (from ci in commoninvoices
                                     join cd in uow.CommonDates.GetAll() on ci.CI_ID equals cd.DA_InvoiceId
                                     select cd).ToList();

                var errorsToDelete = (from l in datesToDelete
                                      join em in uow.ErrorMessages.GetAll() on l.DA_Id equals em.EM_DateId
                                      select em).ToList();
                var linesToDelete = (from ci in commoninvoices
                                     join il in uow.CommonOrderLines.GetAll() on ci.CI_ID equals il.OL_InvoiceID
                                     select il).ToList();
                var errorLinesToDelete = (from l in linesToDelete
                                          join em in uow.ErrorMessages.GetAll() on l.OL_ID equals em.EM_CommonLineId
                                          select em).ToList();
                uow.ErrorMessages.RemoveRange(errorsToDelete);
                _summary += string.Format("{0:g} :", DateTime.Now) + "Removed " + errorsToDelete.Count + " Errors" + Environment.NewLine;
                uow.ErrorMessages.RemoveRange(errorLinesToDelete);
                _summary += string.Format("{0:g} :", DateTime.Now) + "Removed " + errorLinesToDelete.Count + " Error Lines" + Environment.NewLine;
                uow.CommonDates.RemoveRange(datesToDelete);
                _summary += string.Format("{0:g} :", DateTime.Now) + "Removed " + datesToDelete.Count + " Date entries " + Environment.NewLine;
                uow.Companies.RemoveRange(companiesToDelete);
                _summary += string.Format("{0:g} :", DateTime.Now) + "Removed " + companiesToDelete.Count + " Company entries " + Environment.NewLine;
                uow.CommonOrderLines.RemoveRange(linesToDelete);
                _summary += string.Format("{0:g} :", DateTime.Now) + "Removed " + linesToDelete.Count + " Line entries " + Environment.NewLine;
                uow.CommonInvoices.RemoveRange(commoninvoices);
                _summary += string.Format("{0:g} :", DateTime.Now) + "Removed " + commoninvoices.Count + " Commercial Invoices " + Environment.NewLine;
                uow.ConvertedFiles.RemoveRange(deleteList);
                uow.Complete();
                _summary += string.Format("{0:g} :", DateTime.Now) + "Delete operation complete " + Environment.NewLine;
                return _summary;
            }
        }





        public void ReleaseFiles()
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                var releaseList = uow.ConvertedFiles.Find(x => x.FC_Released && x.FC_ReleaseDate == null);
                if (releaseList != null)
                {
                    foreach (var converted in releaseList)
                    {
                        switch (converted.FC_DocumentIdentifier)
                        {
                            case "CINV":
                                BuildCinv(converted.FC_ID);
                                break;
                            case "WORD":
                                break;
                            case "HBL":
                                break;
                            case "WIN":
                                break;


                        }
                    }
                }

            }

        }

        private void BuildCinv(Guid fC_ID)
        {
            NodeFile nodeFile = new NodeFile();
            nodeFile.IdendtityMatrix = GetMatrixFromConverted(fC_ID);
            nodeFile.CommercialInvoice = GetCommercialInvoice(fC_ID);

        }

        private NodeFileIdendtityMatrix GetMatrixFromConverted(Guid fC_ID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new CommonEntity(_connstring)))
            {
                var converted = uow.ConvertedFiles.Get(fC_ID);
                if (converted != null)
                {
                    var cust = uow.Customers.Get(converted.FC_CustomerId).CU_CODE;
                    return new NodeFileIdendtityMatrix
                    {
                        DocumentType = converted.FC_DocumentIdentifier,
                        SenderId = converted.FC_SenderID,
                        CustomerId = cust,
                        FileDateTime = converted.FC_DateReceived.ToString("dd/MM/yyyyThh:mm:ss"),
                        OriginalFileName = converted.FC_FileName,
                        PurposeCode = "DIM"
                    };
                }
                return null;
            }
        }
        #endregion


        #region Helpers

        #endregion
    }
}
