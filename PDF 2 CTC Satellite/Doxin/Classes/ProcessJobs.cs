﻿using DoxIn.Classes;
using DoxIn.Resources;
using NodeData;
using NodeData.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Doxin.Classes
{
    public class ProcessJobs : IDisposable
    {
        #region members
        private bool disposedValue;
        private bool _production;
        const string images = "xmlpdf";
        const string origPath = @"\\ctc-webserver08\files\PDF2Go\In";

        #endregion
        #region Properties
        public bool ProductionMode
        {
            get { return _production; }
            set { _production = value; }
        }
        #endregion

        #region constructors
        public ProcessJobs()
        {

        }

        public ProcessJobs(bool production)
        {
            ProductionMode = production;
        }
        #endregion

        #region Methods
        public void ProcessingQueue()
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            NodeData.HeartBeatManager heartBeat = new HeartBeatManager(Globals.NodeConnManager.ConnString);
            heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());

            string filesPath = string.Empty;

            if (_production)
            {
                if (string.IsNullOrEmpty(Globals.PickupPath))
                {
                    EventLog myLog = new EventLog();
                    myLog.Source = Globals.AppName;

                    // Write an informational entry to the event log.
                    myLog.WriteEntry("Folder does not exist for Production Pickup path.Please enter in Setup");
                }

                filesPath = Globals.PickupPath;
            }
            else
            {
                if (string.IsNullOrEmpty(Globals.TestPath))
                {
                    EventLog myLog = new EventLog();
                    myLog.Source = Globals.AppName;

                    // Write an informational entry to the event log.
                    myLog.WriteEntry("Settings for Test Pickup path are missing. Please enter in Setup");
                    return;
                }
                filesPath = Globals.TestPath;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            int iCount = 0;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var list = diTodo.GetFiles();
            int filesImported = 0;
            foreach (var file in list)
            {
                filesImported++;

                ImportXML importXML = new ImportXML(Globals.SattConnManager.ConnString, file.FullName, Globals.ImagePath);
                Log(m.Name, "Information", 1, "Importing " + file.Name);

                var nodefile = importXML.ConvertXml(file.FullName);
                var convertedFile = importXML.ImportData(nodefile);
                if (convertedFile != null)
                {
                    importXML.MoveImageFile(convertedFile, Path.Combine(origPath, images));

                }
                else
                {
                    Log(m.Name, "Error", 1, "Failed to Import " + file.Name + " .Error: " + importXML.ErrorMessage);
                }
                file.Delete();
            }
            if (filesImported > 0)
            {
                Log(m.Name, "Information", 1, "Import process complete." + filesImported + " files imported");
            }

        }

        public void ProcessDeleted()
        {
            var m = MethodInfo.GetCurrentMethod();
            Log(m.Name, "Information", 1, "Beginning Deleted Files processing");
            ProcessFiles pf = new ProcessFiles(Globals.SattConnManager.ConnString, Globals.AppLogPath);
        }
        public void ProcessReleased()
        {
            var m = MethodInfo.GetCurrentMethod();
            Log(m.Name, "Information", 1, "Beginning Released Files processing");
            ProcessFiles pf = new ProcessFiles(Globals.SattConnManager.ConnString, Globals.AppLogPath);
            pf.DoConvert();

        }

        #endregion

        #region helpers

        private void Log(string proc, string logType, int level, string toLog)
        {
            using (AppLogging log = new AppLogging(Globals.NodeConnManager.ConnString))
            {

                log.AddLog(new AppLog
                {

                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                    GL_ServerName = Environment.MachineName.ToString(),
                    GL_Date = DateTime.Now,
                    GL_ProcName = proc,
                    GL_MessageType = logType,
                    GL_MessageLevel = level,
                    GL_MessageDetail = toLog
                });

            }

        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~ProcessJobs()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
