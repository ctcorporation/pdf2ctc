﻿using DoxIn.Resources;
using NodeData;
using NodeResources;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace DoxIn
{
    public class SystemSettings
    {
        #region members
        private string _xmlPath;
        private XDocument xmlSettings;
        private string _errString;
        #endregion

        #region properties
        public string XmlPath
        {
            get { return _xmlPath; }
            set { _xmlPath = value; }
        }

        public string ErrString
        {
            get
            {
                return _errString;
            }
            set
            {
                _errString = value;
            }
        }
        #endregion

        #region constructors
        public SystemSettings(string xmlPath)
        {
            _xmlPath = xmlPath;
            if (!File.Exists(_xmlPath))
            {
                CreateDefaultSettings();
            }
            xmlSettings = XDocument.Load(_xmlPath);
        }
        #endregion

        #region methods
        public bool LoadSettings()
        {
            bool settingsNeeded = false;
            _errString = string.Empty;

            string server = string.Empty;
            string dbase = string.Empty;
            string user = string.Empty;
            string password = string.Empty;

            //SetElement("Database", "SatelliteDatabase", string.Empty);
            var _satelliteServer = GetElement("SatelliteDatabase", "ServerName");
            if (_satelliteServer == null)
            {
                _errString += "Satellite Server Settings needed " + Environment.NewLine;
                settingsNeeded = true;
            }
            else
            {
                server = _satelliteServer.Value;
            }
            var _satelliteDatabase = GetElement("SatelliteDatabase", "Database");
            if (_satelliteDatabase == null)
            {
                _errString += "Satellite Database Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                dbase = _satelliteDatabase.Value;
            }
            var _satelliteUsername = GetElement("SatelliteDatabase", "UserName");
            if (_satelliteUsername == null)
            {
                _errString += "Satellite Database User Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                user = _satelliteUsername.Value;
            }
            var _satellitePassword = GetElement("SatelliteDatabase", "Password");
            if (_satellitePassword == null)
            {
                _errString += "Satellite Database Password needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                password = _satellitePassword.Value;
            }
            Globals.SattConnManager = new ConnectionManager(server, dbase, user, password);
            // SetElement("Database", "NodeDatabase", string.Empty);
            server = string.Empty;
            dbase = string.Empty;
            user = string.Empty;
            password = string.Empty;

            var _nodeServer = GetElement("NodeDatabase", "ServerName");
            if (_nodeServer == null)
            {
                _errString += "HeartBeat Server Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                server = _nodeServer.Value;
            }
            var _nodeDatabase = GetElement("NodeDatabase", "Database");
            if (_nodeDatabase == null)
            {
                _errString += "HeartBeat Database Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                dbase = _nodeDatabase.Value;
            }
            var _nodeUsername = GetElement("NodeDatabase", "UserName");
            if (_nodeUsername == null)
            {
                _errString += "HeartBeat Database UserName needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                user = _nodeUsername.Value;
            }
            var _nodePassword = GetElement("NodeDatabase", "Password");
            if (_nodePassword == null)
            {
                _errString += "HeartBeat Database Password needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                password = _nodePassword.Value;
            }
            Globals.NodeConnManager = new ConnectionManager(server, dbase, user, password);
            var _pickupPath = GetElement("Config", "PickupPath");
            if (_pickupPath == null)
            {
                _errString += "Pickup Path Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                Globals.PickupPath = _pickupPath.Value;
            }
            var _alertsTo = GetElement("Config", "AlertsTo");
            if (_alertsTo == null)
            {

                SetElement("Config", "AlertsTo", "csr@ctcorp.com.au");
                Globals.AlertsTo = "csr@ctcorp.com.au";

            }
            else
            {
                Globals.AlertsTo = _alertsTo.Value;
            }
            var _outputPath = GetElement("Config", "OutputPath");
            if (_outputPath == null)
            {
                _errString += "Output Path Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                Globals.OutPutPath = _outputPath.Value;
            }
            var _testPath = GetElement("Config", "TestPath");
            if (_testPath == null)
            {
                _errString += "Testing Path Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                Globals.TestPath = _testPath.Value;
            }
            var _failPath = GetElement("Config", "FailPath");
            if (_failPath == null)
            {
                _errString += "Failure Path Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            else
            {
                Globals.FailPath = _failPath.Value;
            }
            var _mainTimer = GetElement("Config", "Timer");
            if (_mainTimer == null)
            {
                SetElement("Config", "Timer", "5");
                Globals.MainTimerInt = 5;
            }
            else
            {
                int timer = 0;
                Globals.MainTimerInt = int.TryParse(_mainTimer.Value, out timer) ? timer : 5;
            }
            var _imagePath = GetElement("Config", "ImagePath");
            if (_imagePath == null)
            {
                _errString += "Default Images Path Settings needed " + Environment.NewLine;
                settingsNeeded = true;
            }
            else
            {
                Globals.ImagePath = _imagePath.Value;
            }

            if (settingsNeeded)
            {
                xmlSettings.Save(_xmlPath);

            }
            string mailserver = string.Empty;
            //GetElement("Communication", "MailServer");
            var _mailServer = GetElement("Communication", "MailServer");
            if (_mailServer == null)
            {
                _errString += "Mail Server settings Settings needed " + Environment.NewLine;
                settingsNeeded = true;
            }
            else
            {
                mailserver = _mailServer.Value;
            }
            string port = string.Empty;

            var _port = GetElement("Communication", "Port");
            if (_port == null)
            {
                _errString += "Mail Server Port Settings needed " + Environment.NewLine;
                settingsNeeded = true;
                port = "25";
            }
            else
            {
                port = _port.Value;
            }
            string ssl = string.Empty;
            var _ssl = GetElement("Communication", "SSL");
            if (_ssl == null)
            {
                SetElement("Communication", "SSL", "None");
            }
            else
            {
                ssl = _ssl.Value;
            }
            user = string.Empty;
            var _user = GetElement("Communication", "UserName");
            user = _user != null ? _user.Value : string.Empty;
            password = string.Empty;
            var _password = GetElement("Communication", "Password");
            password = _password != null ? _password.Value : string.Empty;
            string emailfrom = string.Empty;
            var _emailfrom = GetElement("Communication", "EmailFrom");
            emailfrom = _emailfrom != null ? _emailfrom.Value : string.Empty;

            if (string.IsNullOrEmpty(emailfrom))
            {
                _errString += "System Email From Settings needed " + Environment.NewLine;
                settingsNeeded = true;

            }
            int _iPort = 0;
            Globals.MailServerSettings = new MailServerSettings
            {
                Server = mailserver,
                Email = emailfrom,
                Password = password,
                UserName = user,
                Port = int.TryParse(port, out _iPort) ? _iPort : 25

            };
            if (settingsNeeded)
            {
                xmlSettings.Save(_xmlPath);
            }
            return settingsNeeded;

        }

        public void UpdateSettings()
        {
            xmlSettings.Save(_xmlPath);
        }

        #endregion

        #region helpers

        public void SetElement(string parentElement, string newElement, string newValue)
        {
            try
            {
                var xpath = xmlSettings.Root;
                if (!xmlSettings.Descendants(parentElement).Elements(newElement).Any())
                {
                    XElement newEl = new XElement(newElement);
                    newEl.Value = newValue;
                    var parent = (from e in xmlSettings.Descendants(parentElement)
                                  select e).FirstOrDefault();
                    parent.Add(newEl);
                }
                else
                {
                    var parent = xmlSettings.Descendants(parentElement).Elements(newElement).FirstOrDefault();
                    parent.Value = newValue;
                }
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                Console.WriteLine("Error returning value: Parent='" + parentElement + "' NodeToFind='" + newElement + "'");

            }

        }

        public XElement GetElement(string elementName, string valueToFind)
        {
            try
            {
                if (!xmlSettings.Descendants(elementName).Elements(valueToFind).Any())
                {
                    return null;
                }

                var node = xmlSettings.Descendants(elementName).Elements(valueToFind).FirstOrDefault();
                if (node.Name == valueToFind)
                {
                    if (string.IsNullOrEmpty(node.Value))
                    {
                        return null;
                    };
                    return node;
                }
                return null;
            }
            catch (Exception ex)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod().Name;
                Console.WriteLine("Error returning value: Parent='" + elementName + "' NodeToFind='" + valueToFind + "'");

                return null;
            }

        }
        public void CreateDefaultSettings()
        {
            XDocument xmlSettings = new XDocument(
                new XDeclaration("1.0", "UTF-8", "Yes"),
                new XElement("Satellite",
                    new XElement("Config",
                        new XElement("PickupPath"),
                        new XElement("OutputPath"),
                        new XElement("FailPath"),
                        new XElement("Timer"),
                        new XElement("AlertsTo")),

                    new XElement("Communication",
                        new XElement("MailServer"),
                        new XElement("Port"),
                        new XElement("SSL"),
                        new XElement("UserName"),
                        new XElement("Password"),
                        new XElement("EmailFrom")),

                    new XElement("Database",
                        new XElement("SatelliteDatabase",
                            new XElement("ServerName"),
                            new XElement("Databse"),
                            new XElement("UserName"),
                            new XElement("Password")),
                        new XElement("NodeDatabase",
                            new XElement("ServerName"),
                            new XElement("Databse"),
                            new XElement("UserName"),
                            new XElement("Password")))
                ));
            xmlSettings.Save(_xmlPath);
        }
        #endregion
    }
}
