﻿using DoxIn.Models;
using DoxIn.Repositories;
using System;
using System.Data.Entity.Validation;

namespace DoxIn.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Members
        private readonly CommonEntity _context;
        private string _errorMessage;
        private bool disposed;
        #endregion

        #region Constructor
        public UnitOfWork(CommonEntity context)
        {
            _context = context;
            AddRepos();
        }
        #endregion

        #region Properties

        public ILogRepository Logs
        {
            get;
            private set;
        }
        public ICommonDateRepository CommonDates
        {
            get;
            private set;
        }
        public ICommonInvoiceRepository CommonInvoices
        {
            get;
            private set;
        }
        public ICommonOrderLineRepository CommonOrderLines
        {
            get;
            private set;
        }
        public ICompanyRepository Companies
        {
            get; set;
        }
        public ICompanyTypeRepository CompanyTypes
        {
            get;
            private set;
        }
        public IConvertedFileRepository ConvertedFiles
        {
            get;
            private set;
        }
        public ICustomerRepository Customers
        {
            get;
            private set;
        }
        public ICustomerReferenceRepository CustomerReferences
        {
            get;
            private set;
        }
        public IDateTypeRepository DateTypes
        {
            get;
            private set;
        }
        public IErrorMessageRepository ErrorMessages
        {
            get;
            private set;
        }

        public IWarehouseOrderRepository WarehouseOrders
        {
            get;
            private set;
        }
        #endregion

        #region Methods
        private void AddRepos()
        {

            CommonDates = new CommonDateRepository(_context);
            CommonInvoices = new CommonInvoiceRepository(_context);
            CommonOrderLines = new CommonOrderLineRepository(_context);
            Companies = new CompanyRepository(_context);
            CompanyTypes = new CompanyTypeRepository(_context);
            ConvertedFiles = new ConvertedFileRepository(_context);
            CustomerReferences = new CustomerReferenceRepository(_context);
            Customers = new CustomerRepository(_context);
            DateTypes = new DateTypeRepository(_context);
            ErrorMessages = new ErrorMessageRepository(_context);
            Logs = new LogRepository(_context);

            WarehouseOrders = new WarehouseOrderRepository(_context);

        }

        public int Complete()
        {
            try
            {
                return _context.SaveChanges();

            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }

                }
                throw new Exception(_errorMessage, ex);
            }
        }
        #endregion


        #region Helpers
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }

        #endregion




    }
}
