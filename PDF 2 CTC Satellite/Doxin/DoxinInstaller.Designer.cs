﻿namespace Doxin
{
    partial class DoxinInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.doxinProcessorInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.doxinServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // doxinProcessorInstaller
            // 
            this.doxinProcessorInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.doxinProcessorInstaller.Password = null;
            this.doxinProcessorInstaller.Username = null;
            // 
            // doxinServiceInstaller
            // 
            this.doxinServiceInstaller.Description = "Doxin Satellite";
            this.doxinServiceInstaller.DisplayName = "Doxin Service";
            this.doxinServiceInstaller.ServiceName = "Doxin";
            this.doxinServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // DoxinInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.doxinProcessorInstaller,
            this.doxinServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller doxinProcessorInstaller;
        private System.ServiceProcess.ServiceInstaller doxinServiceInstaller;
    }
}