﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace Doxin
{
    [RunInstaller(true)]
    public partial class DoxinInstaller  : System.Configuration.Install.Installer
    {
        public DoxinInstaller()
        {
            InitializeComponent();
        }
    }
}
