﻿using Doxin.Classes;
using DoxIn;
using DoxIn.Classes;
using DoxIn.Resources;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceProcess;

namespace Doxin
{
    public partial class Doxin_Service : ServiceBase
    {
        public Doxin_Service()
        {
            InitializeComponent();
        }

        public System.Timers.Timer tmrMain;
        public System.Timers.Timer tmrTideMark;
        private Stopwatch procTimer;
        protected override void OnStart(string[] args)
        {
            if (!EventLog.SourceExists("Doxin Service"))
            {
                EventLog.CreateEventSource("Doxin Service", "Application");
                Console.WriteLine("CreatedEventSource");
                Console.WriteLine("Exiting, execute the application a second time to use the source.");
                return;
            }
            Globals.AppName = this.ServiceName;
            LoadSettings();
            using (AppLogging log = new AppLogging(Globals.NodeConnManager.ConnString))
            {

                log.AddLog(new NodeData.Models.AppLog
                {

                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                    GL_ServerName = Environment.MachineName.ToString(),
                    GL_Date = DateTime.Now,
                    GL_ProcName = "Starting",
                    GL_MessageType = "Information",
                    GL_MessageLevel = 1,
                    GL_MessageDetail = "Doxin started"
                });

            }
            tmrMain = new System.Timers.Timer();
            tmrMain.Interval = (int)TimeSpan.FromMinutes(Globals.MainTimerInt).TotalMilliseconds;
            tmrMain.Elapsed += new System.Timers.ElapsedEventHandler(tmrMain_Elapsed);
            tmrTideMark = new System.Timers.Timer();
            tmrTideMark.Interval = (int)TimeSpan.FromMinutes(15).TotalMilliseconds;
            tmrTideMark.Elapsed += new System.Timers.ElapsedEventHandler(tmrTideMark_Elapsed);
        }

        protected override void OnStop()
        {
            tmrMain.Enabled = false;

        }

        protected void LoadSettings()
        {
            var m = MethodInfo.GetCurrentMethod();
            CommonApplicationData appData = new CommonApplicationData("CTC", "Doxin", true);
            string appDataPath = appData.ToString();
            string s = Path.Combine(appDataPath, Globals.AppName + ".xml");
            SystemSettings systemSettings = new SystemSettings(s);
            if (systemSettings.LoadSettings())
            {

                // Create an EventLog instance and assign its source.
                EventLog myLog = new EventLog();
                myLog.Source = this.ServiceName;

                // Write an informational entry to the event log.
                myLog.WriteEntry("Doxin Not Setup properly. " + Environment.NewLine + systemSettings.ErrString);

                ServiceController sc = new ServiceController(this.ServiceName);
                sc.Stop();
            }


        }

        private void tmrMain_Elapsed(object sender, EventArgs e)
        {
            procTimer.Start();
            ProcessJobs();
            procTimer.Stop();
        }

        private void tmrTideMark_Elapsed(object sender, EventArgs e)
        {

        }
        private void ProcessJobs()
        {
            tmrMain.Stop();
            using (ProcessJobs jobProcessor = new ProcessJobs())
            {
                if (procTimer.ElapsedMilliseconds >= (int)TimeSpan.FromMinutes(10).TotalMilliseconds)
                {
                    EventLog myLog = new EventLog();
                    myLog.Source = "Doxin Service";

                    // Write an informational entry to the event log.
                    myLog.WriteEntry("Doxin High Tide Event. Current process running for " + TimeSpan.FromMilliseconds(procTimer.ElapsedMilliseconds).TotalMinutes);
                    Process process = new Process();
                    process.StartInfo.FileName = "cmd";
                    process.StartInfo.Arguments = "/c net stop \"" + this.ServiceName + "\" & net start \"" + this.ServiceName + "\"";
                    process.Start();

                }
            }
        }
    }
}
