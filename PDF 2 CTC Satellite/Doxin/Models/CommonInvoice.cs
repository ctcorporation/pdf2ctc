﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn.Models
{
    public class CommonInvoice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CI_ID { get; set; }

        public string CI_InvoiceNo { get; set; }

        public string CI_ShipmentNo { get; set; }
        public string CI_TotalInvoiceAmount { get; set; }

        public string CI_Currency { get; set; }

        public string CI_CountryOfOrigin { get; set; }

        public string CI_InvoiceTerms { get; set; }

        public bool CI_ErrorsFound { get; set; }
        public Guid CI_ConvertedFileId { get; set; }
        public ConvertedFile CI_ConvertedFile { get; set; }
        public ICollection<CommonOrderLine> CI_InvoiceLines { get; set; }
        public ICollection<CommonDate> CI_Dates { get; set; }
        public ICollection<Company> CI_Companies { get; set; }
        public ICollection<ErrorMessage> CI_Errors { get; set; }
        public ICollection<CustomerReference> CI_CustReferences { get; set; }

    }
}
