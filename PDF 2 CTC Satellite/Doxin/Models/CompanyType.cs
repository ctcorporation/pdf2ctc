﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn.Models
{
    public class CompanyType
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CT_ID { get; set; }

        [MaxLength(30)]
        public string CT_CompanyType { get; set; }

        //Company Nav Props (Many)
        public ICollection<Company> Companies { get; set; }



    }
}
