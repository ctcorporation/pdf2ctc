﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn.Models
{
    [Table("ConvertedFiles")]
    public class ConvertedFile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid FC_ID { get; set; }
        public DateTime FC_DateReceived { get; set; }
        [MaxLength(20)]
        public string FC_DocumentType { get; set; }
        public string FC_FileId { get; set; }
        [MaxLength(100)]
        public string FC_SenderID { get; set; }
        [MaxLength(10)]
        public string FC_PurposeCode { get; set; }
        [MaxLength(10)]
        public string FC_EventCode { get; set; }
        public string FC_FileName { get; set; }
        public string FC_Location { get; set; }
        public bool FC_Released { get; set; }
        public bool FC_Converted { get; set; }
        public bool FC_FlagForDelete { get; set; }
        public DateTime? FC_ReleaseDate { get; set; }
        public DateTime? FC_ConvertDate { get; set; }
        public DateTime? FC_DeleteDate { get; set; }
        [MaxLength(30)]
        public string FC_OperationType { get; set; }
        public bool FC_ErrorsFound { get; set; }
        //Original File Name
        [MaxLength(100)]
        public string FC_OriginalFileName { get; set; }
        [MaxLength(50)]
        public string FC_DocumentIdentifier { get; set; }
        public Guid FC_CustomerId { get; set; }
        public string FC_ImageFileLocation { get; set; }
        [MaxLength(10)]
        public string FC_FileType { get; set; }





    }
}
