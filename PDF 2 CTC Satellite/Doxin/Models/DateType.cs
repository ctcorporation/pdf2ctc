﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn.Models
{
    public class DateType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DT_Id { get; set; }
        [MaxLength(20)]
        public string DT_DateType { get; set; }

        public string DT_Name { get; set; }

        IList<CommonDate> Dates { get; set; }
    }
}
