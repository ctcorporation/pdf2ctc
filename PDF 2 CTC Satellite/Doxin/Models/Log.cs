﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoxIn.Models
{
    public class Log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid LO_Id { get; set; }

        [Display(Name = "Log Type")]
        [MaxLength(20)]
        public string LO_LogType { get; set; }

        public DateTime LO_Date { get; set; }

        [MaxLength(50)]
        public string LO_Operation { get; set; }

        [MaxLength(50)]
        public string LO_FieldName { get; set; }

        public Guid LO_ParentId { get; set; }

        public string LO_Message { get; set; }

    }
}
