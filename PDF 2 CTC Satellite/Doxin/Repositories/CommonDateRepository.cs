﻿using DoxIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoxIn.Repositories
{
    public class CommonDateRepository : GenericRepository<CommonDate>, ICommonDateRepository
    {
        public CommonDateRepository(CommonEntity context)
            : base(context)
        {

        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
