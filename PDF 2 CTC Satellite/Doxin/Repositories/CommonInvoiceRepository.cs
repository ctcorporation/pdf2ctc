﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public class CommonInvoiceRepository : GenericRepository<CommonInvoice>, ICommonInvoiceRepository
    {
        public CommonInvoiceRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }

}
