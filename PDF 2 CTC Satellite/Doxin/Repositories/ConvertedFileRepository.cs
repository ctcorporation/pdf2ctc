﻿using DoxIn.Models;


namespace DoxIn.Repositories
{
    public class ConvertedFileRepository : GenericRepository<ConvertedFile>, IConvertedFileRepository
    {
        public ConvertedFileRepository(CommonEntity context)
            : base(context)
        {

        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }

    }
}
