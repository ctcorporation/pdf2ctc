﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public class CustomerReferenceRepository : GenericRepository<CustomerReference>, ICustomerReferenceRepository
    {
        public CustomerReferenceRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
