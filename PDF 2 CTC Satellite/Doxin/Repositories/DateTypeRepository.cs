﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public class DateTypeRepository : GenericRepository<DateType>, IDateTypeRepository
    {
        public DateTypeRepository(CommonEntity context)
            : base(context)
        {


        }

        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }

        }
    }
}
