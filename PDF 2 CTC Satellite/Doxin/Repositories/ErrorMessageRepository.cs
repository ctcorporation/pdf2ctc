﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public class ErrorMessageRepository : GenericRepository<ErrorMessage>, IErrorMessageRepository
    {
        public ErrorMessageRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
