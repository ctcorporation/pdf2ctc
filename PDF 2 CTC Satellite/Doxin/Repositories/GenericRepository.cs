﻿using DoxIn.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DoxIn.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly CommonEntity Context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(CommonEntity context)
        {
            Context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {

            dbSet.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            dbSet.AddRange(entities);
        }

        public void Remove(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public void Remove(Guid id)
        {
            TEntity tobeDeleted = dbSet.Find(id);
            if (tobeDeleted != null)
            {
                dbSet.Remove(tobeDeleted);
            }
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            dbSet.RemoveRange(entities);
        }

        public TEntity Get(Guid id)
        {
            return dbSet.Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return dbSet;
        }


        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProps = "")
        {
            //var query = Context.Set<TEntity>().Where(predicate);
            //return query;

            IQueryable<TEntity> query = dbSet;

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            foreach (var includeProperty in includeProps.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }


    }
}
