﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface ICommonDateRepository : IGenericRepository<CommonDate>
    {
        CommonEntity CommonEntity { get; }
    }
}