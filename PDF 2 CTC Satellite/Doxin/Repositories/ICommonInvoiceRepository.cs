﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface ICommonInvoiceRepository : IGenericRepository<CommonInvoice>
    {
        CommonEntity CommonEntity { get; }
    }
}