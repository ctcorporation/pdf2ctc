﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface ICommonOrderLineRepository : IGenericRepository<CommonOrderLine>
    {
        CommonEntity CommonEntity { get; }
    }
}