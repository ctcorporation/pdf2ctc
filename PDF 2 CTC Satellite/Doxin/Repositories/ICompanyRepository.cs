﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface ICompanyRepository : IGenericRepository<Company>
    {
        CommonEntity CommonEntity { get; }
    }
}