﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface ICompanyTypeRepository : IGenericRepository<CompanyType>
    {
        CommonEntity CommonEntity { get; }
    }
}