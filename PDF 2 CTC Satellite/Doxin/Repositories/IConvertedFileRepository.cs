﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface IConvertedFileRepository : IGenericRepository<ConvertedFile>
    {
        CommonEntity CommonEntity { get; }
    }
}