﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface ICustomerReferenceRepository : IGenericRepository<CustomerReference>
    {
        CommonEntity CommonEntity { get; }
    }
}