﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        CommonEntity CommonEntity { get; }
    }
}