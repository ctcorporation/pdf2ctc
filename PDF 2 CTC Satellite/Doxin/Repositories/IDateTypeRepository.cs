﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface IDateTypeRepository : IGenericRepository<DateType>
    {
        CommonEntity CommonEntity { get; }
    }
}