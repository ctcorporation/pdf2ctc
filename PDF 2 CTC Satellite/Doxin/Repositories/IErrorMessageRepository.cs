﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface IErrorMessageRepository : IGenericRepository<ErrorMessage>
    {
        CommonEntity CommonEntity { get; }
    }
}