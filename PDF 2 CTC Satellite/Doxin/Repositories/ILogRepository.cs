﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface ILogRepository : IGenericRepository<Log>
    {
        CommonEntity CommonEntity { get; }
    }
}