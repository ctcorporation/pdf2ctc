﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public interface IWarehouseOrderRepository : IGenericRepository<WarehouseOrder>
    {
        CommonEntity CommonEntity { get; }
    }
}