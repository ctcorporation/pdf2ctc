﻿using DoxIn.Models;

namespace DoxIn.Repositories
{
    public class WarehouseOrderRepository : GenericRepository<WarehouseOrder>, IWarehouseOrderRepository
    {
        public WarehouseOrderRepository(CommonEntity context)
           : base(context)
        {

        }
        public CommonEntity CommonEntity
        {
            get { return Context as CommonEntity; }
        }
    }
}
