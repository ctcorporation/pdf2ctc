﻿using System;

namespace DoxIn.Resources
{
    public class SMTPServer
    {
        private static String server;
        private static String port;
        private static String username;
        private static String email;
        private static String password;
        private static string _sSL;

        public static String Server
        {
            get { return server; }
            set { server = value; }
        }
        public static String Port
        {
            get { return port; }
            set { port = value; }
        }
        public static String Email
        {
            get { return email; }
            set { email = value; }
        }
        public static String Username
        {
            get { return username; }
            set { username = value; }
        }
        public static String Password
        {
            get { return password; }
            set { password = value; }
        }

        public static string SSL
        {
            get
            { return _sSL; }
            set
            { _sSL = value; }
        }
    }
}
